package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

public class TestVectorAndList {

	static int count = 50000;
	
	// @Test
	public static void testVector() throws InterruptedException {
		final Vector<String> l = new Vector<String>();
		final CountDownLatch latch = new CountDownLatch(count);
		long start = System.currentTimeMillis();
		for (int i=0; i<count; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					l.add(Thread.currentThread().getName());
					latch.countDown();
				}
			}).start();
		}
		latch.await();
		long end = System.currentTimeMillis();
		System.out.println("testVector 执行时间(ms)：" + (end - start) + ", 容量：" + l.size());
	}	
	
	// @Test
	public static void testList() throws InterruptedException {
		final List<String> l = new ArrayList<String>();
		final CountDownLatch latch = new CountDownLatch(count);
		long start = System.currentTimeMillis();
		for (int i=0; i<count; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					l.add(Thread.currentThread().getName());
					latch.countDown();
				}
			}).start();
		}
		latch.await();
		long end = System.currentTimeMillis();
		System.out.println("testList 执行时间(ms)：" + (end - start) + ", 容量：" + l.size());
	}
	
	public static void main(String[] args) throws InterruptedException {
		TestVectorAndList.testList();
		TestVectorAndList.testVector();
	}
}

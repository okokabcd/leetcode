package test;

import java.util.concurrent.CountDownLatch;

public class TestVolatile {
	public static int plus(final Counter counter) throws InterruptedException {
		int count = 100000;
		final CountDownLatch latch = new CountDownLatch(count);
		for (int i=0; i<count; i++) {
			new Thread(new Runnable() {
				public void run() {
					counter.i++;
					latch.countDown();
				}
			}).start();
		}
		latch.await();
		return 1;
	}
}

class Counter {
	int i;
	void inc() {
		i++;
	}
}

class Counter2 extends Counter {
	volatile int i;
	void inc() {
		i++;
	}
}

class Counter22 extends Counter {
	int i;
	synchronized void inc() {
		i++;
	}
}

class Counter23 extends Counter {
	volatile int i;
	synchronized void inc() {
		i++;
	}
}

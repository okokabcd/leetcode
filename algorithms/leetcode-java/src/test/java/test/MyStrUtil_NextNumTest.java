package test;

import aaa.util.MyStrUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class MyStrUtil_NextNumTest {

    @Test
    public void test01() {
        Assert.assertEquals("0001", MyStrUtil.nextNum(null));
    }

    @Test
    public void test02() {
        Assert.assertEquals("0001", MyStrUtil.nextNum(""));
    }

    @Test
    public void test03() {
        Assert.assertEquals("0001", MyStrUtil.nextNum("0000"));
    }

    @Test
    public void test04() {
        Assert.assertEquals("0005", MyStrUtil.nextNum("0004"));
    }

    @Test
    public void test05() {
        Assert.assertEquals("0000", MyStrUtil.nextNum("9999"));
    }
}

package test;

import aaa.util.MyStrUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class MyStrUtil_ExtractSeqTest {
    @Test
    public void test01() {
        Assert.assertEquals("", MyStrUtil.extractSeq(null));
    }

    @Test
    public void test02() {
        Assert.assertEquals("", MyStrUtil.extractSeq(""));
    }

    @Test
    public void test03() {
        Assert.assertEquals("4位序列号", MyStrUtil.extractSeq("项目年度-问题类别编号-4位序列号"));
    }

    @Test
    public void test04() {
        Assert.assertEquals("", MyStrUtil.extractSeq("项目年度-问题类别编号"));
    }

    @Test
    public void test05() {
        Assert.assertEquals("", MyStrUtil.extractSeq("项目年度-问题类别编号-4位序列号-aaa"));
    }
}

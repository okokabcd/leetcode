package test;

import aaa.util.RegUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class RegUtil_ExtractNumTest {
    @Test
    public void test01() {
        Assert.assertEquals("3.5", RegUtil.extractNum("3.5岗位管理"));
    }

    @Test
    public void test02() {
        Assert.assertEquals("", RegUtil.extractNum("a3.5岗位管理"));
    }

    @Test
    public void test03() {
        Assert.assertEquals("13.5456", RegUtil.extractNum("13.5456岗位管理"));
    }

    @Test
    public void test04() {
        Assert.assertEquals("", RegUtil.extractNum("13..5456岗位管理"));
    }

    @Test
    public void test05() {
        Assert.assertEquals("", RegUtil.extractNum(""));
    }

    @Test
    public void test06() {
        Assert.assertEquals("", RegUtil.extractNum(null));
    }
}

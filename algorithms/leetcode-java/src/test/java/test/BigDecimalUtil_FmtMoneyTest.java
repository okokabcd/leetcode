package test;

import aaa.util.BigDecimalUtil;
import org.junit.Assert;
import org.junit.Test;

public class BigDecimalUtil_FmtMoneyTest {

    /**
     * 非金额数据，返回""
     */
    @Test
    public void test01() {
        Assert.assertEquals("", BigDecimalUtil.fmtMoney("1.23a"));
    }

    /**
     * 自动填充 .00
     */
    @Test
    public void test02() {
        Assert.assertEquals("2.00", BigDecimalUtil.fmtMoney("2"));
    }

    /**
     * 去除小数点2位以后的小数
     */
    @Test
    public void test03() {
        Assert.assertEquals("2.12", BigDecimalUtil.fmtMoney("2.123"));
        Assert.assertEquals("2.12", BigDecimalUtil.fmtMoney("2.124"));
        Assert.assertEquals("2.12", BigDecimalUtil.fmtMoney("2.125"));
        Assert.assertEquals("2.12", BigDecimalUtil.fmtMoney("2.126"));
    }

    /**
     * 输入为空，返回空串
     */
    @Test
    public void test04() {
        Assert.assertEquals("", BigDecimalUtil.fmtMoney(null));
    }

}

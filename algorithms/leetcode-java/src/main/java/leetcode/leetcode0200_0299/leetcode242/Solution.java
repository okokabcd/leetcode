package leetcode.leetcode0200_0299.leetcode242;

/**
 * @author yangdc
 * @date 2022/8/25
 */
public class Solution {
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] m = new int[26];
        for (char c : s.toCharArray()) {
            m[c - 'a']++;
        }
        for (char c : t.toCharArray()) {
            if (--m[c - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
}

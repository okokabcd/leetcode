package leetcode.leetcode0200_0299.leetcode219;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/10/12
 */
public class Solution {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i]) && (i - map.get(nums[i]) <= k)) {
                return true;
            } else {
                map.put(nums[i], i);
            }
        }
        return false;
    }
}

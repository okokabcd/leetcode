package leetcode.leetcode0200_0299.leetcode232;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/8/7
 */
class MyQueueByList {
    List<Integer> list;

    public MyQueueByList() {
        list = new ArrayList();
    }

    public void push(int x) {
        list.add(x);
    }

    public int pop() {
        if (list.size() > 0) {
            int ret = list.get(0);
            list.remove(0);
            return ret;
        }
        return 0;

    }

    public int peek() {
        if (list.size() > 0) {
            return list.get(0);
        }
        return 0;
    }

    public boolean empty() {
        return list.isEmpty();
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */

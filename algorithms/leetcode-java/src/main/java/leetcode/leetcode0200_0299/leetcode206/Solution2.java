package leetcode.leetcode0200_0299.leetcode206;

/**
 * @author yangdc
 * @date 2022/9/1
 */
public class Solution2 {
    public ListNode reverseList(ListNode head) {
        ListNode prev = null;
        return reverseList(head, prev);
    }

    public ListNode reverseList(ListNode head, ListNode prev) {
        if (head == null) {
            return prev;
        }
        ListNode next = head.next;
        head.next = prev;
        return reverseList(next, head);
    }
}

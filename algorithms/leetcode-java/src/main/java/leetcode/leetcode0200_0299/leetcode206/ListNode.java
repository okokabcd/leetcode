package leetcode.leetcode0200_0299.leetcode206;

/**
 * @author yangdc
 * @date 2022/9/1
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

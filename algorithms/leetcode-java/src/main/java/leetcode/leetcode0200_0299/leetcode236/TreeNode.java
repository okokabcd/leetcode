package leetcode.leetcode0200_0299.leetcode236;

/**
 * @author yangdc
 * @date 2022/10/10
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int val) {
        this.val = val;
    }
}

package leetcode.leetcode0200_0299.leetcode225;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author yangdc
 * @date 2022/8/19
 */
public class MyStack {

    Queue<Integer> q;

    public MyStack() {
        // int size()：获取队列长度；
        //boolean add(E)/boolean offer(E)：添加元素到队尾；
        //E remove()/E poll()：获取队首元素并从队列中删除；
        //E element()/E peek()：获取队首元素但并不从队列中删除。
        q = new LinkedList<>();
    }

    public void push(int x) {
        q.add(x);
        for (int i = 0; i < q.size() - 1; i++) {
            q.add(q.poll());
        }
    }

    public int pop() {
        return q.poll();
    }

    public int top() {
        return q.peek();
    }

    public boolean empty() {
        return q.isEmpty();
    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.top();
 * boolean param_4 = obj.empty();
 */

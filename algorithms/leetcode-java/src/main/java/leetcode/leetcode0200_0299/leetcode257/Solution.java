package leetcode.leetcode0200_0299.leetcode257;

/**
 * @author yangdc
 * @date 2022/6/11
 */

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> ans = new ArrayList<>();
        dfs(root, "", ans);
        return ans;
    }

    void dfs(TreeNode node, String path, List<String> ans) {
        boolean end = true;
        if ("".equals(path)) {
            path = String.valueOf(node.val);
        } else {
            path += "->" + node.val;
        }
        if (node.left != null) {
            dfs(node.left, path, ans);
            end = false;
        }
        if (node.right != null) {
            dfs(node.right, path, ans);
            end = false;
        }
        if (end) {
            ans.add(path);
        }
    }
}

package leetcode0200_0299.leetcode257;

/**
 * @author yangdc
 * @date 2024/5/8
 */
public class TreeNode {
    int val;
    leetcode0200_0299.leetcode257.TreeNode left;
    leetcode0200_0299.leetcode257.TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, leetcode0200_0299.leetcode257.TreeNode left, leetcode0200_0299.leetcode257.TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

package leetcode.leetcode0200_0299.leetcode227;

/**
 * @author yangdc
 * @date 2022/8/29
 */
public class Solution {
    public int calculate(String s) {
        return (int) parseExpr(s, 0);
    }

    /**
     * 辅函数 - 递归parse从位置i开始的剩余字符串
     */
    long parseExpr(String s, int i) {
        char op = '+';
        long left = 0;
        long right = 0;
        while (i < s.length()) {
            if (s.charAt(i) != ' ') {
                long n = parseNum(s, i);
                switch (op) {
                    case '+':
                        left += right;
                        right = n;
                        break;
                    case '-':
                        left += right;
                        right = -n;
                        break;
                    case '*':
                        right *= n;
                        break;
                    case '/':
                        right /= n;
                        break;
                }
                if (i < s.length()) {
                    op = s.charAt(i);
                }
            }
            i++;
        }
        return left + right;
    }

    /**
     * 辅函数 - parse从位置i开始的一个数字
     */
    long parseNum(String s, int i) {
        long n = 0;
        while (i < s.length() && Character.isDigit(s.charAt(i))) {
            n = 10 * n + (s.charAt(i++) - '0');
        }
        return n;
    }
}

package leetcode.leetcode0200_0299.leetcode227;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/29
 */
public class CalculateTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
//        输入：s = "3+2*2"
//        输出：7
        Assert.assertEquals(7, solution.calculate("3+2*2"));
    }
}

package leetcode.leetcode0200_0299.leetcode287;

/**
 * @author yangdc
 * @date 2022/10/25
 */
public class Solution {
    public int findDuplicate(int[] nums) {
        int left = 1;
        int right = nums.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            int cnt = 0;
            for (int num : nums) {
                cnt += (num <= mid) ? 1 : 0;
            }
            if (cnt <= mid) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return right;
    }
}

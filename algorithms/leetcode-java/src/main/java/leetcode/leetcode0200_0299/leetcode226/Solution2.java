package leetcode.leetcode0200_0299.leetcode226;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author yangdc
 * @date 2022/9/29
 */
public class Solution2 {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return root;
        }
        Deque<TreeNode> q = new ArrayDeque<>();
        q.push(root);
        while (!q.isEmpty()) {
            TreeNode node = q.pop();
            TreeNode tmp = node.left;
            node.left = node.right;
            node.right = tmp;
            if (node.left != null) {
                q.push(node.left);
            }
            if (node.right != null) {
                q.push(node.right);
            }
        }
        return root;
    }
}

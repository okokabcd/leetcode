package leetcode.leetcode0200_0299.leetcode226;

/**
 * @author yangdc
 * @date 2022/9/29
 */
public class Solution {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return root;
        }
        TreeNode tmp = root.left;
        root.left = invertTree(root.right);
        root.right = invertTree(tmp);
        return root;
    }
}

package leetcode.leetcode0200_0299.leetcode279;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/21
 */
public class NumSquaresTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals(2, solution.numSquares(13));
    }

    @Test
    public void test02() {
        Assert.assertEquals(1, solution.numSquares(1));
    }
}

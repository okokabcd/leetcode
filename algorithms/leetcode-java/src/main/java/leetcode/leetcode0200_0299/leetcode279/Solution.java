package leetcode.leetcode0200_0299.leetcode279;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/21
 */
public class Solution {
    public int numSquares(int n) {
        // 找小于n的完全平方数
        List<Integer> squares = new ArrayList<>();
        for (int i = 1; i < n + 1; i++) {
            int tmp = i * i;
            if (tmp < n + 1) {
                squares.add(tmp);
            } else {
                break;
            }
        }

        int[] dp = new int[n + 1];
        for (int i = 1; i < n + 1; i++) {
            dp[i] = Integer.MAX_VALUE;
        }
        for (int i = 1; i < n + 1; i++) {
            for (int square : squares) {
                if (i < square) {
                    break;
                }
                dp[i] = Math.min(dp[i], dp[i - square] + 1);
            }
        }

        return dp[n];
    }
}

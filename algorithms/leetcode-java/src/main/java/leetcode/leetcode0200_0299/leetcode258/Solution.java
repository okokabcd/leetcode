package leetcode.leetcode0200_0299.leetcode258;

/**
 * https://leetcode-cn.com/problems/add-digits/
 * 258. 各位相加
 *
 * @author yangdc
 * @date 2017年10月5日 上午11:06:31
 */
public class Solution {
    // 树根：https://baike.baidu.com/item/%E6%95%B0%E6%A0%B9/4838735?fr=aladdin
    // 性质：x+9与x的数相同，即一个数加9后它的数根不变
    // 公式：a的数根b = (a-1)%9+1
    public int addDigits(int num) {
        while (num > 9) {
            int[] arr = getInt(num);
            num = 0;
            for (int i : arr) {
                num += i;
            }
        }
        return num;
    }

    public int[] getInt(int num) {
        String str = String.valueOf(num);
        int[] arr = new int[str.length()];
        int i = 0;
        for (char c : str.toCharArray()) {
            arr[i++] = c - '0';
        }
        return arr;
    }
}

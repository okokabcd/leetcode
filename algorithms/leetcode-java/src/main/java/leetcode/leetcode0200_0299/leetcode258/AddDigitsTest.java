package leetcode.leetcode0200_0299.leetcode258;

import org.junit.Assert;
import org.junit.Test;


public class AddDigitsTest {
	@Test
	public void test0() {
		Assert.assertEquals(2, new Solution().addDigits(38));
	}
	
	@Test
	public void test1() {
		Assert.assertEquals(1, new Solution().addDigits(10));
	}
}

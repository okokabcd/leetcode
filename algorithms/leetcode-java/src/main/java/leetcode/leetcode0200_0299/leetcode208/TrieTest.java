package leetcode.leetcode0200_0299.leetcode208;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/9/26
 */
public class TrieTest {
    @Test
    public void test01() {
        Trie trie = new Trie();
        trie.insert("apple");
        // 返回 True
        trie.search("apple");
        // 返回 False
        trie.search("app");
        // 返回 True
        trie.startsWith("app");
        trie.insert("app");
        // 返回 True
        trie.search("app");
    }
}

package leetcode.leetcode0200_0299.leetcode208;

/**
 * @author yangdc
 * @date 2022/9/26
 */
class TrieNode {
   TrieNode[] child;
   boolean isWord;

   TrieNode() {
       this.isWord = false;
       child = new TrieNode[26];
   }
}

class Trie {

    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            int i = c - 'a';
            if (p.child[i] == null) {
                p.child[i] = new TrieNode();
            }
            p = p.child[i];
        }
        p.isWord = true;
    }

    public boolean search(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            int i = c - 'a';
            if (p.child[i] == null) {
                return false;
            }
            p = p.child[i];
        }
        return p.isWord;
    }

    public boolean startsWith(String prefix) {
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            int i = c - 'a';
            if (p.child[i] == null) {
                return false;
            }
            p = p.child[i];
        }
        return true;
    }
}

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */

package leetcode.leetcode0200_0299.leetcode217;

import java.util.HashSet;
import java.util.Set;

/**
 * @author yangdc
 * @date 2021/08/13
 */
class Solution {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> numSet = new HashSet<>();
        for (int tmp : nums) {
            if (numSet.contains(tmp)) {
                return true;
            }
            numSet.add(tmp);
        }
        return false;
    }
}

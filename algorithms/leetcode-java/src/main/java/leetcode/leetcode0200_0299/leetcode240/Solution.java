package leetcode.leetcode0200_0299.leetcode240;

/**
 * @author yangdc
 * @date 2022/8/5
 */
public class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        // 从右上角开始搜索，比target大就向左移，比target小就向下移
        int x = matrix.length - 1;
        int y = 0;
        while (x >=0 && y < matrix[0].length) {
            if (matrix[x][y] == target) {
                return true;
            } else if (matrix[x][y] > target) {
                x--;
            } else {
                y++;
            }
        }
        return false;
    }
}

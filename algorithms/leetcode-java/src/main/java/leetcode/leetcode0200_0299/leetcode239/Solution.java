package leetcode.leetcode0200_0299.leetcode239;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author yangdc
 * @date 2022/8/13
 */
public class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 双端队列，单调递减（队首元素最大，队尾元素最小），存储元素在数组中的位置
        Deque<Integer> dq = new LinkedList<>();
        int[] ans = new int[nums.length - k + 1];
        for (int i = 0; i < nums.length; i++) {
            if (!dq.isEmpty() && dq.getFirst() == i - k) {
                dq.removeFirst();
            }
            while (!dq.isEmpty() && nums[dq.getLast()] < nums[i]) {
                dq.removeLast();
            }
            dq.addLast(i);
            if (i >= k - 1) {
                ans[i - k + 1] = nums[dq.getFirst()];
            }
        }
        return ans;
    }
}

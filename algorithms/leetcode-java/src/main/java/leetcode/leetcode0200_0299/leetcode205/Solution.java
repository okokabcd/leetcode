package leetcode.leetcode0200_0299.leetcode205;

/**
 * @author yangdc
 * @date 2022/8/26
 */
public class Solution {
    public boolean isIsomorphic(String s, String t) {
        int[] sFirstIndex = new int[256];
        int[] tFirstIndex = new int[256];
        for (int i = 0; i < s.length(); i++) {
            if (sFirstIndex[s.charAt(i)] != tFirstIndex[t.charAt(i)]) {
                return false;
            }
            sFirstIndex[s.charAt(i)] = i + 1;
            tFirstIndex[t.charAt(i)] = i + 1;
        }

        return true;
    }
}

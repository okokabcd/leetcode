package leetcode.leetcode0200_0299.leetcode215;

/**
 * @author yangdc
 * @date 2022/5/21
 */
public class Solution {
    public int findKthLargest(int[] nums, int k) {
        int l = 0;
        int r = nums.length - 1;
        // 第k个大，就是第nums.length - k个小
        int target = nums.length - k;
        while (l < r) {
            int mid = quickSelection(nums, l, r);
            if (mid == target) {
                return nums[mid];
            }
            if (mid > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return nums[l];
    }

    /**
     * 快速选择
     */
    private int quickSelection(int[] nums, int l, int r) {
        int i = l + 1;
        int j = r;
        while (true) {
            while (i < r && nums[i] <= nums[l]) {
                i++;
            }
            while (l < j && nums[j] >= nums[l]) {
                j--;
            }
            if (i >= j) {
                break;
            }
            swap(nums, i, j);
        }
        swap(nums, l, j);
        return j;
    }

    private void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

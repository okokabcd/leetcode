package leetcode.leetcode0200_0299.leetcode215;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/21
 */
public class FindKthLargestTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[] nums = new int[]{3, 2, 1, 5, 6, 4};
        int k = 2;
        Assert.assertEquals(5, solution.findKthLargest(nums, k));
    }
}

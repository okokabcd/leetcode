package leetcode.leetcode0200_0299.leetcode204;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/8/1
 */
public class Solution {
    public int countPrimes(int n) {
        if (n <= 2) {
            return 0;
        }
        boolean[] prime = new boolean[n];
        Arrays.fill(prime, true);

        int i = 3;
        int sqrtn = (int) Math.sqrt(n);
        // 偶数一定不是质数
        int count = n / 2;
        while (i <= sqrtn) {
            // 最小质因子一定小于等于开方数
            for (int j = i * i; j < n; j += 2 * i) {
                // 避免偶数和重复遍历
                if (prime[j]) {
                    count--;
                    prime[j] = false;
                }
            }
            do {
                i+= 2;
                // 避免偶数和重复遍历
            } while (i <= sqrtn && !prime[i]);
        }
        return count;
    }
}

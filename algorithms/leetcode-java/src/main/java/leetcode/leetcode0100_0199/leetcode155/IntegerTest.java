package leetcode.leetcode0100_0199.leetcode155;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/8
 */
public class IntegerTest {

    /**
     * 结论：
     * 1、Integer与Integer用==比较的有效范围是[-128,127]
     * 2、Integer与int用==比较时范围不限
     */

    @Test
    public void test001() {
        Integer a = 1;
        Integer b = 1;
        Assert.assertTrue(a == b);
    }

    @Test
    public void test002() {
        Integer a = 127;
        Integer b = 127;
        Assert.assertTrue(a == b);
    }

    @Test
    public void test003() {
        Integer a = 128;
        Integer b = 128;
        Assert.assertFalse(a == b);
    }

    @Test
    public void test004() {
        int a = 128;
        Integer b = 128;
        Assert.assertTrue(a == b);
    }

    @Test
    public void test005() {
        Integer a = -128;
        Integer b = -128;
        Assert.assertTrue(a == b);
    }

    @Test
    public void test006() {
        Integer a = -129;
        Integer b = -129;
        Assert.assertFalse(a == b);
    }

    @Test
    public void test007() {
        int a = -129;
        Integer b = -129;
        Assert.assertTrue(a == b);
    }
}

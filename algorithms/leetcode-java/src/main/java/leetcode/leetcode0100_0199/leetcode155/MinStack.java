package leetcode.leetcode0100_0199.leetcode155;

import java.util.Stack;

/**
 * @author yangdc
 * @date 2022/8/8
 */
class MinStack {
    Stack<Integer> s;
    /**
     * 辅助栈
     */
    Stack<Integer> minS;

    public MinStack() {
        s = new Stack<>();
        minS = new Stack<>();
    }

    public void push(int val) {
        s.push(val);
        // 注意这里是>=
        if (minS.isEmpty() || minS.peek() >= val) {
            minS.push(val);
        }
    }

    public void pop() {
        int val = s.pop();
        if (!minS.isEmpty() && minS.peek() == val) {
            minS.pop();
        }
    }

    public int top() {
        return s.peek();
    }

    public int getMin() {
        return minS.isEmpty() ? 0 : minS.peek();
    }
}

// Integer 与 int 比较

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */

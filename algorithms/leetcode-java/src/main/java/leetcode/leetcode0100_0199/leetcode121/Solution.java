package leetcode.leetcode0100_0199.leetcode121;

/**
 * @author yangdc
 * @date 2022/7/3
 */
public class Solution {
    public int maxProfit(int[] prices) {
        // 记录i位置之前所有价格中的最低价格
        int buy = prices[0];
        // 当前最大利润
        int profit = 0;
        for (int i = 1; i < prices.length; i++) {
            buy = Math.min(buy, prices[i]);
            profit = Math.max(profit, prices[i] - buy);
        }
        return profit;
    }
}

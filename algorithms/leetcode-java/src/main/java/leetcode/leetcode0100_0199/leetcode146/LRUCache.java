package leetcode.leetcode0100_0199.leetcode146;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/10/14
 */
class LRUCache {
    Map<Integer, Pair> hash;
    List<Pair> cache;
    int size;

    public LRUCache(int capacity) {
        hash = new HashMap<>();
        cache = new ArrayList<>();
        this.size = capacity;
    }

    public int get(int key) {
        if (!hash.containsKey(key)) {
            return -1;
        }
        Pair pair = hash.get(key);
        cache.remove(pair);
        cache.add(0, pair);
        return pair.value;
    }

    public void put(int key, int value) {
        Pair newPair = new Pair(key, value);
        if (hash.containsKey(key)) {
            Pair oldPair = hash.get(key);
            cache.remove(oldPair);
            cache.add(0, newPair);
            hash.put(key, newPair);
            return;
        }

        if (cache.size() >= size) {
            Pair pair = cache.get(cache.size() - 1);
            cache.remove(pair);
            hash.remove(pair.key);
        }
        hash.put(key, newPair);
        cache.add(0, newPair);
    }

    class Pair{
        int key;
        int value;

        Pair(int key, int value) {
            this.key = key;
            this.value = value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */

package leetcode.leetcode0100_0199.leetcode135;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/7/13
 */
public class Solution {
    public int candy(int[] ratings) {
        int size = ratings.length;
        if (size < 2) {
            return size;
        }
        int[] num = new int[size];
        Arrays.fill(num, 1);
        for (int i = 1; i < size; i++) {
            if (ratings[i] > ratings[i - 1]) {
                num[i] = num[i - 1] + 1;
            }
        }
        for (int i = size - 1; i > 0; i--) {
            if (ratings[i] < ratings[i - 1]) {
                num[i - 1] = Math.max(num[i - 1], num[i] + 1);
            }
        }
        return Arrays.stream(num).sum();
    }
}

package leetcode.leetcode0100_0199.leetcode198;

/**
 * @author yangdc
 * @date 2022/6/16
 */
public class Solution {
    public int rob(int[] nums) {
        int pre2 = 0;
        int pre1 = 0;
        int cur = 0;
        for (int i = 0; i < nums.length; i++) {
            cur = Math.max(pre2 + nums[i], pre1);
            pre2 = pre1;
            pre1 = cur;
        }
        return cur;
    }
}

package leetcode.leetcode0100_0199.leetcode198;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/16
 */
public class RobTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals(4, solution.rob(new int[]{1, 2, 3, 1}));
    }

    @Test
    public void test02() {
        Assert.assertEquals(1, solution.rob(new int[]{1}));
    }
}

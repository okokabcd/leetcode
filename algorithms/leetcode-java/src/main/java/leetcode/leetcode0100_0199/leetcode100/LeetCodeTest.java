package leetcode.leetcode0100_0199.leetcode100;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/1/25
 */
public class LeetCodeTest {
    Solution solution = new Solution();

    @Test
    public void test001() {
        Solution.TreeNode p = new Solution().new TreeNode();
        p.val = 1;
        p.left = new Solution().new TreeNode();
        p.left.val = 2;
        p.right = new Solution().new TreeNode();
        p.right.val = 3;

        Solution.TreeNode q = new Solution().new TreeNode();
        q.val = 1;
        q.left = new Solution().new TreeNode();
        q.left.val = 2;
        q.right = new Solution().new TreeNode();
        q.right.val = 3;

        Assert.assertTrue(solution.isSameTree(p, q));
    }

    @Test
    public void test002() {
        Solution.TreeNode p = new Solution().new TreeNode();
        p.val = 1;
        p.left = new Solution().new TreeNode();
        p.left.val = 2;

        Solution.TreeNode q = new Solution().new TreeNode();
        q.val = 1;
        q.right = new Solution().new TreeNode();
        q.right.val = 2;

        Assert.assertFalse(solution.isSameTree(p, q));
    }
}

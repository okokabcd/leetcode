package leetcode.leetcode0100_0199.leetcode100;

/**
 * 100. 相同的树
 * https://leetcode-cn.com/problems/same-tree/
 *
 * @author yangdc
 * @date 2022/1/25
 */
public class Solution {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        // 结构不一样
        if (p == null && q != null) {
            return false;
        }
        if (p != null && q == null) {
            return false;
        }
        if (p == null && q == null) {
            return true;
        }

        // 值不一样
        if (p.val != q.val) {
            return false;
        }
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}

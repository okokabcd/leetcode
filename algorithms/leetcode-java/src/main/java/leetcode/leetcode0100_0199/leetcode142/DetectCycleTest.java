package leetcode.leetcode0100_0199.leetcode142;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/19
 */
public class DetectCycleTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Solution.ListNode head = new Solution.ListNode(3);
        head.next = new Solution.ListNode(2);
        head.next.next = new Solution.ListNode(0);
        head.next.next.next = new Solution.ListNode(-4);
        head.next.next.next.next = head.next;

        Assert.assertEquals(head.next, solution.detectCycle(head));
    }

    @Test
    public void test02() {
        Solution.ListNode head = new Solution.ListNode(1);
        head.next = new Solution.ListNode(2);
        head.next.next = head;

        Assert.assertEquals(head, solution.detectCycle(head));
    }

    @Test
    public void test03() {
        Solution.ListNode head = new Solution.ListNode(1);
        Assert.assertNull(solution.detectCycle(head));
    }
}

package leetcode.leetcode0100_0199.leetcode148;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/09/04
 */
public class SortListTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        // [4,2,1,3]
        ListNode head = getListNode(new int[]{4, 2, 1, 3});
        ListNode ans = solution.sortList(head);
        printListNode(ans);
    }

    void printListNode(ListNode head) {
        ListNode tmp = head;
        while (tmp != null) {
            System.out.print(tmp.val + ",");
            tmp = tmp.next;
        }
        System.out.println();
    }

    ListNode getListNode(int[] arr) {
        ListNode dummy = new ListNode();
        ListNode tail = dummy;
        for (int i : arr) {
            tail.next = new ListNode(i);
            tail = tail.next;
        }
        return dummy.next;
    }
}

package leetcode.leetcode0100_0199.leetcode148;

/**
 * @author yangdc
 * @date 2022/09/03
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
package leetcode.leetcode0100_0199.leetcode106;

/**
 * @author yangdc
 * @date 2022/10/7
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
    }
}

package leetcode.leetcode0100_0199.leetcode106;

/**
 * @author yangdc
 * @date 2022/10/7
 */
public class Solution {
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return helper(inorder, 0, inorder.length - 1, postorder, 0, postorder.length - 1);
    }

    TreeNode helper(int[] in, int inL, int inR, int[] post, int postL, int postR) {
        if (inL > inR || postL > postR) {
            return null;
        }
        TreeNode node = new TreeNode(post[postR]);
        int i = 0;
        for (i = inL; i < in.length; i++) {
            if (in[i] == node.val) {
                break;
            }
        }
        node.left = helper(in, inL, i - 1, post, postL, postL + i - inL - 1);
        node.right = helper(in, i + 1, inR, post, postL + i - inL, postR - 1);
        return node;
    }
}

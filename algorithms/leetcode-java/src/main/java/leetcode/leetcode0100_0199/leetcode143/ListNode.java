package leetcode.leetcode0100_0199.leetcode143;

/**
 * @author yangdc
 * @date 2022/5/11
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }

    public static ListNode stringToListNode(String line) {
        ListNode head = new ListNode(0);
        ListNode cur = head;
        for (String tmp : line.split(",")) {
            // ListNode tmpNode = cur;
            cur.next = new ListNode(Integer.parseInt(tmp));
            cur = cur.next;
        }

        return head.next;
    }

    public void println() {
        ListNode cur = this;
        while (cur != null) {
            System.out.print(cur.val + ",");
            cur = cur.next;
        }
        System.out.println();
    }
}

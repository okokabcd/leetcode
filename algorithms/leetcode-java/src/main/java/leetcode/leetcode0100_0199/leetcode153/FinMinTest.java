package leetcode.leetcode0100_0199.leetcode153;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/28
 */
public class FinMinTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals(1, solution.findMin(new int[]{3, 4, 5, 1, 2}));
    }

    @Test
    public void test02() {
        Assert.assertEquals(1, solution.findMin(new int[]{2, 1}));
    }
}

package leetcode.leetcode0100_0199.leetcode153;

/**
 * @author yangdc
 * @date 2022/5/28
 */
public class Solution {
    public int findMin(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        int min = nums[0];
        while (l <= r) {
            int mid = l + (r - l) / 2;
            // 判断有序区间
            if (nums[mid] >= nums[l]) {
                // 左侧有序
                min = Math.min(nums[l], min);
                l = mid + 1;
            } else {
                // 右侧有序
                min = Math.min(nums[mid], min);
                r = mid - 1;
            }
        }
        return min;
    }
}

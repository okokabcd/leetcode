package leetcode.leetcode0100_0199.leetcode130;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/6/10
 */
public class Solution {
    public void solve(char[][] board) {
        this.m = board.length;
        if (this.m == 0) {
            return;
        }
        this.board = board;
        this.n = board[0].length;

        for (int y = 0; y < m; y++) {
            dfs(0, y);
            dfs(n - 1, y);
        }

        for (int x = 0; x < n; x++) {
            dfs(x, 0);
            dfs(x, m - 1);
        }

        Map<Character, Character> v = new HashMap<>();
        v.put('G', 'O');
        v.put('O', 'X');
        v.put('X', 'X');
        for (int y = 0; y < m; y++) {
            for (int x = 0; x < n; x++) {
                switch (board[y][x]) {
                    case 'G':
                        board[y][x] = 'O';
                        break;
                    case 'O':
                        board[y][x] = 'X';
                        break;
                    case 'X':
                        board[y][x] = 'X';
                }
            }
        }
    }

    private char[][] board;
    private int m;
    private int n;

    private void dfs(int x, int y) {
        if (x < 0 || x >= n || y < 0 || y >= m || board[y][x] != 'O') {
            return;
        }
        board[y][x] = 'G';
        dfs(x - 1, y);
        dfs(x + 1, y);
        dfs(x, y - 1);
        dfs(x, y + 1);
    }
}

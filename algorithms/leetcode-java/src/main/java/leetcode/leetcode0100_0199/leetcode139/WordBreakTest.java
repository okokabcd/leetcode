package leetcode.leetcode0100_0199.leetcode139;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/24
 */
public class WordBreakTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        String s = "leetcode";
        List<String> wordDict = Arrays.asList(new String[]{"leet", "code"});
        Assert.assertTrue(solution.wordBreak(s, wordDict));
        System.out.println(1);
    }
}

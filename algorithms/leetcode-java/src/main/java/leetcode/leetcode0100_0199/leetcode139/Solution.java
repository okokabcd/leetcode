package leetcode.leetcode0100_0199.leetcode139;

import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/24
 */
public class Solution {
    public boolean wordBreak(String s, List<String> wordDict) {
        int n = s.length();
        boolean[] dp = new boolean[n + 1];
        dp[0] = true;
        for (int i = 1; i <= n; i++) {
            for (String tmp : wordDict) {
                int len = tmp.length();
                if (i>=len && tmp.equals(s.substring(i - len, i))) {
                    dp[i] = dp[i] || dp[i - len];
                }
            }
        }
        return dp[n];
    }
}

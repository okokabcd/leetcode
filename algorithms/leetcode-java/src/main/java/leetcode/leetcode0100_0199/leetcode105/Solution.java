package leetcode.leetcode0100_0199.leetcode105;

import java.util.HashMap;
import java.util.Map;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 * int val;
 * TreeNode left;
 * TreeNode right;
 * TreeNode() {}
 * TreeNode(int val) { this.val = val; }
 * TreeNode(int val, TreeNode left, TreeNode right) {
 * this.val = val;
 * this.left = left;
 * this.right = right;
 * }
 * }
 */
class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0) {
            return null;
        }
        Map<Integer, Integer> hash = new HashMap<>();
        for (int i = 0; i < preorder.length; i++) {
            hash.put(inorder[i], i);
        }
        return buildTreeHelper(hash, preorder, 0, preorder.length - 1, 0);
    }

    TreeNode buildTreeHelper(Map<Integer, Integer> hash, int[] preorder, int s0, int e0, int s1) {
        if (s0 > e0) {
            return null;
        }
        int mid = preorder[s1];
        int index = hash.get(mid);
        int leftLen = index - s0 - 1;
        TreeNode node = new TreeNode(mid);
        node.left = buildTreeHelper(hash, preorder, s0, index - 1, s1 + 1);
        node.right = buildTreeHelper(hash, preorder, index + 1, e0, s1 + 2 + leftLen);
        return node;
    }
}

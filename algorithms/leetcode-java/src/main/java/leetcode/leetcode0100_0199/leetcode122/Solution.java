package leetcode.leetcode0100_0199.leetcode122;

/**
 * @author yangdc
 * @date 2022/7/29
 */
public class Solution {
    public int maxProfit(int[] prices) {
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            int tmp = prices[i] - prices[i-1];
            res += tmp > 0 ? tmp : 0;
        }
        return res;
    }
}

package leetcode.leetcode0100_0199.leetcode126;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/6/8
 */
public class FindLaddersTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        solution.findLadders("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log", "cog"));
    }

    @Test
    public void test02() {
        solution.findLadders("a", "c", Arrays.asList("a", "b", "c"));
    }

}

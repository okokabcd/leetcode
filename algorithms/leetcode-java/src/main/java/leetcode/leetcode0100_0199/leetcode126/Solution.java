package leetcode.leetcode0100_0199.leetcode126;

import java.util.*;

/**
 * @author yangdc
 * @date 2022/6/8
 */
public class Solution {
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> ans = new ArrayList<>();
        Set<String> dict = new HashSet<>(wordList);
        // 终点不在字典列表里直接返回
        if (!dict.contains(endWord)) {
            return ans;
        }
        // dict.remove(beginWord);
        // dict.remove(endWord);
        // 记录从起始节点进行DFS的节点
        Set<String> q1 = new HashSet<>();
        q1.add(beginWord);
        // 记录终止节点进行DFS的节点
        Set<String> q2 = new HashSet<>();
        q2.add(endWord);
        // 记录每个节点的可达到下一个节点列表
        Map<String, List<String>> next = new HashMap<>();
        boolean reversed = false;
        boolean found = false;

        while (!q1.isEmpty() && !q2.isEmpty() && !found) {
            // 正向扩 与 反向扩，层次少的扩
            if (q1.size() > q2.size()) {
                swap(q1, q2);
                reversed = !reversed;
            }

            // 字典中的节点只能使用一次
            for (String w : q1) {
                dict.remove(w);
            }
            for (String w : q2) {
                dict.remove(w);
            }

            Set<String> q = new HashSet<>();
            for (String w : q1) {
                String s = w;
                char[] sArr = s.toCharArray();
                for (int i = 0; i < s.length(); i++) {
                    char ch = sArr[i];
                    // 节点的每个字符都有26个方向
                    for (int j = 0; j < 26; j++) {
                        sArr[i] = (char) (j + 'a');
                        s = String.valueOf(sArr);
                        if (q2.contains(s)) {
                            if (reversed) {
                                add(next, s, w);
                            } else {
                                add(next, w, s);
                            }
                            found = true;
                        }
                        if (dict.contains(s)) {
                            if (reversed) {
                                add(next, s, w);
                            } else {
                                add(next, w, s);
                            }
                            q.add(s);
                        }
                    }
                    sArr[i] = ch;
                    s = String.valueOf(sArr);
                }
            }
        }
        if (found) {
            Deque<String> path = new ArrayDeque<>();
            path.add(beginWord);
            backTracking(beginWord, endWord, next, path, ans);
        }
        return ans;
    }

    void swap(Set<String> s1, Set<String> s2) {
        Set<String> tmp = s1;
        s1 = s2;
        s2 = tmp;
    }

    void backTracking(String src, String dst, Map<String, List<String>> next, Deque<String> path, List<List<String>> ans) {
        if (src == dst) {
            ans.add(new ArrayList<>(path));
            return;
        }
        // next.get(str)报空
        if (!next.containsKey(src)) {
            return;
        }
        for (String s : next.get(src)) {
            path.addLast(s);
            backTracking(s, dst, next, path, ans);
            path.removeLast();
        }
    }

    private void add(Map<String, List<String>> map, String key, String value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<>());
        }
        map.get(key).add(value);
    }
}

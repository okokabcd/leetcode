package leetcode.leetcode0100_0199.leetcode191;

/**
 * @author yangdc
 * @date 2022/10/24
 */
public class Solution {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int ans = 0;
        while (n != 0) {
            ans++;
            n = n & (n - 1);
        }
        return ans;
    }
}

package leetcode.leetcode0100_0199.leetcode191;

/**
 * @author yangdc
 * @date 2022/10/24
 */
public class Solution2 {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int ans = 0;
        for (int i = 0; i < 32; i++) {
            ans += n & 1;
            n >>= 1;
        }
        return ans;
    }
}

package leetcode.leetcode0100_0199.leetcode101;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/1/25
 */
public class LeetCodeTest {
    Solution solution = new Solution();

    @Test
    public void test001() {
        // Solution.TreeNode root = new Solution().new TreeNode();
        TreeNode root = new TreeNode();
        root.val = 1;
        root.left = new TreeNode();
        root.left.val = 2;
        root.left.left = new TreeNode();
        root.left.left.val = 3;
        root.left.right = new TreeNode();
        root.left.right.val = 4;
        root.right = new TreeNode();
        root.right.val = 2;
        root.right.left = new TreeNode();
        root.right.left.val = 4;
        root.right.right = new TreeNode();
        root.right.right.val = 3;

        Assert.assertTrue(solution.isSymmetric(root));
    }

    @Test
    public void test002() {
        TreeNode root = new TreeNode();
        root.val = 1;
        root.left = new TreeNode();
        root.left.val = 2;
        root.left.right = new TreeNode();
        root.left.right.val = 3;
        root.right = new TreeNode();
        root.right.val = 2;
        root.right.right = new TreeNode();
        root.right.right.val = 3;

        Assert.assertFalse(solution.isSymmetric(root));
    }
}

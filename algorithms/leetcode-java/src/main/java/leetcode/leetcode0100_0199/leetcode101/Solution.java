package leetcode.leetcode0100_0199.leetcode101;

/**
 * 101. 对称二叉树
 * https://leetcode-cn.com/problems/symmetric-tree/
 *
 * @author yangdc
 * @date 2022/1/25
 */
public class Solution {

    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isMirror(root.left, root.right);
    }

    public boolean isMirror(TreeNode left, TreeNode right) {
        // (1)如果两个子树都为空指针，则它们相等或对称
        if (left == null && right == null) {
            return true;
        }
        // (2) 如果两个子树只有一个为空指针，则它们不相等或不对称
        if (left == null && right != null) {
            return false;
        }
        if (left != null && right == null) {
            return false;
        }
        // (3)如果两个子树根节点的值不相等， 则它们不相等或不对称
        if (left != null && right != null && left.val != right.val) {
            return false;
        }
        // (4)根据相等或对称要求，进行递归处理。
        return isMirror(left.left, right.right) && isMirror(left.right, right.left);
    }
}

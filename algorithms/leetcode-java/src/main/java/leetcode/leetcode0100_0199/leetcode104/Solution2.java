package leetcode.leetcode0100_0199.leetcode104;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author yangdc
 * @date 2022/9/5
 */
public class Solution2 {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int ans = 0;
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty()) {
            ans++;
            for (int i = q.size(); i > 0; i--) {
                TreeNode t = q.poll();
                if (t.left != null) {
                    q.offer(t.left);
                }
                if (t.right != null) {
                    q.offer(t.right);
                }
            }
        }
        return ans;
    }
}

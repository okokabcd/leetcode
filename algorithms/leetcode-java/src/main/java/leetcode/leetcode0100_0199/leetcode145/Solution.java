package leetcode.leetcode0100_0199.leetcode145;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/10/9
 */
public class Solution {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        traverse(list, root);
        return list;
    }


    public void traverse(List<Integer> list, TreeNode root) {
        if (root == null) {
            return;
        }
        traverse(list, root.left);
        traverse(list, root.right);
        list.add(root.val);
    }
}

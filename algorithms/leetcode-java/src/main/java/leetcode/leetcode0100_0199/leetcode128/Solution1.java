package leetcode.leetcode0100_0199.leetcode128;

import java.util.HashSet;
import java.util.Set;

/**
 * @author yangdc
 * @date 2022/8/16
 */
public class Solution1 {
    public int longestConsecutive(int[] nums) {
        Set<Integer> intSet = new HashSet<>();
        for (int num : nums) {
            intSet.add(num);
        }
        int ans = 0;
        while (!intSet.isEmpty()) {
            int cur = intSet.stream().findFirst().get();
            intSet.remove(cur);
            int pre = cur - 1;
            int next = cur + 1;
            while(intSet.contains(pre)) {
                intSet.remove(pre--);
            }
            while(intSet.contains(next)) {
                intSet.remove(next++);
            }
            ans = Math.max(ans, next - pre - 1);
        }
        return ans;
    }
}

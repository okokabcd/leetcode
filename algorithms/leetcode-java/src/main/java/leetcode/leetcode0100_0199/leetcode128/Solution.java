package leetcode.leetcode0100_0199.leetcode128;

import java.util.HashSet;
import java.util.Set;

/**
 * @author yangdc
 * @date 2022/8/16
 */
public class Solution {
    public int longestConsecutive(int[] nums) {
        Set<Integer> intSet = new HashSet<>();
        for (int num : nums) {
            intSet.add(num);
        }
        int ans = 0;
        for (int num : nums) {
            if (intSet.remove(num)) {
                int pre = num - 1;
                int next = num + 1;
                while (intSet.remove(pre)) {
                    pre--;
                }
                while (intSet.remove(next)) {
                    next++;
                }
                ans = Math.max(ans, next - pre - 1);
            }
        }
        return ans;
    }
}

package leetcode.leetcode0100_0199.leetcode128;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/16
 */
public class LongestConsecutiveTest {
    Solution1 solution = new Solution1();

    @Test
    public void test01() {
        int[] nums = {100,4,200,1,3,2};
        Assert.assertEquals(4, solution.longestConsecutive(nums));
    }
}

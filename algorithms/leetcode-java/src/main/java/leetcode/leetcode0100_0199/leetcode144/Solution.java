package leetcode.leetcode0100_0199.leetcode144;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/9/23
 */
class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        // 先序遍历：中左右
        List<Integer> ans = new ArrayList<>();
        preorder(root, ans);
        return ans;
    }

    void preorder(TreeNode node, List<Integer> ans) {
        if (node == null) {
            return;
        }
        ans.add(node.val);
        preorder(node.left, ans);
        preorder(node.right, ans);
    }
}

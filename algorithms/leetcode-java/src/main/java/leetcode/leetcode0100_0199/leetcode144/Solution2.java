package leetcode.leetcode0100_0199.leetcode144;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/9/23
 */
public class Solution2 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }
        // 使用栈来辅助实现，Java中用双端队列来代替栈
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.addFirst(root);
        while (!stack.isEmpty()) {
            TreeNode tmp = stack.pop();
            ans.add(tmp.val);
            if (tmp.right != null) {
                stack.addFirst(tmp.right);
            }
            if (tmp.left != null) {
                stack.addFirst(tmp.left);
            }
        }
        return ans;
    }
}

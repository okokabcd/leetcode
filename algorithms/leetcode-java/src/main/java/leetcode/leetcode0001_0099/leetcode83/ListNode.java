package leetcode.leetcode0001_0099.leetcode83;

/**
 * @author yangdc
 * @date 2022/09/04
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

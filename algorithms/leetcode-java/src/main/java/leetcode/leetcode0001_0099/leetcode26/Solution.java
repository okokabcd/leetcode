package leetcode.leetcode0001_0099.leetcode26;

/**
 * @author: yangdc
 * @date: 9/8/2021 2:07 PM
 */
class Solution {
    public static void main(String[] args) {
        new Solution().test01();
    }

    public void test01() {
        int[] nums = new int[]{0,0,1,1,1,2,2,3,3,4};
        int result = removeDuplicates(nums);
        System.out.println(result);
    }

    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        if (nums.length < 2) {
            return nums.length;
        }
        int i = 0;
        int j = 0;
        while (j < nums.length) {
            if (i == 0 || nums[j] != nums[i - 1]) {
                nums[i++] = nums[j++];
            } else {
                j++;
            }
        }
        return i;
    }
}

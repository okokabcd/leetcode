package leetcode.leetcode0001_0099.leetcode03;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yangdc
 * @date: 2019/12/14 10:01
 */
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        // 字符串为空或为空串
        if (s == null || s.length() < 1) {
            return 0;
        }
        // 字符串长度大于0
        Map<Character, Integer> cMap = new HashMap<>();
        int startIdx = 0;
        int maxLen = 1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (cMap.containsKey(c)) {
                // 例如："abcabcbb"，遍历到第2个a时，开始索引变为第1个a的下一个索引值
                int tmpStartIdx = cMap.get(c) + 1;
                // 例如："abba"，取最大的索引
                startIdx = startIdx > tmpStartIdx ? startIdx : tmpStartIdx;
            }
            cMap.put(c, i);
            int tmpMaxLen = i - startIdx + 1;
            maxLen = tmpMaxLen > maxLen ? tmpMaxLen : maxLen;
        }
        return maxLen;
    }
}

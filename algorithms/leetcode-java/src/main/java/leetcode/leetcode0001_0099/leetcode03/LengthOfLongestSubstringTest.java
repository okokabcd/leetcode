package leetcode.leetcode0001_0099.leetcode03;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/15
 */
public class LengthOfLongestSubstringTest {
    Solution2 solution = new Solution2();
    @Test
    public void test001() {
        String str = "abcabcbb";
        Assert.assertEquals(3, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test002() {
        String str = "bbbbb";
        Assert.assertEquals(1, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test003() {
        String str = "pwwkew";
        Assert.assertEquals(3, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test004() {
        String str = "";
        Assert.assertEquals(0, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test005() {
        String str = " ";
        Assert.assertEquals(1, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test006() {
        String str = "au";
        Assert.assertEquals(2, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test007() {
        String str = "abba";
        Assert.assertEquals(2, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test008() {
        String str = "aa";
        Assert.assertEquals(1, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test009() {
        String str = "aab";
        Assert.assertEquals(2, solution.lengthOfLongestSubstring(str));
    }

    @Test
    public void test010() {
        String str = "dvdf";
        Assert.assertEquals(3, solution.lengthOfLongestSubstring(str));
    }
}

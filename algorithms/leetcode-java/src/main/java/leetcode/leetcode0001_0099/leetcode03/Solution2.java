package leetcode.leetcode0001_0099.leetcode03;

/**
 * @author yangdc
 * @date 2022/5/15
 */
public class Solution2 {
    public int lengthOfLongestSubstring(String s) {
        int[] chars = new int[128];
        int left = 0;
        int maxSize = 0;
        // 窗口右边界向右滑
        for (int right = 0; right < s.length(); right++) {
            chars[s.charAt(right)]++;
            if (chars[s.charAt(right)] == 1) {
                maxSize = Math.max(right - left + 1, maxSize);
            } else {
                // 窗口左边界向右滑
                while(chars[s.charAt(right)] != 1) {
                    chars[s.charAt(left)]--;
                    left++;
                }
            }
        }
        return maxSize;
    }
}

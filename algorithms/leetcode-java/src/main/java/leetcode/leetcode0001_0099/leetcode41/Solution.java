package leetcode.leetcode0001_0099.leetcode41;

import org.junit.Test;

/**
 * @author: yangdc
 * @date: 8/8/2021 7:10 AM
 */
class Solution {
    public static void main(String[] args) {
        new Solution().test01();
    }

    @Test
    public void test01() {
        // int[] nums = new int[]{7,8,9,11,12};
        // int[] nums = new int[]{1, 2, 0};
        int[] nums = new int[]{1};
        // Assert.assertEquals(1, firstMissingPositive(nums));
        System.out.println(firstMissingPositive(nums));
    }

    public int firstMissingPositive(int[] nums) {
        int N = nums.length;
        for (int i = 0; i < N; i++) {
            // 将小于或等于0的数修改为N+1
            if (nums[i] < 1) {
                nums[i] = N + 1;
            }
        }
        for (int i = 0; i < N; i++) {
            // x为元素，将数组中x的序号的值置为负数
            int x = Math.abs(nums[i]);
            if (x <= N) {
                nums[x-1] = -Math.abs(nums[x-1]);
            }
        }
        int fmp = N + 1;
        for (int i = 0; i < N; i++) {
            if (nums[i] > 0) {
                fmp = i + 1;
                break;
            }
        }
        return fmp;
    }
}

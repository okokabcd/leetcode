package leetcode.leetcode0001_0099.leetcode77;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yangdc
 * @date 2022/6/4
 */
public class Solution {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> ans = new ArrayList<>();
        backTracking(ans, new int[k], 0, 1, n, k);
        return ans;
    }

    void backTracking(List<List<Integer>> ans, int[] comb, int count, int pos, int n, int k) {
        if (count == k) {
            ans.add(Arrays.stream(comb).boxed().collect(Collectors.toList()));
            return;
        }
        for (int i = pos; i <= n; i++) {
            // 修改当前节点状态
            comb[count++] = i;
            // 递归子节点
            backTracking(ans, comb, count, i + 1, n, k);
            // 回改当前节点状态
            count--;
        }
    }
}

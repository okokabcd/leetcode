package leetcode.leetcode0001_0099.leetcode15;

import org.junit.Test;

import java.util.*;

/**
 * 超出时间限制
 *
 * @author: yangdc
 * @date: 2019/12/29 19:43
 */
public class Solution3 {
    @Test
    public void test001() {
        int[] nums = new int[]{-1, 0, 1, 2, -1, -4};
        // Assert.assertEquals();
        List<List<Integer>> list = threeSum(nums);
//        for (List<Integer> tmp : list) {
//            for (Integer tmpInt : tmp) {
//                System.out.print(tmpInt + ", ");
//            }
//            System.out.println();
//        }
    }

    public List<List<Integer>> threeSum(int[] nums) {
        Map<Integer, List<Integer>> idxMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int key = 0 - nums[i];
            if (idxMap.containsKey(key)) {
                List<Integer> tmpList = idxMap.get(key);
                tmpList.add(Integer.valueOf(i));
                idxMap.put(key, tmpList);
            } else {
                List<Integer> tmpList = new ArrayList<>();
                tmpList.add(i);
                idxMap.put(key, tmpList);
            }
        }
        Set<String> resultSet = new HashSet<>();
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int key = nums[i] + nums[j];
                if (idxMap.containsKey(key)) {
                    List<Integer> kList = idxMap.get(key);
                    for (Integer k : kList) {
                        if (k > j) {
                            List<Integer> tmpResult = Arrays.asList(nums[i], nums[j], nums[k]);
                            Collections.sort(tmpResult);
                            String tmpResultStr = "";
                            for (Integer tmp : tmpResult) {
                                tmpResultStr += tmp + "_";
                            }
                            tmpResultStr = tmpResultStr.substring(0, tmpResultStr.length() - 1);
                            resultSet.add(tmpResultStr);
                        }
                    }
                }
            }
        }
        List<List<Integer>> retList = new ArrayList<>();
        for (String tmp : resultSet) {
            String[] arr = tmp.split("_");
            retList.add(Arrays.asList(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), Integer.parseInt(arr[2])));
        }
        return retList;
    }
}

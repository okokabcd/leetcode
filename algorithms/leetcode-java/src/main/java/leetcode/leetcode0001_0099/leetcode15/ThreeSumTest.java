package leetcode.leetcode0001_0099.leetcode15;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/10/23
 */
public class ThreeSumTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        solution.threeSum(new int[]{-1,0,1,2,-1,-4});
    }
}

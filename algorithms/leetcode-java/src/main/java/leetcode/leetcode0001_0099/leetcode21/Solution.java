package leetcode.leetcode0001_0099.leetcode21;

/**
 * @author yangdc
 * @date 2022/9/2
 */
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        // 提示信息：
        // 两个链表的节点数目范围是 [0, 50]
        // -100 <= Node.val <= 100
        // l1 和 l2 均按 非递减顺序 排列
        if (list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }
        if (list1.val > list2.val) {
            list2.next = mergeTwoLists(list1, list2.next);
            return list2;
        }
        list1.next = mergeTwoLists(list1.next, list2);
        return list1;
    }
}

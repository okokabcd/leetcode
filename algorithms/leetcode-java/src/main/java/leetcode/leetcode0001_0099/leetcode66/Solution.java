package leetcode.leetcode0001_0099.leetcode66;

/**
 * @author yangdc
 * @date 2021/08/12
 */
class Solution {
    public int[] plusOne(int[] digits) {
        // 题目规定参数为非空数组，这里不做验证
        int carryBit = 1;
        for(int i = digits.length - 1; i >= 0; i--) {
            int tmp = digits[i] + carryBit;
            if (tmp > 9) {
                digits[i] = 0;
                carryBit = 1;
            } else {
                digits[i] = tmp;
                carryBit = 0;
            }
        }

        if (carryBit == 0) {
            return digits;
        }
        int[] digits2 = new int[digits.length + 1];
        digits2[0] = 1;
        for (int i = 1; i < digits2.length; i++) {
            digits2[i] = digits[0];
        }
        return digits2;
    }
}

package leetcode.leetcode0001_0099.leetcode07;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author: yangdc
 * @date: 2019/12/18 22:10
 */
public class Solution {
    @Test
    public void test001() {
        int i = 123;
        Assert.assertEquals(321, reverse(i));
    }

    @Test
    public void test002() {
        int i = -123;
        Assert.assertEquals(-321, reverse(i));
    }

    @Test
    public void test003() {
        int i = 120;
        Assert.assertEquals(21, reverse(i));
    }

    @Test
    public void test004() {
        int i = 0;
        Assert.assertEquals(0, reverse(i));
    }

    @Test
    public void test005() {
        int i = 1534236469;
        //      2147483647
        Assert.assertEquals(0, reverse(i));
    }

    @Test
    public void test006() {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
    }

    @Test
    public void test007() {
        int i = -2147483648;
        Assert.assertEquals(0, reverse(i));
    }

    public int reverse(int x) {
        if (x == 0 || x == -2147483648) {
            return 0;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        int x1 = Math.abs(x);
        int r1 = x1 / 10;
        int r2 = x1 % 10;
        while (r1 > 0) {
            queue.offer(r2);
            r2 = r1 % 10;
            r1 = r1 / 10;
        }
        queue.offer(r2);
        int reverseVal = 0;
        int max = Integer.MAX_VALUE / 10;
        while (!queue.isEmpty()) {
            if (reverseVal > max) {
                reverseVal = 0;
                break;
            }
            reverseVal = reverseVal * 10 + queue.poll();
        }
        return reverseVal * (x1 / x);
    }
}

package leetcode.leetcode0001_0099.leetcode06;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/18 21:31
 */
public class Solution {

    @Test
    public void test001() {
        String str = "LEETCODEISHIRING";
        Assert.assertEquals("LCIRETOESIIGEDHN", convert(str, 3));
    }

    @Test
    public void test002() {
        String str = "LEETCODEISHIRING";
        Assert.assertEquals("LDREOEIIECIHNTSG", convert(str, 4));
    }

    public String convert(String s, int numRows) {
        if (s == null) {
            return "";
        }
        int len = s.length();
        if (len <= numRows || len <= 1 || numRows == 1) {
            return s;
        }
        char[] zigZagChars = new char[len];
        int idx = 0;
        int interval = 2 * numRows - 2;
        for (int row = 0; row < numRows; row++) {
            int step = interval - 2 * row;
            for (int col = row; col < len; col += interval) {
                zigZagChars[idx++] = s.charAt(col);
                if (step > 0 && step < interval && col + step < len) {
                    zigZagChars[idx++] = s.charAt(col + step);
                }
            }
        }
        return new String(zigZagChars);
    }
}

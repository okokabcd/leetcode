package leetcode.leetcode0001_0099.leetcode05;


import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/16 14:33
 */
public class Solution {

    @Test
    public void test001() {
        String s = "babad";
        Assert.assertEquals("aba", longestPalindrome(s));
    }

    @Test
    public void test002() {
        String s = "cbbd";
        Assert.assertEquals("bb", longestPalindrome(s));
    }

    @Test
    public void test003() {
        boolean[] bool = new boolean[1];
        System.out.println(bool[0]);
    }

    public String longestPalindrome(String s) {
        // 注意：判断输入范围
        if (s == null || s.length() == 0) {
            return "";
        }
        // boolean的默认值是false，不用初始化了
        boolean[][] dp = new boolean[s.length()][s.length()];
        int maxSubLen = 0;
        String maxSubStr = null;
        for (int len = 0; len < s.length(); len++) {
            for (int i = 0; i < s.length() - len; i++) {
                int j = i + len;
                if (i == j) {
                    // 单个字符肯定是回文串
                    dp[i][j] = true;
                } else if (j - i == 1) {
                    // 两个字符如果相等也是回文串
                    dp[i][j] = s.charAt(i) == s.charAt(j);
                } else {
                    dp[i][j] = (s.charAt(i) == s.charAt(j)) && dp[i + 1][j - 1];
                }
                if (dp[i][j] && maxSubLen < (j - i + 1)) {
                    maxSubStr = s.substring(i, j + 1);
                }
            }
        }
        return maxSubStr;
    }
}

package leetcode.leetcode0001_0099.leetcode64;

/**
 * @author yangdc
 * @date 2022/6/18
 */
public class Solution {
    public int minPathSum(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int[][] dp = new int[m][n];
        // dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + grid[i][j]
        dp[0][0] = grid[0][0];
        for (int x = 0; x < m; x++) {
            for (int y = 0; y < n; y++) {
                if (x == 0 && y == 0) {
                    dp[x][y] = grid[x][y];
                } else if (x == 0) {
                    dp[x][y] = dp[x][y-1] + grid[x][y];
                } else if (y == 0) {
                    dp[x][y] = dp[x-1][y] + grid[x][y];
                } else {
                    dp[x][y] = Math.min(dp[x-1][y], dp[x][y-1]) + grid[x][y];
                }
            }
        }
        return dp[m - 1][n - 1];
    }
}

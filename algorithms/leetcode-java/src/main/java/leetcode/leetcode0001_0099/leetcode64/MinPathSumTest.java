package leetcode.leetcode0001_0099.leetcode64;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/18
 */
public class MinPathSumTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals(7, solution.minPathSum(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}));
    }
}

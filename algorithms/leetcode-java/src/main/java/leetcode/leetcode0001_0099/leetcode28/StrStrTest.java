package leetcode.leetcode0001_0099.leetcode28;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/30
 */
public class StrStrTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
//        输入：haystack = "hello", needle = "ll"
//        输出：2
        String haystack = "hello";
        String needle = "ll";
        Assert.assertEquals(2, solution.strStr(haystack, needle));
    }

    @Test
    public void test02() {
        String haystack = "mississippi";
        String needle = "issip";
        Assert.assertEquals(4, solution.strStr(haystack, needle));
    }
}

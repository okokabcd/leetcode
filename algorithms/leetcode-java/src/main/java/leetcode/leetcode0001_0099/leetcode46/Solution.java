package leetcode.leetcode0001_0099.leetcode46;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yangdc
 * @date 2022/6/3
 */
public class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        backTracking(nums, 0, ans);
        return ans;
    }

    void backTracking(int[] nums, int level, List<List<Integer>> ans) {
        if (level == nums.length - 1) {
            ans.add(Arrays.stream(nums).boxed().collect(Collectors.toList()));
            return;
        }
        for (int i = level; i < nums.length; i++) {
            // 修改当前节点状态
            swap(nums, i, level);
            // 递归子节点
            backTracking(nums, level + 1, ans);
            // 回改当前节点状态
            swap(nums, i, level);
        }
    }

    void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

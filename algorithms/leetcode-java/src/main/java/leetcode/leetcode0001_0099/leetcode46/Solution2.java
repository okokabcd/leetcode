package leetcode.leetcode0001_0099.leetcode46;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/3
 */
public class Solution2 {
    List<List<Integer>> res = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        traceBack(nums, new int[nums.length], new ArrayList<>());
        return res;
    }

    public void traceBack(int[] nums, int[] used, List<Integer> list) {
        if (list.size() == nums.length) {
            res.add(new ArrayList<>(list));
            return;
        }
        for (int i = 0; i < nums.length; ++i) {
            if (used[i] == 1) {
                continue;
            }
            list.add(nums[i]);
            used[i] = 1;
            traceBack(nums, used, list);
            used[i] = 0;
            list.remove(list.size() - 1);
        }
    }
}

package leetcode.leetcode0001_0099.leetcode01;

import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/13 10:40
 */
public class Solution3 {

    @Test
    public void test001() {
        ListNode l1 = getListNode(new int[]{1, 8});
        ListNode l2 = getListNode(new int[]{0});

//        ListNode l1 = getListNode(new int[]{2, 4, 3});
//        ListNode l2 = getListNode(new int[]{5, 6, 4});

//        ListNode l1 = getListNode(new int[]{5});
//        ListNode l2 = getListNode(new int[]{5});

        printListNode(l1);
        printListNode(l2);
        ListNode retNode = addTwoNumbers(l1, l2);
        printListNode(retNode);
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int add = 0;
        ListNode retNode = new ListNode(0);
        ListNode curL1 = l1;
        ListNode curL2 = l2;
        ListNode curRet = retNode;
        // 2 考虑l1与l2长度不一样
        while (curL1 != null || curL2 != null) {
            curRet.next = new ListNode(0);
            curRet = curRet.next;
            int tmpSum = (curL1 != null ? curL1.val : 0) + (curL2 != null ? curL2.val : 0) + add;
            add = tmpSum / 10;
            curRet.val = tmpSum % 10;
            // 3 考虑l1或l2为空的情况
            curL1 = curL1 == null ? curL1 : curL1.next;
            curL2 = curL2 == null ? curL2 : curL2.next;
        }
        // 1 考虑最高位进位
        if (add > 0) {
            curRet.next = new ListNode(add);
        }
        return retNode.next;
    }

    public void printListNode(ListNode node) {
        ListNode cur = node;
        String line = "[";
        while (cur != null) {
            line += "," + cur.val;
            cur = cur.next;
        }
        line = line.replaceFirst("\\,", "");
        line += "]";
        System.out.println(line);
    }

    public ListNode getListNode(int[] vals) {
        ListNode retNode = new ListNode(0);
        ListNode curNode = retNode;
        for (int val : vals) {
            curNode.next = new ListNode(val);
            curNode = curNode.next;
        }
        return retNode.next;
    }

    class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}

package leetcode.leetcode0001_0099.leetcode01;

import org.junit.Assert;
import org.junit.Test;

/**
 * 1. 两数之和 测试
 *
 * @author yangdc
 * @date 2022/01/22
 */
public class TwoSumTest {
    Solution5 solution = new Solution5();

    @Test
    public void test01() {
        Assert.assertArrayEquals(new int[]{0, 1}, solution.twoSum(new int[]{2, 7, 11, 15}, 9));
    }

    @Test
    public void test02() {
        Assert.assertArrayEquals(new int[]{1, 2}, solution.twoSum(new int[]{3, 2, 4}, 6));
    }
}

package leetcode.leetcode0001_0099.leetcode01;

import java.util.HashMap;
import java.util.Map;

/**
 * 2. 两数之和
 *
 * @author yangdc
 * @date 2022/01/22
 */
public class Solution2 {
    public int[] twoSum(int[] nums, int target) {
        // 构造k-v
        Map<Integer, Integer> kv = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (kv.containsKey((target - nums[i]))) {
                return new int[]{i, kv.get(target - nums[i])};
            }
            kv.put(nums[i], i);
        }
        return null;
    }
}

package leetcode.leetcode0001_0099.leetcode01;

import java.util.HashMap;
import java.util.Map;

public class Solution5 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> hash = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hash.containsKey(target - nums[i])) {
                return new int[]{i, hash.get(target - nums[i])};
            }
            hash.put(nums[i], i);
        }
        return null;
    }
}

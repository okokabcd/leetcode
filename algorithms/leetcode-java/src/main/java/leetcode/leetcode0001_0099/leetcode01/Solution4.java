package leetcode.leetcode0001_0099.leetcode01;

/**
 * @author yangdc
 * @date 2022/5/10
 */
public class Solution4 {
    // 用双指针法解决此问题的前提是数组已排序
    public int[] twoSum(int[] nums, int target) {
        int i = 0;
        int j = nums.length - 1;
        while (i < j) {
            int tmp = nums[i] + nums[j];
            if (tmp > target) {
                j--;
            } else if (tmp > target) {
                i++;
            } else {
                return new int[]{i, j};
            }
        }
        return null;
    }
}

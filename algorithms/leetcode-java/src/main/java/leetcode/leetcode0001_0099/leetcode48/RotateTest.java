package leetcode.leetcode0001_0099.leetcode48;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/4
 */
public class RotateTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        // 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
        // 输出：[[7,4,1],[8,5,2],[9,6,3]]
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        solution.rotate(matrix);
        int[][] target = {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}};
        for (int[] tmp : matrix) {
            for (int i : tmp) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
        Assert.assertArrayEquals(target, matrix);
    }
}

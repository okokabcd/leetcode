package leetcode.leetcode0001_0099.leetcode48;

/**
 * @author yangdc
 * @date 2022/8/4
 */
public class Solution {
    public void rotate(int[][] matrix) {
        // 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
        // 输出：[[7,4,1],[8,5,2],[9,6,3]]
        // x表示行 y表示列
        /**
         * i 0->n/2     0->2
         * j i->n-i     i->3-i
         * 00 01 02 03
         *    11 12
         * 从上到下 (j,n-i)
         * 从右到左
         *
         * (j,n-i)<-(i,j)
         * (i,j)<-(n-j,i)
         * (n-j,i)<-(n-i, n-j)
         */
        int temp = 0;
        int n = matrix.length - 1;
        for (int i = 0; i <= n / 2; i++) {
            for (int j = i; j < n - i; j++) {
                // 0行n列
                temp = matrix[j][n - i];
                matrix[j][n - i] = matrix[i][j];
                matrix[i][j] = matrix[n-j][i];
                matrix[n-j][i] = matrix[n-i][n-j];
                matrix[n-i][n-j] = temp;
            }
        }
    }
}

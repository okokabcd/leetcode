package leetcode.leetcode0001_0099.leetcode27;

/**
 * @author: yangdc
 * @date: 9/8/2021 1:20 PM
 */
class Solution {
    public static void main(String[] args) {
        new Solution().test01();
    }

    public void test02() {
        int[] nums = new int[]{3,2,2,3};
        int val = 3;
        int result = removeElement(nums, val);
        System.out.println(result);
    }

    public void test01() {
        int[] nums = new int[]{0,1,2,2,3,0,4,2};
        int val = 2;
        int result = removeElement(nums, val);
        System.out.println(result);
    }

    public int removeElement(int[] nums, int val) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            }
        }
        return i;
    }
}

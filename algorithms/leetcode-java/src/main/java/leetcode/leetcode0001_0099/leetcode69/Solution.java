package leetcode.leetcode0001_0099.leetcode69;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class Solution {
    public int mySqrt(int a) {
        // 防止int越界，用long来存储乘法结果
        long x = a;
        while (x * x > a) {
            x = (x + a / x) / 2;
        }
        return (int) x;
    }
}

package leetcode.leetcode0001_0099.leetcode69;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class Solution2 {
    public int mySqrt(int a) {
        // 注意：单独考虑0，防止除以0的情况
        if (a == 0) {
            return 0;
        }
        int left = 1;
        int right = a;
        int mid;
        int sqrt;
        // 注意：条件是 <=
        while (left <= right) {
            // 注意：这样写可以防止溢出
            mid = left + (right - left) / 2;
            sqrt = a / mid;
            if (sqrt == mid) {
                // 直接返回，mid就是平方根
                return mid;
            } else if (mid > sqrt) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return right;
    }
}

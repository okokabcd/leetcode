package leetcode.leetcode0001_0099.leetcode69;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class MySqrtTest {
    Solution2 solution = new Solution2();

    @Test
    public void test01() {
        Assert.assertEquals(2, solution.mySqrt(4));
    }

    @Test
    public void test02() {
        Assert.assertEquals(2, solution.mySqrt(8));
    }
}

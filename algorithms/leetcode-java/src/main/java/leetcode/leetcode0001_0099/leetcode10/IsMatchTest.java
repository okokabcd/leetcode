package leetcode.leetcode0001_0099.leetcode10;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/7/5
 */
public class IsMatchTest {
    Solution solution = new Solution();

    @Test
    public void test001() {
        String s = "aa";
        String p = "leetcode";
        Assert.assertFalse(solution.isMatch(s, p));
    }

    @Test
    public void test002() {
        String s = "aa";
        String p = "leetcode0001_0100*";
        Assert.assertTrue(solution.isMatch(s, p));
    }

    @Test
    public void test003() {
        String s = "ab";
        String p = ".*";
        Assert.assertTrue(solution.isMatch(s, p));
    }

    @Test
    public void test004() {
        String s = "aab";
        String p = "c*leetcode0001_0100*b";
        Assert.assertTrue(solution.isMatch(s, p));
    }

    @Test
    public void test005() {
        String s = "mississippi";
        String p = "mis*is*p*.";
        Assert.assertFalse(solution.isMatch(s, p));
    }

    @Test
    public void test006() {
        String s = "aasdfasdfasdfasdfas";
        String p = "aasdf.*asdf.*asdf.*asdf.*s";
        Assert.assertTrue(solution.isMatch(s, p));
    }

    @Test
    public void test007() {
        String s = "12345";
        System.out.println(s.substring(0, s.length()));
    }
}

package leetcode.leetcode0001_0099.leetcode70;

public class Solution2 {
    public int climbStairs(int n) {
        if (n == 0) {
            return 1;
        }
        if (n < 3) {
            return n;
        }
        return climbStairs(n - 2) * 2 + climbStairs(n - 3);
    }
}

package leetcode.leetcode0001_0099.leetcode70;

import org.junit.Assert;
import org.junit.Test;

public class ClimbingStairsTest {

	// Solution solution = new Solution();
	Solution2 solution = new Solution2();

	@Test
	public void test0() {
		int n = 1;
		Assert.assertEquals(1, solution.climbStairs(n));
	}
	
	@Test
	public void test1() {
		int n = 2;
		Assert.assertEquals(2, solution.climbStairs(n));
	}
	
	@Test
	public void test2() {
		int n = 3;
		Assert.assertEquals(3, solution.climbStairs(n));
	}
}

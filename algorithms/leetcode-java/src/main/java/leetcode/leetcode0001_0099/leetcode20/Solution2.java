package leetcode.leetcode0001_0099.leetcode20;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author yangdc
 * @date 2023/8/16
 */
public class Solution2 {
    public boolean isValid(String s) {
        // s长度为奇数时可以直接排除
        if (s.length() % 2 == 1) {
            return false;
        }

        // 可以构造一个map实现快速匹配
        Map<Character, Character> patternMap = new HashMap(){{
            put(')', '(');
            put(']', '[');
            put('}', '{');
        }};
        Stack<Character> charStack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (!patternMap.containsKey(c)) {
                charStack.push(c);
                continue;
            }
            if (charStack.isEmpty() || !patternMap.get(c).equals(charStack.pop())) {
                return false;
            }
        }
        return charStack.isEmpty();
    }
}

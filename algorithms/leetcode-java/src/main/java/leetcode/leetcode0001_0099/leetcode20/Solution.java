package leetcode.leetcode0001_0099.leetcode20;

import java.util.Stack;

/**
 * @author yangdc
 * @date 2022/8/9
 */
public class Solution {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (Character c : s.toCharArray()) {
            if (isPush(stack, c)) {
                stack.push(c);
            } else {
                stack.pop();
            }
        }
        return stack.isEmpty();
    }

    private boolean isPush(Stack<Character> stack, Character c) {
        if (stack.isEmpty()) {
            return true;
        }

        // {[(
        boolean isPop = (stack.peek().equals('[') && c.equals(']') ||
                stack.peek().equals('{') && c.equals('}') ||
                stack.peek().equals('(') && c.equals(')'));
        return !isPop;
    }
}

package leetcode.leetcode0001_0099.leetcode13;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yangdc
 * @date: 2019/12/29 18:37
 */
public class Solution {
    @Test
    public void test001() {
        Assert.assertEquals(3, romanToInt("III"));
    }

    @Test
    public void test002() {
        Assert.assertEquals(4, romanToInt("IV"));
    }

    @Test
    public void test003() {
        Assert.assertEquals(9, romanToInt("IX"));
    }

    @Test
    public void test004() {
        Assert.assertEquals(58, romanToInt("LVIII"));
    }

    @Test
    public void test005() {
        Assert.assertEquals(1994, romanToInt("MCMXCIV"));
    }

    public int romanToInt(String s) {
        // 考虑边界
        if (s == null || s.length() == 0) {
            return 0;
        }

        Map<String, Integer> map = new HashMap<>();
        map.put("I", 1);
        map.put("V", 5);
        map.put("X", 10);
        map.put("L", 50);
        map.put("C", 100);
        map.put("D", 500);
        map.put("M", 1000);
        // 特殊情况
        map.put("IV", 4);
        map.put("IX", 9);
        map.put("XL", 40);
        map.put("XC", 90);
        map.put("CD", 400);
        map.put("CM", 900);

        // MCMXCIV
        int result = 0;
        String cur = String.valueOf(s.charAt(0));
        for (int i = 1; i < s.length(); i++) {
            String tmp = cur + String.valueOf(s.charAt(i));
            if (map.containsKey(tmp)) {
                cur = tmp;
            } else {
                result += map.get(cur);
                cur = String.valueOf(s.charAt(i));
            }
        }
        result += map.get(cur);
        return result;
    }
}

package leetcode.leetcode0001_0099.leetcode02;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/13 10:48
 */
public class Solution {
    @Test
    public void test001() {
        int[] nums = new int[]{2, 7, 11, 15};
        int target = 9;
        int[] result = new Solution().twoSum(nums, target);
        Assert.assertArrayEquals(new int[]{0, 1}, result);
    }

    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }
}

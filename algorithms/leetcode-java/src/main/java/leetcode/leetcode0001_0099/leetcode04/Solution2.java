package leetcode.leetcode0001_0099.leetcode04;

/**
 * @author yangdc
 * @date 2022/5/30
 */
public class Solution2 {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int n1 = nums1.length;
        int n2 = nums2.length;
        // 对长度小的数组做二分搜索
        if (n1 > n2 ) {
            return findMedianSortedArrays(nums2, nums1);
        }

        // 偶数情况：nums1=>[1, 2, 3] nums2=>[3, 4, 5]  中位数 2 3
        // 奇数情况：nums1=>[1, 2, 3] nums2=>[2, 3, 4, 5] 中位数 3
        // nums1=>[-1, 1, 3, 5, 7, 9] nums2=>[2, 4, 6, 8, 10, 12, 14, 16]
        int l = 0;
        int r = n1;
        // 偶数情况：左中位数 奇数情况：中位数
        int k = (n1 + n2 + 1) / 2;
        while (l < r) {
            int m1 = l + (r - l) / 2;
            int m2 = k - m1;
            if (nums1[m1] < nums2[m2-1]) {
                l = m1 + 1;
            } else {
                r = m1;
            }
        }
        int m1 = l;
        int m2 = k - l;

        int c1 = Math.max(m1 < 1 ? Integer.MIN_VALUE : nums1[m1-1], m2 < 1 ? Integer.MIN_VALUE : nums2[m2-1]);
        // 奇数情况
        if ((n1 + n2) % 2 == 1) {
            return c1;
        }

        int c2 = Math.min(m1 >= n1 ? Integer.MAX_VALUE : nums1[m1], m2 >= n2 ? Integer.MAX_VALUE : nums2[m2]);
        return (c1 + c2) * 0.5;
    }
}

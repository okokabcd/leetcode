package leetcode.leetcode0001_0099.leetcode04;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/30
 */
public class FindMedianSortedArraysTest {
    Solution2 solution = new Solution2();

    @Test
    public void test001() {
        int[] nums1 = new int[]{1, 3};
        int[] nums2 = new int[]{2};
        Assert.assertEquals(2d, solution.findMedianSortedArrays(nums1, nums2), 2);
    }

    @Test
    public void test002() {
        int[] nums1 = new int[]{1, 2};
        int[] nums2 = new int[]{3, 4};
        Assert.assertEquals(2.5d, solution.findMedianSortedArrays(nums1, nums2), 2);
    }
}

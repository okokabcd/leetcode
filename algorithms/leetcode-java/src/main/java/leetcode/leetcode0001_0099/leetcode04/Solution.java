package leetcode.leetcode0001_0099.leetcode04;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/15 16:09
 */
public class Solution {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int i1 = 0, i2 = 0, i = 0, len = nums1.length + nums2.length;
        int[] arr = new int[len];
        while (true) {
            // 每队选出一个代表
            Integer num1Val = i1 < nums1.length ? nums1[i1] : null;
            Integer num2Val = i2 < nums2.length ? nums2[i2] : null;
            // 开始比赛
            if (num1Val != null) {
                if (num2Val != null) {
                    arr[i++] = num1Val < num2Val ? nums1[i1++] : nums2[i2++];
                } else {
                    arr[i++] = nums1[i1++];
                }
            } else {
                if (num2Val != null) {
                    arr[i++] = nums2[i2++];
                } else {
                    break;
                }
            }
            if (i > len/2) {
                return len % 2 == 0 ? (arr[len / 2 - 1] + arr[len / 2]) / 2d : arr[(len + 1) / 2 - 1] * 1d;
            }
        }
        return 0d;
    }

    private String getArrStr(int[] arr) {
        String str = "[";
        for (int i : arr) {
            str += "," + i;
        }
        str = str.replaceFirst("\\,", "") + "]";
        return str;
    }
}

package leetcode.leetcode0001_0099.leetcode75;

/**
 * @author yangdc
 * @date 2022/5/24
 */
public class Solution2 {
    public void sortColors(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        // 注意结束条件是 i<=right
        for (int i = 0; i <= right; i++) {
            if (nums[i] == 0) {
                swap(nums, i, left++);
            } else if (nums[i] == 2) {
                // 注意此时i要呆在原地，因为for里面会执行i++,此时做i--做消减
                swap(nums, i--, right--);
            }
        }
    }

    void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

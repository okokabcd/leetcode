package leetcode.leetcode0001_0099.leetcode75;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/24
 */
public class SortColorsTest {
    Solution2 solution = new Solution2();

    @Test
    public void test01() {
        int[] nums = new int[]{2, 0, 2, 1, 1, 0};
        solution.sortColors(nums);
        Assert.assertArrayEquals(new int[]{0, 0, 1, 1, 2, 2}, nums);
    }

    @Test
    public void test02() {
        int[] nums = new int[]{2, 0, 1};
        solution.sortColors(nums);
        Assert.assertArrayEquals(new int[]{0, 1, 2}, nums);
    }
}

package leetcode.leetcode0001_0099.leetcode75;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * @author yangdc
 * @date 2022/5/24
 */
public class Solution {
    public void sortColors(int[] nums) {
        // 统计频率
        Map<Integer, Integer> seqMap = new HashMap<>();
        for (int tmp : nums) {
            seqMap.put(tmp, seqMap.getOrDefault(tmp, 0) + 1);
        }

        // 按key排序
        PriorityQueue<Map.Entry<Integer, Integer>> priorityQueue = new PriorityQueue<>((a, b) -> b.getKey() - a.getKey());
        for (Map.Entry<Integer, Integer> entry : seqMap.entrySet()) {
            priorityQueue.add(entry);
        }

        // 提取结果
        int idx = nums.length - 1;
        while (!priorityQueue.isEmpty()) {
            Map.Entry<Integer, Integer> entry = priorityQueue.poll();
            for (int i = 0; i < entry.getValue(); i++) {
                nums[idx--] = entry.getKey();
            }
        }
    }
}

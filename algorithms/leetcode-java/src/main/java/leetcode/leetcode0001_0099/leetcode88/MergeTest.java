package leetcode.leetcode0001_0099.leetcode88;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/10
 */
public class MergeTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[] nums1 = new int[]{1, 2, 3, 0, 0, 0};
        int[] nums2 = new int[]{2, 5, 6};
        solution.merge(nums1, 3, nums2, 3);
        Assert.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 6}, nums1);
    }

    @Test
    public void test02() {
        int[] nums1 = new int[]{0};
        int[] nums2 = new int[]{1};
        solution.merge(nums1, 0, nums2, 1);
        Assert.assertArrayEquals(new int[]{1}, nums1);
    }

    @Test
    public void test03() {
        int[] nums1 = new int[]{2, 0};
        int[] nums2 = new int[]{1};
        solution.merge(nums1, 1, nums2, 1);
        Assert.assertArrayEquals(new int[]{1, 2}, nums1);
    }

    @Test
    public void test04() {
        int[] nums1 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] nums2 = new int[]{-50, -50, -48, -47, -44, -44, -37, -35, -35, -32, -32, -31, -29, -29, -28, -26, -24, -23, -23, -21, -20, -19, -17, -15, -14, -12, -12, -11, -10, -9, -8, -5, -2, -2, 1, 1, 3, 4, 4, 7, 7, 7, 9, 10, 11, 12, 14, 16, 17, 18, 21, 21, 24, 31, 33, 34, 35, 36, 41, 41, 46, 48, 48};
        solution.merge(nums1, 0, nums2, 63);
        Assert.assertArrayEquals(new int[]{-50, -50, -48, -47, -44, -44, -37, -35, -35, -32, -32, -31, -29, -29, -28, -26, -24, -23, -23, -21, -20, -19, -17, -15, -14, -12, -12, -11, -10, -9, -8, -5, -2, -2, 1, 1, 3, 4, 4, 7, 7, 7, 9, 10, 11, 12, 14, 16, 17, 18, 21, 21, 24, 31, 33, 34, 35, 36, 41, 41, 46, 48, 48}, nums1);
    }
}

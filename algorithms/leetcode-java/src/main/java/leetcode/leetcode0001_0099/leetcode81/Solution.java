package leetcode.leetcode0001_0099.leetcode81;

/**
 * @author yangdc
 * @date 2022/5/27
 */
public class Solution {
    public boolean search(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        // 注意：此处是 <=
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] == target) {
                return true;
            }
            // 判断哪个区间是增序的
            if (nums[l] == nums[mid]) {
                // 判断不出来，因为有重复数字
                l++;
                // 注意：此处要跳出循环
                continue;
            }
            if (nums[mid] <= nums[r]) {
                // 右区间是增序的
                if (target > nums[mid] && target <= nums[r]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            } else {
                // 左区间是是增序的
                // 注意：此处是 >=
                if (target >= nums[l] && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            }
        }
        return false;
    }
}

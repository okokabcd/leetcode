package leetcode.leetcode0001_0099.leetcode81;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/27
 */
public class SearchTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertTrue(solution.search(new int[]{2, 5, 6, 0, 0, 1, 2}, 0));
    }

    @Test
    public void test02() {
        Assert.assertFalse(solution.search(new int[]{2, 5, 6, 0, 0, 1, 2}, 3));
    }

    @Test
    public void test03() {
        Assert.assertTrue(solution.search(new int[]{1, 0, 1, 1, 1}, 0));
    }

    @Test
    public void test04() {
        Assert.assertTrue(solution.search(new int[]{3, 5, 1}, 3));
    }


}

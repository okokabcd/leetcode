package leetcode.leetcode0001_0099.leetcode91;

import java.util.*;

/**
 * @author yangdc
 * @date 2022/6/22
 */
public class Solution {
    public int numDecodings(String s) {
        // 1个数字 对应 一个解码 dp(1) = 1
        // 2个数字 对应 dp(2) = dp(1) + (后两位是有效编码? 1 : 0)
        // i个数字 对应 dp(i) = dp(i-1) + (后两位是有效编码? 1 : 0)
        if (s.startsWith("0")) {
            return 0;
        }
        if ("10".equals(s) || "20".equals(s)) {
            return 1;
        }
        List<Integer> dp = new ArrayList<>();
        dp.add(0, 1);
        for (int i = 1; i < s.length(); i++) {
            int count = 0;
            Integer tmp = Integer.parseInt(s.substring(i - 1, i + 1));
            if (tmp < 10) {
                count = dp.get(i - 1);
            } else if (tmp == 20 || tmp == 10) {
                count = dp.get(i - 2);
            } else if (tmp < 27) {
                count = dp.get(i - 1) + 1;
            } else {
                count = dp.get(i - 1);
            }
            dp.add(i, count);
        }
        return dp.get(dp.size() - 1);
    }
}

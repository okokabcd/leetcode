package leetcode.leetcode0001_0099.leetcode91;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/22
 */
public class NumDecodingsTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
//        String str = "abc";
//        System.out.println(str.substring(0, 2));
        Assert.assertEquals(2, solution.numDecodings("12"));
    }

    @Test
    public void test02() {
        Assert.assertEquals(3, solution.numDecodings("226"));
    }

    @Test
    public void test03() {
        Assert.assertEquals(0, solution.numDecodings("0"));
    }

    @Test
    public void test04() {
        Assert.assertEquals(0, solution.numDecodings("06"));
    }

    @Test
    public void test05() {
        Assert.assertEquals(1, solution.numDecodings("10"));
    }

    @Test
    public void test06() {
        Assert.assertEquals(5, solution.numDecodings("1123"));
    }
}

package leetcode.leetcode0001_0099.leetcode79;

/**
 * @author yangdc
 * @date 2022/6/5
 */
public class Solution {
    boolean find = false;
    public boolean exist(char[][] board, String word) {
        if (board.length == 0) {
            return false;
        }
        int m = board.length;
        int n = board[0].length;
        boolean[][] visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                backTracking(i, j, board, word, visited, 0);
            }
        }
        return find;
    }

    void backTracking(int i, int j, char[][] board, String word, boolean[][] visited, int pos) {
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length) {
            return;
        }
        if (visited[i][j] || find || board[i][j] != word.charAt(pos)) {
            return;
        }
        if (pos == word.length() - 1) {
            find = true;
            return;
        }
        visited[i][j] = true;
        backTracking(i + 1, j, board, word, visited, pos + 1);
        backTracking(i - 1, j, board, word, visited, pos + 1);
        backTracking(i, j + 1, board, word, visited, pos + 1);
        backTracking(i, j - 1, board, word, visited, pos + 1);
        visited[i][j] = false;
    }
}

package leetcode.leetcode0001_0099.leetcode34;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/26
 */
public class SearchRangeTest {
    Solution2 solution = new Solution2();

    @Test
    public void test01() {
        Assert.assertArrayEquals(new int[]{-1, -1}, solution.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 6));
    }

    @Test
    public void test02() {
        Assert.assertArrayEquals(new int[]{3, 4}, solution.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8));
    }

    @Test
    public void test03() {
        Assert.assertArrayEquals(new int[]{-1, -1}, solution.searchRange(new int[]{}, 0));
    }

    @Test
    public void test04() {
        Assert.assertArrayEquals(new int[]{0, 0}, solution.searchRange(new int[]{1}, 1));
    }

    @Test
    public void test05() {
        Assert.assertArrayEquals(new int[]{1, 1}, solution.searchRange(new int[]{1, 4}, 4));
    }
}

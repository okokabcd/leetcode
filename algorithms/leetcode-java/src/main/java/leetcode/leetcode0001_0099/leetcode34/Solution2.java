package leetcode.leetcode0001_0099.leetcode34;

/**
 * @author yangdc
 * @date 2022/5/26
 */
public class Solution2 {
    public int[] searchRange(int[] nums, int target) {
        int left = lower_bound(nums, target);
        int right = upper_bound(nums, target);
        if (left == right) {
            return new int[]{-1, -1};
        }
        return new int[]{left, right - 1};
    }

    private int lower_bound(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        int mid;
        while (left < right) {
            mid = left + (right - left) / 2;
            if (nums[mid] >= target) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    private int upper_bound(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        int mid;
        while (left < right) {
            mid = left + (right - left) / 2;
            if (nums[mid] > target) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }
}

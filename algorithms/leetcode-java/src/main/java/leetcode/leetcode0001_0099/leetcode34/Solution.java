package leetcode.leetcode0001_0099.leetcode34;

/**
 * @author yangdc
 * @date 2022/5/26
 */
public class Solution {
    public int[] searchRange(int[] nums, int target) {
        int startIdx = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target && startIdx == -1) {
                startIdx = i;
            }
            if (nums[i] > target && startIdx != -1) {
                return new int[]{startIdx, i - 1};
            }
        }
        if (startIdx != -1) {
            return new int[]{startIdx, nums.length - 1};
        }
        return new int[]{-1, -1};
    }
}

package leetcode.leetcode0001_0099.leetcode08;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/20 21:30
 */
public class Solution {

    @Test
    public void test001() {
        String str = "42";
        Assert.assertEquals(42, myAtoi(str));
    }

    @Test
    public void test002() {
        String str = "   -42";
        // 第一个非空白字符为 '-', 它是一个负号。
        // 我们尽可能将负号与后面所有连续出现的数字组合起来，最后得到 -42
        Assert.assertEquals(-42, myAtoi(str));
    }

    @Test
    public void test003() {
        String str = "4193 with words";
        // 转换截止于数字 '3' ，因为它的下一个字符不为数字。
        Assert.assertEquals(4193, myAtoi(str));
    }

    @Test
    public void test004() {
        String str = "words and 987";
        // 第一个非空字符是 'w', 但它不是数字或正、负号。 因此无法执行有效的转换。
        Assert.assertEquals(0, myAtoi(str));
    }

    @Test
    public void test005() {
        String str = "-91283472332";
        // 数字 "-91283472332" 超过 32 位有符号整数范围。 因此返回 INT_MIN (−231) 。
        Assert.assertEquals(-2147483648, myAtoi(str));
    }

    @Test
    public void test007() {
        String str = "-6147483648";
        Assert.assertEquals(-2147483648, myAtoi(str));
    }

    @Test
    public void test006() {
        // 2147483647
        System.out.println(Integer.MAX_VALUE);
        // -2147483648
        System.out.println(Integer.MIN_VALUE);
        System.out.println("---");

        int i = -2147483648;
        System.out.println(i);
        System.out.println(i - 1);
        System.out.println(-1 * i);
        System.out.println("---");

        int i2 = 2147483647;
        System.out.println(i2);
        System.out.println(i2 + 1);
        System.out.println(i2 * -1);

    }

    public int myAtoi(String str) {
        if (str == null) {
            return 0;
        }
        // 忽略首尾空格
        str = str.trim();
        if (str.length() == 0) {
            return 0;
        }
        // 首字母是数字，正号，负号
        char c = str.charAt(0);
        int iVal = 0;
        int sign = 1;
        if ('-' == c) {
            sign = -1;
        } else if ('+' == c) {
        } else if (c >= '0' && c <= '9') {
            iVal = (c - '0');
        } else {
            return 0;
        }
        for (int i = 1; i < str.length(); i++) {
            char tmpC = str.charAt(i);
            if (tmpC < '0' || tmpC > '9') {
                break;
            }
            int newVal = 10 * iVal + (tmpC - '0');
            if (iVal > newVal || iVal > 214748364) {
                return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            iVal = newVal;
        }
        return sign * iVal;
    }
}

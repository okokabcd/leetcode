package leetcode.leetcode0001_0099.leetcode23;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author yangdc
 * @date 2022/8/11
 */
public class Solution {

    public ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> pq = new PriorityQueue<>(Comparator.comparingInt(o -> o.val));
        for (ListNode node : lists) {
            if (node != null) {
                pq.add(node);
            }
        }

        ListNode ret = null;
        ListNode cur = null;
        while (!pq.isEmpty()) {
            ListNode node = pq.poll();
            if (ret == null) {
                cur = node;
                ret = cur;
            } else {
                cur.next = node;
                cur = cur.next;
            }
            if (node.next != null) {
                pq.add(node.next);
            }
        }
        return ret;
    }

    /**
     * Definition for singly-linked list.
     */
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}

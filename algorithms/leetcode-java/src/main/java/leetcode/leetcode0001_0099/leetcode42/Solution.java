package leetcode.leetcode0001_0099.leetcode42;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2021/08/07
 */
public class Solution {
    @Test
    public void test01() {
        int[] height = new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        Assert.assertEquals(6, trap(height));
    }

    @Test
    public void test02() {
        int[] height = new int[]{4, 2, 0, 3, 2, 5};
        Assert.assertEquals(9, trap(height));
    }


    // 虽然我的效率低，但是不影响我分享
    // 思路2 找最高点，分别从两边往最高点遍历，如果下一个数当前数小说明可以接到水

    /**
     * 思路1 反向遍历，双指针法
     *
     * @param height
     * @return
     */
    public int trap(int[] height) {
        int area = 0;
        int i = 0;
        int j = height.length - 1;

        int globalMinH = 0;
        // 保证两边高度不为0
        while (i < j) {
            if (height[i] == 0) {
                i++;
                continue;
            }
            if (height[j] == 0) {
                j--;
                continue;
            }
            globalMinH = height[i] < height[j] ? height[i] : height[j];
            break;
        }
        int lastGlobalMinH = 0;
        while (i < j) {
            if (height[i] < globalMinH || height[i] <= lastGlobalMinH) {
                i++;
                continue;
            }
            if (height[j] < globalMinH || height[j] <= lastGlobalMinH) {
                j--;
                continue;
            }
            globalMinH = height[i] <= height[j] ? height[i] : height[j];
            if (height[i] <= height[j]) {
                int k = i + 1;
                while (k < j) {
                    int diffH = globalMinH - (height[k] - lastGlobalMinH > 0 ? height[k] : lastGlobalMinH);
                    area += diffH > 0 ? diffH : 0;
                    k++;
                }
                i++;
            } else {
                int k = j - 1;
                while (i < k) {
                    int diffH = globalMinH - (height[k] - lastGlobalMinH > 0 ? height[k] : lastGlobalMinH);
                    area += diffH > 0 ? diffH : 0;
                    k--;
                }
                j--;
            }
            lastGlobalMinH = globalMinH;
        }
        return area;
    }
}

package leetcode.leetcode0001_0099.leetcode76;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/13
 */
public class MinWindowTest2 {
    Solution2 solution = new Solution2();

    @Test
    public void test01() {
        String s = "ADOBECODEBANC";
        String t = "ABC";
        String o = "BANC";
        Assert.assertEquals(o, solution.minWindow(s, t));
    }

    @Test
    public void test02() {
        String s = "ADOBECCODEBANCC";
        String t = "ABCC";
        String o = "BANCC";
        Assert.assertEquals(o, solution.minWindow(s, t));
    }
}

package leetcode.leetcode0001_0099.leetcode76;

import org.junit.Assert;
import org.junit.Test;

public class minWindowTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        String s = "ADOBECODEBANC";
        String t = "ABC";
        String o = "BANC";
        Assert.assertEquals(o, solution.minWindow(s, t));
    }
}

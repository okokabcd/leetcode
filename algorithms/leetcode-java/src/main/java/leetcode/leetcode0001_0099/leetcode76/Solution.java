package leetcode.leetcode0001_0099.leetcode76;

/**
 * @author yangdc
 * @date 2022/5/12
 */
public class Solution {
    public String minWindow(String s, String t) {
        int[] chars = new int[128];
        boolean[] flag = new boolean[128];
        for (int i = 0; i < t.length(); ++i) {
            ++chars[t.charAt(i)];
            flag[t.charAt(i)] = true;
        }
        int l = 0;
        int cnt = 0;
        int minL = 0;
        int minSize = s.length() + 1;
        for (int r = 0; r < s.length(); ++r) {
            if (flag[s.charAt(r)]) {
                if ((--chars[s.charAt(r)]) >= 0) {
                    ++cnt;
                }
                while (cnt == t.length()) {
                    if (r - l + 1 < minSize) {
                        minL = l;
                        minSize = r - l + 1;
                    }
                    if (flag[s.charAt(l)] && (++chars[s.charAt(l)]) > 0) {
                        --cnt;
                    }
                    ++l;
                }
            }
        }
        return minSize > s.length() ? "" : s.substring(minL, minSize);
    }
}

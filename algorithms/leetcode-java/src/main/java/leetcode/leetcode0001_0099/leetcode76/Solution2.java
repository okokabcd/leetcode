package leetcode.leetcode0001_0099.leetcode76;

/**
 * @author yangdc
 * @date 2022/5/13
 */
public class Solution2 {
    public String minWindow(String s, String t) {
        // 定义一个字符频率数组来记录t字符串中每个字符出现的次数
        int[] chars = new int[128];
        boolean[] flag = new boolean[128];
        for (char c : t.toCharArray()) {
            chars[c]++;
            flag[c] = true;
        }

        // 定义左指针
        int left = 0;
        // 记录滑动窗口中包含t中字符的数量
        int cnt = 0;
        // 记录最小滑动窗口的左边界指针
        int minLeft = 0;
        // 记录最小滑动窗口的大小
        int minSize = s.length() + 1;
        // 向右滑动右指针
        for (int right = 0; right < s.length(); right++) {
            char c = s.charAt(right);
            if (flag[c]) {
                chars[c]--;
                if (chars[c] >= 0) {
                    cnt++;
                }
                while (cnt == t.length()) {
                    if (right - left + 1 < minSize) {
                        minLeft = left;
                        minSize = right - left + 1;
                    }
                    if (flag[s.charAt(left)]) {
                        chars[s.charAt(left)]++;
                        if (chars[s.charAt(left)] > 0) {
                            cnt--;
                        }
                    }
                    left++;
                }
            }
        }
        return minSize > s.length() ? "" : s.substring(minLeft, minLeft + minSize);
    }
}

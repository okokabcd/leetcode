package leetcode.leetcode0001_0099.leetcode12;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yangdc
 * @date: 2019/12/23 20:53
 */
public class Solution {

    @Test
    public void test001() {
        int num = 3;
        Assert.assertEquals("III", intToRoman(num));
    }


    @Test
    public void test002() {
        int num = 4;
        Assert.assertEquals("IV", intToRoman(num));
    }


    @Test
    public void test003() {
        int num = 9;
        Assert.assertEquals("IX", intToRoman(num));
    }


    @Test
    public void test004() {
        int num = 58;
        Assert.assertEquals("LVIII", intToRoman(num));
    }

    @Test
    public void test005() {
        int num = 1994;
        Assert.assertEquals("MCMXCIV", intToRoman(num));
    }

    public String intToRoman(int num) {
        // 映射
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "I");
        map.put(5, "V");
        map.put(10, "X");
        map.put(50, "L");
        map.put(100, "C");
        map.put(500, "D");
        map.put(1000, "M");
        // 特殊情况映射
        map.put(4, "IV");
        map.put(9, "IX");
        map.put(40, "XL");
        map.put(90, "XC");
        map.put(400, "CD");
        map.put(900, "CM");

        String roman = "";
        int a = num;
        int b = 0;
        int nTen = 1;
        while (a > 0 || b > 0) {
            b = a % 10;
            a = a / 10;
            int key = nTen * b;
            if (b == 0) {
                // 对应0的情况
            } else if (map.get(key) == null) {
                if (b == 2 || b == 3) {
                    // 对应2，3情况
                    for (int i = 0; i < b; i++) {
                        roman = map.get(nTen) + roman;
                    }
                } else {
                    // 对应6，7，8
                    for (int i = 0; i < b - 5; i++) {
                        roman = map.get(nTen) + roman;
                    }
                    roman = map.get(5 * nTen) + roman;
                }
            } else {
                // 对应1，4，9的情况
                roman = map.get(key) + roman;
            }
            nTen *= 10;
        }
        return roman;
    }
}

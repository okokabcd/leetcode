package leetcode.leetcode0001_0099.leetcode51;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yangdc
 * @date 2022/6/6
 */
public class Solution {
    public List<List<String>> solveNQueens(int n) {
        board = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            // jdk11
            // board.add(".".repeat(n));
             board.add(".".repeat(n).toCharArray());
        }
        cols = new boolean[n];
        diag1 = new boolean[2 * n - 1];
        diag2 = new boolean[2 * n - 1];
        sols = new ArrayList<>();

        nqueens(n, 0);

        return sols;
    }

    /**
     * 记录棋盘
     */
    private List<char[]> board;
    /**
     * 记录第x列是否有皇后
     */
    private boolean[] cols;
    /**
     * 记录第x条正对角线上是否有皇后
     */
    private boolean[] diag1;
    /**
     * 记录第x条反对角线上是否有皇后
     */
    private boolean[] diag2;
    /**
     * 记录解
     */
    private List<List<String>> sols;

    boolean available(int x, int y, int n) {
        return !cols[x] && !diag1[x + y] && !diag2[x - y + n - 1];
    }

    /**
     * 更新棋盘
     *
     * @param x
     * @param y
     * @param n
     * @param put
     */
    void updateBoard(int x, int y, int n, boolean put) {
        cols[x] = put;
        diag1[x + y] = put;
        diag2[x - y + n - 1] = put;
        board.get(y)[x] = put ? 'Q' : '.';
    }

    void nqueens(int n, int y) {
        if (y == n) {
            List<String> tmp =  board.stream().map(t -> String.valueOf(t)).collect(Collectors.toList());
            sols.add(tmp);
            return;
        }
        for (int x = 0; x < n; x++) {
            if (!available(x, y, n)) {
                continue;
            }
            // 更新棋盘
            updateBoard(x, y, n, true);
            nqueens(n, y + 1);
            // 将棋盘还原
            updateBoard(x, y, n, false);
        }
    }
}

package leetcode.leetcode0001_0099.leetcode09;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/20 22:43
 */
public class Solution {

    @Test
    public void test001() {
        int x = 121;
        Assert.assertTrue(isPalindrome(x));
    }

    @Test
    public void test002() {
        int x = -121;
        Assert.assertFalse(isPalindrome(x));
    }

    @Test
    public void test003() {
        int x = 10;
        Assert.assertFalse(isPalindrome(x));
    }

    public boolean isPalindrome(int x) {
        String str = String.valueOf(x);
        for (int i = 0; i < str.length() / 2; i++) {
            int j = str.length() - 1 - i;
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
        }
        return true;
    }
}

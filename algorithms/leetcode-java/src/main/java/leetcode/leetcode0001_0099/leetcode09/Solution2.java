package leetcode.leetcode0001_0099.leetcode09;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/20 22:43
 */
public class Solution2 {

    @Test
    public void test001() {
        int x = 121;
        Assert.assertTrue(isPalindrome(x));
    }

    @Test
    public void test002() {
        int x = -121;
        Assert.assertFalse(isPalindrome(x));
    }

    @Test
    public void test003() {
        int x = 10;
        Assert.assertFalse(isPalindrome(x));
    }

    @Test
    public void test004() {
        int x = 1;
        Assert.assertTrue(isPalindrome(x));
    }

    @Test
    public void test005() {
        int x = 101;
        Assert.assertTrue(isPalindrome(x));
    }

    @Test
    public void test006() {
        int x = 100;
        Assert.assertFalse(isPalindrome(x));
    }

    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        if (x == 10) {
            return false;
        }
        int y = 0;
        while (true) {
            y = y * 10 + x % 10;
            if (y == 0) {
                return false;
            }
            x = x / 10;
            if ((x == y) || (x > y && x / 10 == y)) {
                return true;
            }
            if (x < y) {
                return false;
            }
        }
    }
}

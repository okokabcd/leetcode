package leetcode.leetcode0001_0099.leetcode14;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author: yangdc
 * @date: 2019/12/29 19:03
 */
public class Solution {
    @Test
    public void test001() {
        String[] strs = new String[]{"flower","flow","flight"};
        Assert.assertEquals("fl", longestCommonPrefix(strs));
    }

    @Test
    public void test002() {
        String[] strs = new String[]{"dog","racecar","car"};
        Assert.assertEquals("", longestCommonPrefix(strs));
    }

    public String longestCommonPrefix(String[] strs) {
        // 考虑边界
        if (strs == null || strs.length == 0 || strs[0] == null || strs[0].length() == 0) {
            return "";
        }
        String prefix = "";
        int len = strs[0].length();
        for (int i = 0; i < len; i++) {
            String nextPrefix = prefix + String.valueOf(strs[0].charAt(i));
            for (String str : strs) {
                if (!str.startsWith(nextPrefix)) {
                    return prefix;
                }
            }
            prefix = nextPrefix;
        }
        return prefix;
    }
}

package leetcode.leetcode0400_0499.leetcode453;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author yangdc
 * @date 2022/7/27
 */
public class Solution {
    public int findMinArrowShots(int[][] points) {
        // 二维数组的排序？？
        Arrays.sort(points, Comparator.comparingInt(o -> o[1]));
        int res = 1;
        int end = points[0][1];
        for (int i = 1; i < points.length; i++) {
            if (points[i][0] <= end) {
                end = Math.min(end, points[i][1]);
            } else {
                res++;
                end = points[i][1];
            }
        }
        return res;
    }
}

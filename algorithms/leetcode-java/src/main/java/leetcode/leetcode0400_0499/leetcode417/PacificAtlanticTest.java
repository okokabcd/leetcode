package leetcode.leetcode0400_0499.leetcode417;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/2
 */
public class PacificAtlanticTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[][] heights = new int[][]{{1, 2, 2, 3, 5}, {3, 2, 3, 4, 4}, {2, 4, 5, 3, 1}, {6, 7, 1, 4, 5}, {5, 1, 1, 2, 4}};
        solution.pacificAtlantic(heights);
    }
}

package leetcode.leetcode0400_0499.leetcode417;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/2
 */
public class Solution {
    public List<List<Integer>> pacificAtlantic(int[][] heights) {
        this.heights = heights;
        this.M = heights.length;
        this.N = heights[0].length;

        boolean[][] reachsP = new boolean[M][N];
        boolean[][] reachsA = new boolean[M][N];
        int[] leftDir = new int[]{-1, 0};
        int[] rightDir = new int[]{1, 0};
        int[] upDir = new int[]{0, -1};
        int[] downDir = new int[]{0, 1};

        for (int i = 0; i < M; i++) {
            dfs(reachsP, i, 0);
            dfs(reachsA, i, N - 1);
        }
        for (int j = 0; j < N; j++) {
            dfs(reachsP, 0, j);
            dfs(reachsA, M - 1, j);
        }


        List<List<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (reachsA[i][j] && reachsP[i][j]) {
                    ans.add(Arrays.asList(i, j));
                }
            }
        }

        return ans;
    }

    private int[][] heights;
    private int M;
    private int N;
    private int[][] dirs = new int[][]{{0, 1}, {0, -1}, {-1, 0}, {1, 0}};

    private void dfs(boolean[][] reachs, int i, int j) {
        reachs[i][j] = true;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            if (x >= 0 && x < M && y >= 0 && y < N && !reachs[x][y] && heights[x][y] >= heights[i][j]) {
                dfs(reachs, x, y);
            }
        }
    }
}

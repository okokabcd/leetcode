package leetcode.leetcode0400_0499.leetcode406;

import java.util.*;

/**
 * @author yangdc
 * @date 2022/7/30
 */
public class Solution {
    public int[][] reconstructQueue(int[][] people) {
        // 先对people按身高进行排序
        Arrays.sort(people, (a, b) -> a[0] == b[0] ? a[1] - b[1] : b[0] - a[0]);
        for (int i = 1; i < people.length; i++) {
            int cnt = 0;
            for (int j = 0; j < i; j++) {
                if (cnt == people[i][1]) {
                    int[] t = people[i];
                    for (int k = i - 1; k >= j; k--) {
                        people[k+1]  = people[k];
                    }
                    people[j] = t;
                    break;
                }
                cnt++;
            }
        }
        return people;
    }
}

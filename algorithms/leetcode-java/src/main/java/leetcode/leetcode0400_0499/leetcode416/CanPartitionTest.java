package leetcode.leetcode0400_0499.leetcode416;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/29
 */
public class CanPartitionTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertTrue(solution.canPartition(new int[]{1, 5, 11, 5}));
    }
}

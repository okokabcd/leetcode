package leetcode.leetcode0400_0499.leetcode416;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/6/29
 */
public class Solution {
    public boolean canPartition(int[] nums) {
        // 数组求和
        int sum = Arrays.stream(nums).sum();
        // 场景1：和为奇数不能均分
        if (sum % 2 == 1) {
            return false;
        }
        int target = sum / 2;
        int n = nums.length;
        boolean[][] dp = new boolean[n + 1][target + 1];
        dp[0][0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= target; j++) {
                if (j < nums[i - 1]) {
                    dp[i][j] = dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]];
                }
            }
        }

        return dp[n][target];
    }
}

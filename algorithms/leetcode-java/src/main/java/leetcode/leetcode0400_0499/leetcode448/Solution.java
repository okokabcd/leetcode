package leetcode.leetcode0400_0499.leetcode448;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/8/3
 */
public class Solution {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        for (int num : nums) {
            // 这个地方要注意 -1，数组下标从0开始
            int pos = Math.abs(num) - 1;
            if (nums[pos] > 0) {
                nums[pos] = -nums[pos];
            }
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                ans.add(i + 1);
            }
        }

        return ans;
    }
}

package leetcode.leetcode0400_0499.leetcode435;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author yangdc
 * @date 2022/7/14
 */
public class Solution {
    public int eraseOverlapIntervals(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        int n = intervals.length;
        // 二维数组排序
        Arrays.sort(intervals, (a, b) -> a[1] - b[1]);

        int removed = 0;
        int prev = intervals[0][1];
        for (int i = 1; i < n; i++) {
            if (intervals[i][0] < prev) {
                removed++;
            } else {
                prev = intervals[i][1];
            }
        }
        return removed;
    }
}

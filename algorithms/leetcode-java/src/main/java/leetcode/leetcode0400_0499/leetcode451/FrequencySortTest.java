package leetcode.leetcode0400_0499.leetcode451;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/23
 */
public class FrequencySortTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        String s = "tree";
        Assert.assertEquals("eert", solution.frequencySort(s));
    }
}

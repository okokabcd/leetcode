package leetcode.leetcode0400_0499.leetcode451;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * @author yangdc
 * @date 2022/5/23
 */
public class Solution {
    public String frequencySort(String s) {
        Map<Character, Integer> sizeMap = new HashMap<>();
        for (Character tmp : s.toCharArray()) {
            sizeMap.put(tmp, sizeMap.getOrDefault(tmp, 0) + 1);
        }

        PriorityQueue<Map.Entry<Character, Integer>> priorityQueue = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
        for (Map.Entry<Character, Integer> entry : sizeMap.entrySet()) {
            priorityQueue.add(entry);
        }

        StringBuilder ret = new StringBuilder();
        while(!priorityQueue.isEmpty()) {
            Map.Entry<Character, Integer> entry = priorityQueue.poll();
            for (int i = 0; i < entry.getValue(); i++) {
                ret.append(entry.getKey());
            }
        }

        return ret.toString();
    }
}

package leetcode.leetcode0400_0499.leetcode409;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/8/31
 */
public class Solution {
    public int longestPalindrome(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        boolean odd = false;
        int ans = 0;
        for (Integer count : map.values()) {
            if (count % 2 == 0) {
                ans += count;
            } else {
                odd = true;
                ans += (count - 1);
            }
        }
        ans += odd ? 1 : 0;
        return ans;
    }
}

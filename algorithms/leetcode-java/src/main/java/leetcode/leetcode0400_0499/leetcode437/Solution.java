package leetcode.leetcode0400_0499.leetcode437;

/**
 * @author yangdc
 * @date 2022/09/08
 */

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 * int val;
 * TreeNode left;
 * TreeNode right;
 * TreeNode() {}
 * TreeNode(int val) { this.val = val; }
 * TreeNode(int val, TreeNode left, TreeNode right) {
 * this.val = val;
 * this.left = left;
 * this.right = right;
 * }
 * }
 */
class Solution {
    public int pathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return 0;
        }
        return pathSumStartWithRoot(root, targetSum) + pathSum(root.left, targetSum) + pathSum(root.right, targetSum);
    }

    /**
     * 注意，这里入参类型为long，否则通不过下面这个用例
     * 这里root.val太大，递归调用多了targetNum-root.val就会溢出整数型的最小值，把参数换成long即可
     * [1000000000,1000000000,null,294967296,null,1000000000,null,1000000000,null,1000000000]
     * 0
     */
    int pathSumStartWithRoot(TreeNode root, long targetSum) {
        if (root == null) {
            return 0;
        }

        int count = root.val == targetSum ? 1 : 0;
        count += pathSumStartWithRoot(root.left, targetSum - root.val);
        count += pathSumStartWithRoot(root.right, targetSum - root.val);
        return count;
    }
}

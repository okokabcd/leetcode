package leetcode.leetcode0400_0499.leetcode415;

import org.junit.Assert;

public class addStringsTest {
	@org.junit.Test
	public void test0() {
		// System.out.println(Integer.MAX_VALUE);
		//                                      2147483647
		// System.out.println(Integer.parseInt("6913259244"));
		String s2 = "6913259244";
		String s1 = "71103343";
		System.out.println(new Solution().addStrings(s1, s2));
	}

	@org.junit.Test
	public void test1() {
		String s2 = "1";
		String s1 = "1";
		Assert.assertEquals("2", new Solution().addStrings(s1, s2));
	}

	@org.junit.Test
	public void test2() {
		String s2 = "12";
		String s1 = "12";
		Assert.assertEquals("24", new Solution().addStrings(s1, s2));
	}

	@org.junit.Test
	public void test3() {
		String s2 = "9";
		String s1 = "9";
		Assert.assertEquals("18", new Solution().addStrings(s1, s2));
	}

	@org.junit.Test
	public void test4() {
		String s2 = "9";
		String s1 = "99";
		Assert.assertEquals("108", new Solution().addStrings(s1, s2));
	}

	@org.junit.Test
	public void test5() {
		String s1 = "584";
		String s2 = "18";
		Assert.assertEquals("602", new Solution().addStrings(s1, s2));
	}
	
	@org.junit.Test
	public void test6() {
		String s2 = "6994";
		String s1 = "36";
		Assert.assertEquals("7030", new Solution().addStrings(s1, s2));
	}
}

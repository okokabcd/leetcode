package leetcode.leetcode0400_0499.leetcode415;

/**
 * 415. 字符串相加
 * https://wx.zsxq.com/dweb2/index/group/15522844428282
 *
 * @author yangdc
 * @date 2017年9月26日 上午10:13:55
 */
public class Solution {

    public String addStrings(String num1, String num2) {
        // 字符串加法、链表加法以及二进制加法之类的都可以这么写
        StringBuilder sb = new StringBuilder();
        int carry = 0, i = num1.length() - 1, j = num2.length()  - 1;
        while (i >=0 || j >=0 || carry != 0) {
            if (i >= 0) {
                carry += num1.charAt(i--) - '0';
            }
            if (j >= 0) {
                carry += num2.charAt(j--) - '0';
            }
            sb.append(carry%10);
            carry /= 10;
        }
        return sb.reverse().toString();
    }
}

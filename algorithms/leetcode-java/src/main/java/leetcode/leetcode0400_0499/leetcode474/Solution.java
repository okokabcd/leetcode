package leetcode.leetcode0400_0499.leetcode474;

/**
 * @author yangdc
 * @date 2022/6/30
 */
public class Solution {
    public int findMaxForm(String[] strs, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];
        for (String str : strs) {
            int[] countArr = count(str);
            int count0 = countArr[0];
            int count1 = countArr[1];
            for (int i = m; i>= count0; i--) {
                for (int j = n; j>= count1; j--) {
                    dp[i][j] = Math.max(dp[i][j], 1 + dp[i-count0][j-count1]);
                }
            }
        }
        return dp[m][n];
    }

    /**
     * 计算字符串中0和1的数量
     */
    int[] count(String s) {
        int count0 = s.length();
        int count1 = 0;
        for (char c : s.toCharArray()) {
            if ('1' == c) {
                count1++;
                count0--;
            }
        }
        return new int[]{count0, count1};
    }
}

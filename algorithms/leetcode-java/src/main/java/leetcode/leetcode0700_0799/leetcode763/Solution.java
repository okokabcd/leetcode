package leetcode.leetcode0700_0799.leetcode763;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/7/28
 */
public class Solution {
    public List<Integer> partitionLabels(String s) {
        Map<Character, Integer> map = new HashMap<>();
        char[] cArr = s.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            map.put(cArr[i], i);
        }

        List<Integer> res = new ArrayList<>();
        int start = 0;
        int last = 0;
        for (int i = 0; i < cArr.length; i++) {
            last = Math.max(map.get(cArr[i]), last);
            if (i == last) {
                res.add(last - start + 1);
                start = last + 1;
            }
        }

        return res;
    }
}

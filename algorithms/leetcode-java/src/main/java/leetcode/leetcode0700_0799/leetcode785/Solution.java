package leetcode.leetcode0700_0799.leetcode785;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author yangdc
 * @date 2022/10/11
 */
public class Solution {
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        if (n == 0) {
            return true;
        }
        int[] color = new int[graph.length];
        Deque<Integer> q = new ArrayDeque<>();
        for (int i = 0; i < n; i++) {
            if (color[i] == 0) {
                q.push(i);
                color[i] = 1;
            }
            while (!q.isEmpty()) {
                int node = q.pop();
                for (int j : graph[node]) {
                    if (color[j] == 0) {
                        q.push(j);
                        color[j] = color[node] == 2 ? 1 : 2;
                    } else if (color[node] == color[j]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}

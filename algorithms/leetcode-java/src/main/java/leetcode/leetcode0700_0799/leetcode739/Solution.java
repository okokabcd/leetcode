package leetcode.leetcode0700_0799.leetcode739;

import java.util.Stack;

/**
 * @author yangdc
 * @date 2022/8/10
 */
public class Solution {
    public int[] dailyTemperatures(int[] temperatures) {
        int[] ans = new int[temperatures.length];
        Stack<Integer> desStack = new Stack<>();
        for (int i = 0; i < temperatures.length; i++) {
            while (!desStack.isEmpty()) {
                int preIndex = desStack.peek();
                if (temperatures[i] <= temperatures[preIndex]) {
                    break;
                }
                desStack.pop();
                ans[preIndex] = i - preIndex;
            }
            desStack.push(i);
        }
        return ans;
    }
}

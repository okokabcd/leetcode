package leetcode.leetcode0700_0799.leetcode769;

/**
 * @author yangdc
 * @date 2022/8/6
 */
public class Solution {
    public int maxChunksToSorted(int[] arr) {
        int ans = 0;
        int max = arr[0];
        // [1,2,0,3]
        // [4,3,2,1,0]
        // [1,0,2,3,4]
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(arr[i], max);
            if (max == i) {
                ans++;
            }
        }
        return ans;
    }
}

package leetcode.leetcode1688;

/**
 * 1688. 比赛中的配对次数
 * https://leetcode-cn.com/problems/count-of-matches-in-tournament/
 *
 * @author yangdc
 * @date 2022/1/25
 */
public class Solution2 {

    public int numberOfMatches(int n) {
        return n - 1;
    }
}

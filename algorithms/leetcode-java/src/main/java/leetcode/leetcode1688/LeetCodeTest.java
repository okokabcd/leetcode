package leetcode.leetcode1688;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/1/25
 */
public class LeetCodeTest {
    Solution solution = new Solution();

    @Test
    public void test001() {
        Assert.assertEquals(6, solution.numberOfMatches(7));
    }

    @Test
    public void test002() {
        Assert.assertEquals(13, solution.numberOfMatches(14));
    }
}

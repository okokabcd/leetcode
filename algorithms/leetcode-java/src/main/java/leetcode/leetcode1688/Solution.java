package leetcode.leetcode1688;

/**
 * 1688. 比赛中的配对次数
 * https://leetcode-cn.com/problems/count-of-matches-in-tournament/
 *
 * @author yangdc
 * @date 2022/1/25
 */
public class Solution {

    public int numberOfMatches(int n) {
        int total = 0;
        while (n > 1) {
            total += (n % 2 == 0) ? n / 2 : (n - 1) / 2;
            n = (n % 2 == 0) ? n / 2 : ((n - 1) / 2 + 1);
        }
        return total;
    }
}

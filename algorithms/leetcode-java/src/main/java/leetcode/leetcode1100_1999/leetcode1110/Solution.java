package leetcode.leetcode1100_1999.leetcode1110;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        List<TreeNode> ans = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        for (int tmp : to_delete) {
            set.add(tmp);
        }
        helper(root, true, set, ans);
        return ans;
    }

    TreeNode helper(TreeNode node, boolean isRoot, Set<Integer> set, List<TreeNode> ans) {
        if (node == null) {
            return null;
        }
        boolean deleted = set.contains(node.val);
        if (isRoot && !deleted) {
            ans.add(node);
        }
        node.left = helper(node.left, deleted, set, ans);
        node.right = helper(node.right, deleted, set, ans);
        return deleted ? null : node;
    }
}

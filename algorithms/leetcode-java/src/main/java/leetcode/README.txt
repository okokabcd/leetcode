算法
递归（深度优先DFS，广度优先BFS）
动态规划（Dynamic Programming），
二分查找（Binary Search），
回溯（Back tracing），
分治法（Divide and Conquer），

数据结构
树、数组、链表、字符串和hash表

编程

package leetcode.leetcode889;

/**
 * @author yangdc
 * @date 2022/10/6
 */
public class Solution {
    public TreeNode constructFromPrePost(int[] preorder, int[] postorder) {
        return helper(preorder, 0, preorder.length - 1, postorder, 0, postorder.length - 1);
    }

    TreeNode helper(int[] pre, int preL, int preR, int[] post, int postL, int postR) {
        if (preL > preR || postL > postR) {
            return null;
        }
        TreeNode node = new TreeNode(pre[preL]);
        if (preL == preR) {
            return node;
        }
        int idx = -1;
        for (idx = postL; idx <= postR; idx++) {
            if (pre[preL + 1] == post[idx]) {
                break;
            }
        }
        node.left = helper(pre, preL + 1, preL + 1 + (idx - postL), post, postL, idx);
        node.right = helper(pre, preL + 1 + (idx - postL) + 1, preR, post, idx + 1, postR - 1);
        return node;
    }
}

package leetcode.leetcode0300_0399.leetcode344;

/**
 * 反转字符串
 *
 * @author yangdc
 * @date 2021/08/23
 */
public class Solution {
    public static void main(String[] args) {
        new Solution().test01();
    }

    public void test01() {
        char[] arr = new char[]{'h','e','l','l','o'};
        reverseString(arr);
        for (char tmp : arr) {
            System.out.print(tmp + ", ");
        }
        System.out.println();
    }

    public void reverseString(char[] s) {
        int i = 0;
        int j = s.length - 1;
        while (i < j) {
            char tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;
            i++;
            j--;
        }
    }

}

package leetcode.leetcode0300_0399.leetcode347;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
    public int[] topKFrequent(int[] nums, int k) {
        // 统计频数并记录最大频数值
        Map<Integer, Integer> countMap = new HashMap<>();
        int maxCount = 0;
        for (int tmp : nums) {
            // int tmpCount = countMap.containsKey(tmp) ? countMap.get(tmp) + 1 : 1;
            int tmpCount = countMap.getOrDefault(tmp, 0) + 1;
            maxCount = Math.max(maxCount, tmpCount);
            countMap.put(tmp, tmpCount);
        }

        // 构造桶
        List<Integer>[] listArr = new List[maxCount + 1];
        for (Integer tmp : countMap.keySet()) {
            int count = countMap.get(tmp);
            if (listArr[count] == null) {
                listArr[count] = new ArrayList<>();
            }
            listArr[count].add(tmp);
        }

        // 从桶中倒数取前k个
        List<Integer> resList = new ArrayList<>();
        for (int i = listArr.length - 1; i >= 0 && resList.size() < k; i--) {
            if (listArr[i] == null) {
                continue;
            }
            resList.addAll(listArr[i]);
        }
        return resList.stream().mapToInt(x -> x).toArray();
    }
}

package leetcode.leetcode0300_0399.leetcode349;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author yangdc
 * @date 2021/08/16
 */
public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        // 排序
        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int i = 0;
        int j = 0;
        Set<Integer> s = new HashSet<>();
        while (i < nums1.length && j < nums2.length) {
            int tmp1 = nums1[i];
            int tmp2 = nums2[j];
            if (tmp1 < tmp2) {
                i++;
            } else if (tmp1 > tmp2) {
                j++;
            } else {
                i++;
                j++;
                if (!s.contains(tmp1)) {
                    s.add(tmp1);
                }
            }
        }
        int[] arr = new int[s.size()];
        Integer[] sArr = s.toArray(new Integer[0]);
        for (int m = 0; m < sArr.length; m++) {
            arr[m] = sArr[m];
        }
        return arr;
    }
}

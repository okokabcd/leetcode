package leetcode.leetcode0300_0399.leetcode312;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/7/10
 */
public class Solution {
    public int maxCoins(int[] nums) {
        int n = nums.length;
        int[] nums2 = new int[n + 2];
        nums2[0] = 1;
        nums2[n + 1] = 1;
        for (int i = 0; i < n; i++) {
            nums2[i + 1] = nums[i];
        }
        int[][] dp = new int[n + 2][n + 2];
        for (int l = 1; l <= n; l++) {
            for (int i = 1; i <= n - l + 1; i++) {
                int j = i + l - 1;
                for (int k = i; k <= j; k++) {
                    dp[i][j] = Math.max(dp[i][j], dp[i][k - 1] + nums2[i - 1] * nums2[k] * nums2[j + 1] + dp[k + 1][j]);
                }
            }
        }
        return dp[1][n];
    }
}

package leetcode.leetcode0300_0399.leetcode300;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/25
 */
public class LengthOfLISTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[] nums = new int[]{10, 9, 2, 5, 3, 7, 101, 18};
        Assert.assertEquals(4, solution.lengthOfLIS(nums));
    }
}

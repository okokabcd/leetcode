package leetcode.leetcode0300_0399.leetcode304;

/**
 * @author yangdc
 * @date 2022/08/18
 */
public class NumMatrix1 {

    private int[] integral;
    private int m;
    private int n;

    public NumMatrix1(int[][] matrix) {
        m = matrix.length;
        n = matrix[0].length;
        integral = new int[matrix.length * n + 1];
        for (int j = 0; j < n; j++) {
            integral[j + 1] = integral[j] + matrix[0][j];
        }
        for (int i = 0; i < m; i++) {
            int k = i * n + 1;
            integral[i * n + 1] = integral[i == 0 ? 0 : (k - n)] + matrix[i][0];
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                int k = i * n + j + 1;
                integral[k] = matrix[i][j] + integral[k - 1] + integral[k - n] - integral[k - n - 1];
            }
        }
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        if (row2 == 0 && col2 == 0) {
            return integral[1];
        }
        row1 = row1 == 0 ? 1 : row1;
        col1 = col1 == 0 ? 1 : col1;
        int a = row2 * n + col2 + 1;
        int b = row2 * n + (col1 - 1) + 1;
        int c = (row1 - 1) * n + col2 + 1;
        int d = (row1 - 1) * n + (col1 - 1) + 1;
        return integral[a] - integral[b] - integral[c] + integral[d];
    }
}

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix obj = new NumMatrix(matrix);
 * int param_1 = obj.sumRegion(row1,col1,row2,col2);
 */

package leetcode.leetcode0300_0399.leetcode304;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/08/18
 */
public class NumMatrixTest {
    @Test
    public void test01() {
        int[][] matrix = {{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}};
        NumMatrix nm = new NumMatrix(matrix);
        Assert.assertEquals(8, nm.sumRegion(2, 1, 4, 3));
        Assert.assertEquals(11, nm.sumRegion(1, 1, 2, 2));
        Assert.assertEquals(12, nm.sumRegion(1, 2, 2, 4));
    }
    // ["NumMatrix","sumRegion"]
    // [ [ [ [-1] ] ],[0,0,0,0] ]
}

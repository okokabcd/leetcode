package leetcode.leetcode0500_0599.leetcode530;

/**
 * @author yangdc
 * @date 2022/10/5
 */
public class Solution {
    public int getMinimumDifference(TreeNode root) {
        int[] res = {Integer.MAX_VALUE};
        int[] pre = {-1};
        inorder(root, pre, res);
        return res[0];
    }

    void inorder(TreeNode root, int[] pre, int[] res) {
        if (root == null) {
            return;
        }
        inorder(root.left, pre, res);
        if (pre[0] != -1) {
            res[0] = Math.min(res[0], root.val - pre[0]);
        }
        pre[0] = root.val;
        inorder(root.right, pre, res);
    }
}

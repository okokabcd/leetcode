package leetcode.leetcode0500_0599.leetcode524;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/5/20
 */
public class FindLongestWordTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        String s = "abpcplea";
        List<String> dictionary = Arrays.asList("ale", "apple", "monkey", "plea");
        Assert.assertEquals("apple", solution.findLongestWord(s, dictionary));
    }

    @Test
    public void test02() {
        String s = "apple";
        List<String> dictionary = Arrays.asList("zxc","vbn");
        Assert.assertEquals("", solution.findLongestWord(s, dictionary));
    }

}

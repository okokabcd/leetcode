package leetcode.leetcode0500_0599.leetcode524;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public String findLongestWord(String s, List<String> dictionary) {
        List<String> res = new ArrayList<>();
        int maxLen = 0;
        for (String tmp : dictionary) {
            if (isSubstring(s, tmp)) {
                if (tmp.length() > maxLen) {
                    maxLen = tmp.length();
                    res.clear();
                    res.add(tmp);
                } else if (tmp.length() == maxLen) {
                    res.add(tmp);
                }
            }
        }
        return getMin(res);
    }
    private boolean isSubstring(String str, String subStr) {
        int i = str.length() - 1;
        int j = subStr.length() - 1;
        while (i >= 0 && j >= 0) {
            if (str.charAt(i) == subStr.charAt(j)) {
                j--;
            }
            i--;
        }
        return j == -1;
    }

    private String getMin(List<String> list) {
        if (list == null || list.size() == 0) {
            return "";
        }
        String minStr = list.get(0);
        for (String tmp : list) {
            if (minStr.compareTo(tmp) > 0) {
                minStr = tmp;
            }
        }
        return minStr;
    }
}

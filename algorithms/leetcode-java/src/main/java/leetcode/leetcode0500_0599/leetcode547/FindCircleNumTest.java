package leetcode.leetcode0500_0599.leetcode547;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/6/1
 */
public class FindCircleNumTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals(1, solution.findCircleNum(new int[][]{{1, 0, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 1}, {1, 0, 1, 1}}));
    }

    @Test
    public void test02() {
        Assert.assertEquals(3, solution.findCircleNum(new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}));
    }

    @Test
    public void test03() {
        Assert.assertEquals(2, solution.findCircleNum(new int[][]{{1, 1, 0}, {1, 1, 0}, {0, 0, 1}}));
    }
}

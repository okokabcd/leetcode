package leetcode.leetcode0500_0599.leetcode547;

/**
 * @author yangdc
 * @date 2022/6/28
 */
public class Solution2 {
    public int findCircleNum(int[][] isConnected) {
        int num = 0;
        int M = isConnected.length;
        boolean[] visited = new boolean[M];
        for (int i = 0; i < M; i++) {
            if (!visited[i]) {
                dfs(isConnected, i, visited);
                num++;
            }
        }
        return num;
    }

    void dfs(int[][] isConnected, int i, boolean[] visited) {
        visited[i] = true;
        for (int j = 0; j < isConnected[0].length; j++) {
            if (isConnected[i][j] == 1 && !visited[j]) {
                dfs(isConnected, j, visited);
            }
        }
    }
}

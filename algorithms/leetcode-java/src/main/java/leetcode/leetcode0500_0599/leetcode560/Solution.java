package leetcode.leetcode0500_0599.leetcode560;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/8/21
 */
public class Solution {
    public int subarraySum(int[] nums, int k) {
        int ans = 0;
        int sum = 0;
        Map<Integer, Integer> d = new HashMap<>();
        d.put(0, 1);
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            ans += d.getOrDefault(sum - k, 0);
            d.put(sum, d.getOrDefault(sum, 0) + 1);
        }
        return ans;
    }
}

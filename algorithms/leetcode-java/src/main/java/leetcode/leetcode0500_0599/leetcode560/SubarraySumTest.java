package leetcode.leetcode0500_0599.leetcode560;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/21
 */
public class SubarraySumTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        // nums = [1,1,1], k = 2
        int[] nums = {1, 1, 1};
        int k = 2;
        Assert.assertEquals(2, solution.subarraySum(nums, k));
    }

    @Test
    public void test02() {
        int[] nums = {1};
        int k = 0;
        Assert.assertEquals(0, solution.subarraySum(nums, k));
    }
}

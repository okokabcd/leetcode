package leetcode.leetcode0500_0599.leetcode513;

/**
 * @author yangdc
 * @date 2022/9/30
 */
public class Solution {
    public int findBottomLeftValue(TreeNode root) {
        int maxDepth = 1;
        int[] res = new int[]{root.val};
        helper(root, 1, maxDepth, res);
        return res[0];
    }

    void helper(TreeNode node, int depth, int maxDpeth, int[] res) {
        if (node == null) {
            return;
        }
        if (depth > maxDpeth) {
            maxDpeth = depth;
            res[0] = node.val;
        }
        helper(node.left, depth + 1, maxDpeth, res);
        helper(node.right, depth + 1, maxDpeth, res);
    }
}

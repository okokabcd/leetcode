package leetcode.leetcode0500_0599.leetcode543;

/**
 * @author yangdc
 * @date 2022/9/9
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 * int val;
 * TreeNode left;
 * TreeNode right;
 * TreeNode() {}
 * TreeNode(int val) { this.val = val; }
 * TreeNode(int val, TreeNode left, TreeNode right) {
 * this.val = val;
 * this.left = left;
 * this.right = right;
 * }
 * }
 */
class Solution {
    /**
     * 用来优化递归
     */
    Map<TreeNode, Integer> map = new HashMap<>();

    public int diameterOfBinaryTree(TreeNode root) {
        int[] diameter = new int[1];
        maxDepth(root, diameter);
        return diameter[0];
    }

    /**
     * 求二叉树的最大深度，而二叉树的直径为 左二叉树深度 + 右二叉树深度
     */
    int maxDepth(TreeNode root, int[] diameter) {
        if (root == null) {
            return 0;
        }
        if (map.containsKey(root)) {
            return map.get(root);
        }
        int l = maxDepth(root.left, diameter);
        int r = maxDepth(root.right, diameter);
        diameter[0] = Math.max(l + r, diameter[0]);
        return Math.max(l, r) + 1;
    }
}

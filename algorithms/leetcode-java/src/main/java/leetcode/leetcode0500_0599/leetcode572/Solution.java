package leetcode.leetcode0500_0599.leetcode572;

/**
 * @author yangdc
 * @date 2022/9/28
 */
public class Solution {
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        // root至少有1个节点
        // subRoot至少有1个节点
        if (root == null) {
            return false;
        }
        boolean isSame = isSameTreeNode(root, subRoot);
        if (isSame) {
            return true;
        }
        return isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    boolean isSameTreeNode(TreeNode node1, TreeNode node2) {
        if (node1 == null && node2 == null) {
            return true;
        }
        if (node1 == null && node2 != null) {
            return false;
        }
        if (node1 != null && node2 == null) {
            return false;
        }
        if (node1.val != node2.val) {
            return false;
        }
        return isSameTreeNode(node1.left, node2.left) && isSameTreeNode(node1.right, node2.right);
    }
}

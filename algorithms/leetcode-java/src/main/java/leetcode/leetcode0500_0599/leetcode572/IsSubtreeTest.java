package leetcode.leetcode0500_0599.leetcode572;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/9/28
 */
public class IsSubtreeTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(4);
        root.right = new TreeNode(5);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(2);

        TreeNode subRoot = new TreeNode(4);
        subRoot.left = new TreeNode(1);
        subRoot.right = new TreeNode(2);

        System.out.println(solution.isSubtree(root, subRoot));
    }
}

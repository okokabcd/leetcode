package leetcode.leetcode0500_0599.leetcode504;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/2
 */
public class ConvertToBase7Test {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertEquals("202", solution.convertToBase7(100));
    }

    @Test
    public void test02() {
        Assert.assertEquals("-10", solution.convertToBase7(-7));
    }
}

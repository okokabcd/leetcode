package leetcode.leetcode0500_0599.leetcode504;

/**
 * @author yangdc
 * @date 2022/8/2
 */
public class Solution {
    public String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }
        boolean isNegative = num < 0;
        num = isNegative ? -num : num;
        StringBuilder ans = new StringBuilder();
        while (num >= 7) {
            int a = num / 7;
            int b = num % 7;
            ans.append(b);
            num = a;
        }
        ans.append(num);

        return (isNegative ? "-" : "") + ans.reverse();
    }
}

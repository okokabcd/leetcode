package leetcode.leetcode0500_0599.leetcode503;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/22
 */
public class NextGreaterElementsTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        // 输入: nums = [1,2,3,4,3]
        // 输出: [2,3,4,-1,4]
        int[] nums = {1, 2, 3, 4, 3};
        Assert.assertArrayEquals(new int[]{2, 3, 4, -1, 4}, solution.nextGreaterElements(nums));
    }
}

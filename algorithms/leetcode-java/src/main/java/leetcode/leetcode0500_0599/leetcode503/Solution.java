package leetcode.leetcode0500_0599.leetcode503;

import java.util.Arrays;
import java.util.Stack;

/**
 * @author yangdc
 * @date 2022/8/22
 */
public class Solution {
    public int[] nextGreaterElements(int[] nums) {
        // 输入: nums = [1,2,3,4,3]
        // 输出: [2,3,4,-1,4]
        int n = nums.length;
        int[] ans = new int[n];
        Arrays.fill(ans, -1);
        Stack<Integer> desStack = new Stack<>();
        for (int i = 0; i < n * 2; i++) {
            int num = nums[i % n];
            while (!desStack.isEmpty() && nums[desStack.peek()] < num) {
                ans[desStack.peek()] = num;
                desStack.pop();
            }
            if (i < n) {
                desStack.push(i);
            }
        }
        for (int tmp : ans) {
            System.out.print(tmp + ", ");
        }
        return ans;
    }
}

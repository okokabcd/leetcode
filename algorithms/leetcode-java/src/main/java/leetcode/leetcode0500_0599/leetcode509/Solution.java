package leetcode.leetcode0500_0599.leetcode509;

/**
 * @author yangdc
 * @date 2022/8/29
 */
public class Solution {
    public int fib(int n) {
        if (n < 2) {
            return n;
        }
        int[] dp = new int[n + 1];
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
}

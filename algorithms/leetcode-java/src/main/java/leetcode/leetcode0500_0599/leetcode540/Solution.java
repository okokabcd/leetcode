package leetcode.leetcode0500_0599.leetcode540;

public class Solution {
    public int singleNonDuplicate(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        while (l < r) {
            int mid = l + (r - l) / 2;
            // 保证mid是偶数
            mid = mid / 2 * 2;
            if (nums[mid] == nums[mid + 1]) {
                l = mid + 2;
            } else {
                r = mid;
            }
        }
        // 针对只有一个元素的数组，题目中已经给出数组最小长度为1
        return nums[l];
    }
}

package leetcode.leetcode0500_0599.leetcode540;

import org.junit.Assert;
import org.junit.Test;

public class SingleNonDuplicateTest {
    Solution solution = new Solution();

    @Test
    public void test001() {
        Assert.assertEquals(2, solution.singleNonDuplicate(new int[]{1,1,2,3,3,4,4,8,8}));
    }

    @Test
    public void test002() {
        Assert.assertEquals(10, solution.singleNonDuplicate(new int[]{3,3,7,7,10,11,11}));
    }

    @Test
    public void test003() {
        Assert.assertEquals(1, solution.singleNonDuplicate(new int[]{1}));
    }

    @Test
    public void test004() {
        Assert.assertEquals(9, solution.singleNonDuplicate(new int[]{1,1,2,2,4,4,5,5,9}));
    }
}

package leetcode.leetcode0500_0599.leetcode538;

/**
 * @author yangdc
 * @date 2022/10/1
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode (int val) {
        this.val = val;
    }

    TreeNode (int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

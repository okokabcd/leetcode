package leetcode.leetcode0500_0599.leetcode538;

/**
 * @author yangdc
 * @date 2022/10/1
 */
public class Solution {

    public TreeNode convertBST(TreeNode root) {
        int[] sum = {0};
        helper(root, sum);
        return root;
    }

    void helper(TreeNode node, int[] sum) {
        if (node == null) {
            return;
        }
        helper(node.right, sum);
        node.val += sum[0];
        sum[0] = node.val;
        helper(node.left, sum);
    }

}

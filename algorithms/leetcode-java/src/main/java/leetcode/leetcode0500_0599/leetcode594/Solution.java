package leetcode.leetcode0500_0599.leetcode594;

import cn.hutool.core.lang.hash.Hash;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * @author yangdc
 * @date 2022/8/24
 */
public class Solution {
    public int findLHS(int[] nums) {
        Map<Integer, Integer> countMap = new HashMap<>();
        for (int num : nums) {
            countMap.put(num, countMap.getOrDefault(num, 0) + 1);
        }

        // a-b由小到大 b-a由大到小
        PriorityQueue<Map.Entry<Integer, Integer>> priorityQueue
                = new PriorityQueue<>(Comparator.comparingInt(Map.Entry::getKey));
        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            priorityQueue.add(entry);
        }

        int ans = 0;
        Map.Entry<Integer, Integer> pre = priorityQueue.poll();
        while (!priorityQueue.isEmpty()) {
            Map.Entry<Integer, Integer> cur = priorityQueue.poll();
            if (cur.getKey() - pre.getKey() == 1) {
                ans = Math.max(ans, cur.getValue() + pre.getValue());
            }
            pre = cur;
        }

        return ans;
    }
}

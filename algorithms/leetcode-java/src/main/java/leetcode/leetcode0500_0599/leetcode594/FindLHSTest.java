package leetcode.leetcode0500_0599.leetcode594;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/24
 */
public class FindLHSTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[] nums = {1, 3, 2, 2, 5, 2, 3, 7};
        Assert.assertEquals(5, solution.findLHS(nums));
    }
}

package leetcode.leetcode0500_0599.leetcode542;

import org.junit.Assert;
import org.junit.Test;

public class UpdateMatrixTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        int[][] except = new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}};
        int[][] input = new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}};
        Assert.assertArrayEquals(except, solution.updateMatrix(input));
    }

    @Test
    public void test02() {
        int[][] except = new int[][]{{0}, {0}, {0}, {0}, {0}};
        int[][] input = new int[][]{{0}, {0}, {0}, {0}, {0}};
        Assert.assertArrayEquals(except, solution.updateMatrix(input));
    }
}

package leetcode.leetcode0600_0699.leetcode695;

/**
 * @author yangdc
 * @date 2022/5/31
 */
public class Solution {
    public int maxAreaOfIsland(int[][] grid) {
        this.GRID = grid;
        this.M = grid.length;
        this.N = grid[0].length;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (GRID[i][j] == 1) {
                    dfs(i, j);
                    max = Math.max(max, island);
                    island = 0;
                }
            }
        }
        return max;
    }

    private int[][] GRID;
    private int M;
    private int N;
    private int[][] dirs = new int[][]{{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    private int island = 0;
    private int max = 0;

    private void dfs(int i, int j) {
        // 此处保证GRID[i][j] == 1
        GRID[i][j] = 0;
        island++;
        for (int[] dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            if (x >= 0 && x < M && y >= 0 && y < N && GRID[x][y] == 1) {
                dfs(x, y);
            }
        }
    }
}

package leetcode.leetcode0600_0699.leetcode696;

/**
 * @author yangdc
 * @date 2022/8/28
 */
public class Solution {
    public int countBinarySubstrings(String s) {
        // 输入：s = "00110011"
        // 输出：6
        int ans = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            ans += extendSubstrings(s, i, i + 1);
        }
        return ans;
    }

    private int extendSubstrings(String s, int l, int r) {
        char lc = s.charAt(l);
        char rc = s.charAt(r);
        if (!((lc == '1' && rc == '0') || (lc == '0' && rc == '1'))) {
            return 0;
        }
        int count = 0;
        while (l >= 0 && r < s.length() && lc == s.charAt(l) && rc == s.charAt(r)) {
            count++;
            l--;
            r++;
        }
        return count;
    }
}

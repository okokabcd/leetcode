package leetcode.leetcode0600_0699.leetcode696;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/28
 */
public class CountBinarySubstringsTest {

    Solution solution = new Solution();

    @Test
    public void test01() {
        // 输入：s = "00110011"
        // 输出：6
        String s = "00110011";
        Assert.assertEquals(6, solution.countBinarySubstrings(s));
    }
}

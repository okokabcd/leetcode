package leetcode.leetcode0600_0699.leetcode643;

/**
 * @author yangdc
 * @date 2022/5/17
 */
public class Solution {
    public double findMaxAverage(int[] nums, int k) {
        // 初始化窗口
        double total = 0;
        for (int i = 0; i < k; i++) {
            total += nums[i];
        }
        double maxAvg = total / k;
        // 向右移
        for (int right = k; right < nums.length; right++) {
            total = total + nums[right] - nums[right - k];
            double tmpAvg = total / k;
            if (tmpAvg >= maxAvg) {
                maxAvg = tmpAvg;
            }
        }
        return maxAvg;
    }
}

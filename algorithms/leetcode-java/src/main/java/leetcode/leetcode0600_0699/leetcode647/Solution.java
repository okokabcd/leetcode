package leetcode.leetcode0600_0699.leetcode647;

/**
 * @author yangdc
 * @date 2022/8/27
 */
public class Solution {
    public int countSubstrings(String s) {
        // 我们可以从字符串的每个位置开始，向左向右延长，判断存在多少以当前位置为中轴的回文 子字符串。
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            // 奇数长度
            count += extendSubstrings(s, i, i);
            // 偶数长度
            count += extendSubstrings(s, i, i+1);
        }
        return count;
    }

    private int extendSubstrings(String s, int l, int r) {
        int count = 0;
        while (l >=0 && r < s.length() && s.charAt(l) == s.charAt(r)) {
            l--;
            r++;
            count++;
        }
        return count;
    }
}

package leetcode.leetcode0600_0699.leetcode650;

/**
 * @author yangdc
 * @date 2022/7/2
 */
public class Solution {
    public int minSteps(int n) {
        int[] dp = new int[n + 1];
        int h = n * n;
        for (int i = 2; i <= n; i++) {
            dp[i] = i;
            for (int j = 2; j <= h; j++) {
                if (i % j == 0) {
                    dp[i] = dp[j] + dp[i / j];
                    break;
                }
            }
        }
        return dp[n];
    }
}

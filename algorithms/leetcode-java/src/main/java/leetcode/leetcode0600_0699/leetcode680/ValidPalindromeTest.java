package leetcode.leetcode0600_0699.leetcode680;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/5/18
 */
public class ValidPalindromeTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        Assert.assertTrue(solution.validPalindrome("aba"));
    }

    @Test
    public void test02() {
        Assert.assertTrue(solution.validPalindrome("abca"));
    }

    @Test
    public void test03() {
        Assert.assertTrue(solution.validPalindrome("cbbcc"));
    }

    @Test
    public void test04() {
        Assert.assertTrue(solution.validPalindrome("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga"));
    }

    @Test
    public void test05() {
        Assert.assertFalse(solution.validPalindrome("abc"));
    }

    @Test
    public void test06() {
        // ececabbacec
        Assert.assertTrue(solution.validPalindrome("ebcbbececabbacecbbcbe"));
    }

    @Test
    public void test100() {
        String s = "1234567890";
        System.out.println(s.substring(1, 9));
    }


}

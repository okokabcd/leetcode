package leetcode.leetcode0600_0699.leetcode680;

/**
 * @author yangdc
 * @date 2022/5/18
 */
public class Solution {
    int pos = 0;

    public boolean validPalindrome(String s) {
        boolean valid = isPalindrome(s);
        if (!valid) {
            int left = pos;
            int right = s.length() - pos;
            boolean ret1 = isPalindrome(s.substring(left + 1, right));
            boolean ret2 = isPalindrome(s.substring(left, right - 1));
            valid = ret1 || ret2;
        }
        return valid;
    }

    private boolean isPalindrome(String s) {
        int left = 0;
        int right = s.length() - 1;
        while (left < right) {
            if (s.charAt(left) == s.charAt(right)) {
                left++;
                right--;
                continue;
            }
            pos = left;
            return false;
        }
        return true;
    }
}

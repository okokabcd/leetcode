package leetcode.leetcode0600_0699.leetcode605;

/**
 * @author yangdc
 * @date 2022/7/15
 */
public class Solution {
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        if (n == 0) {
            return true;
        }
        int count = 0;
        int len = flowerbed.length;
        for (int i = 0; i < len; i++) {
            boolean left = i == 0 ? true : flowerbed[i - 1] == 0;
            boolean right = i == len - 1 ? true : flowerbed[i + 1] == 0;
            if (flowerbed[i] == 0 && left && right) {
                count++;
                flowerbed[i] = 2;
                if (count >= n) {
                    return true;
                }
            }
        }
        return false;
    }
}

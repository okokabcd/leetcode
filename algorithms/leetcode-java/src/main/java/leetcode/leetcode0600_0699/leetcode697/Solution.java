package leetcode.leetcode0600_0699.leetcode697;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangdc
 * @date 2022/8/23
 */
public class Solution {
    public int findShortestSubArray(int[] nums) {
        Map<Integer, Integer> countMap = new HashMap<>();
        Map<Integer, Integer> firstIdxMap = new HashMap<>();
        int ans = nums.length;
        int degree = 0;
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            countMap.put(num, countMap.getOrDefault(num, 0) + 1);
            // 记录首位置
            if (countMap.get(num) == 1) {
                firstIdxMap.put(num, i);
            }

            if (countMap.get(num) == degree) {
                ans = Math.min(ans, i - firstIdxMap.get(num) + 1);
            } else if (countMap.get(num) > degree) {
                ans = i - firstIdxMap.get(num) + 1;
                degree = countMap.get(num);
            }
        }
        return ans;
    }
}

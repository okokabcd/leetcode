package leetcode.leetcode0600_0699.leetcode665;

/**
 * @author yangdc
 * @date 2022/7/31
 */
public class Solution {
    public boolean checkPossibility(int[] nums) {
        int cnt = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < nums[i - 1]) {
                if (cnt == 0) {
                    return false;
                }
                cnt--;
                if (i == 1) {
                    nums[i - 1] = nums[i];
                } else if (nums[i] >= nums[i - 2]) {
                    nums[i - 1] = nums[i];
                } else {
                    nums[i] = nums[i - 1];
                }
            }
        }
        return true;
    }
}

package leetcode.leetcode0900_0999.leetcode934;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author yangdc
 * @date 2022/6/7
 */
public class Solution {
    public int shortestBridge(int[][] grid) {
        // 用来存入第一个岛屿的坐标
        Queue<Pair> queue = new LinkedList();
        boolean found = false;
        for (int i = 0; !found && i < grid.length; i++) {
            for (int j = 0; !found && j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    dfs(grid, j, i, queue);
                    found = true;
                }
            }
        }

        int steps = 0;
        int[] dirs = new int[]{0, 1, 0, -1, 0};
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size-- != 0) {
                Pair p = queue.poll();
                int x = p.x;
                int y = p.y;
                for (int i = 0; i < 4; i++) {
                    int tx = x + dirs[i];
                    int ty = y + dirs[i + 1];
                    if (tx < 0 || ty < 0 || tx >= grid[0].length || ty >= grid.length || grid[ty][tx] == 2) {
                        continue;
                    }
                    if (grid[ty][tx] == 1) {
                        return steps;
                    }
                    grid[ty][tx] = 2;
                    queue.add(new Pair(tx, ty));
                }
            }
            steps++;
        }

        return -1;
    }

    private void dfs(int[][] grid, int x, int y, Queue<Pair> queue) {
        if (x < 0 || y < 0 || x >= grid[0].length || y >= grid.length || grid[y][x] != 1) {
            return;
        }
        grid[y][x] = 2;
        queue.add(new Pair(x, y));
        dfs(grid, x - 1, y, queue);
        dfs(grid, x, y - 1, queue);
        dfs(grid, x + 1, y, queue);
        dfs(grid, x, y + 1, queue);
    }

    class Pair {
        int x;
        int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

}

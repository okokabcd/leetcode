package leetcode.leetcode0900_0999.leetcode912;

import org.junit.Assert;
import org.junit.Test;

public class SortArrayTest {
    @Test
    public void test01() {
        int[] nums = new int[]{5, 2, 3, 1};
        Assert.assertArrayEquals(new int[]{1, 2, 3, 5}, new Solution2().sortArray(nums));
    }
}

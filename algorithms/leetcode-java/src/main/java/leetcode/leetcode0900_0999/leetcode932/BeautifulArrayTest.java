package leetcode.leetcode0900_0999.leetcode932;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/7/9
 */
public class BeautifulArrayTest {
    Solution solution = new Solution();

    @Test
    public void test01() {
        // Assert.assertArrayEquals(new int[]{2,1,4,3}, solution.beautifulArray(4));
        int[] tmp = solution.beautifulArray(4);
        System.out.println(Arrays.toString(tmp));
    }
}

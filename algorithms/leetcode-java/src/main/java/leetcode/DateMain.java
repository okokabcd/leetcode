package leetcode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author yangdc
 * @date 2024/5/9
 */
public class DateMain {
    public static void main(String[] args) {
        String dateStr = "2024-05-01 00:00:00";
        LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}

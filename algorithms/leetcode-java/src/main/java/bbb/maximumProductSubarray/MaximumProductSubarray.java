package bbb.maximumProductSubarray;


/**
 * Author:   admin
 * Date:     2018/8/14 16:02
 */
public class MaximumProductSubarray {
    public static void main(String[] args) {
        int[] nums = {2, 3, -2, 4};
        System.out.println(new MaximumProductSubarray().maxProduct(nums));
    }

    public int maxProduct(int[] nums) {
        if (nums.length == 1) return nums[0];

        // 定义问题：状态及对状态的定义
        // 设max[i]表示数列中第i项结尾的连续子序列的最大连乘积
        // 求max[0]...max[n]中的最大值
        // 状态转移方程
        // max[0] = nums[0]
        // max[i] = Max.max(max[i-1] * nums[i], nums[i])
        int[] max = new int[nums.length];
        int[] min = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            max[i] = min[i] = nums[i];
        }

        int product = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < 0) {
                max[i] = Math.max(min[i - 1] * nums[i], max[i]);
                min[i] = Math.min(max[i - 1] * nums[i], min[i]);
                product = Math.max(max[i], product);
            } else {
                max[i] = Math.max(max[i - 1] * nums[i], max[i]);
                min[i] = Math.min(min[i - 1] * nums[i], min[i]);
                product = Math.max(max[i], product);
            }
        }
        return product;
    }
}

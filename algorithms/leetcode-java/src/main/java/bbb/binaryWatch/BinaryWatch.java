package bbb.binaryWatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 下午3:04:51
 *
 */
public class BinaryWatch {
	public List<String> readBinaryWatch(int num) {
		List<String> list = new ArrayList<String>();
		if (num == 0) {
			list.add("0:00");
			return list;
		}
		// h 0 1 2 3 4
		// m 0 1 2 3 4 5 6
		Map<Integer, String> hMap = new HashMap<Integer, String>();
		Map<Integer, String> mMap = new HashMap<Integer, String>();
		hMap.put(0, "0");
		hMap.put(1, "8,4,2,1");
		hMap.put(2, "10,9,6,5,3"); // 12,
		hMap.put(3, "11,7"); // 14,13,
		hMap.put(4, "15");
		mMap.put(0, "00");
		mMap.put(1, "32,16,08,04,02,01");
		mMap.put(2, "48,40,36,34,33,24,20,18,17,12,10,09,06,05,03");
		mMap.put(3, "56,52,50,49,44,42,41,38,37,35,28,26,25,22,21,19,14,13,11,07");
		mMap.put(4, "58,57,54,53,51,46,45,43,39,30,29,27,23,15"); // 60,
		mMap.put(5, "59,55,47,31"); // 62,61,
		mMap.put(6, "63");

		for (int h = 0; h <= num && h <= 3; h++) {
			int m = num - h;
			if (m > 5) {
				continue;
			}
			for (String tmpH : hMap.get(h).split(",")) {
				for (String tmpM : mMap.get(m).split(",")) {
					list.add(tmpH + ":" + tmpM);
				}
			}
		}
		return list;
	}
}

package bbb.employeeImportance;

import java.util.*;

/**
 * Author:   admin
 * Date:     2018/9/4 15:37
 */
public class EmployeeImportance {
    public int getImportance(List<Employee> employees, int id) {
        Map<Integer, Employee> employeeMap = new HashMap<>();
        for (Employee tmp : employees) {
            employeeMap.put(tmp.id, tmp);
        }

        Queue<Employee> employeeQueue = new LinkedList<>();
        employeeQueue.offer(employeeMap.get(id));
        int importance = 0;
        while (!employeeQueue.isEmpty()) {
            Employee cur = employeeQueue.poll();
            importance += cur.importance;
            if (cur.subordinates == null) continue;
            for (int tmp : cur.subordinates) {
                employeeQueue.offer(employeeMap.get(tmp));
            }
        }
        return importance;
    }

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        // [[1,5,[2,3]],[2,3,[]],[3,3,[]]]
        employees.add(new Employee(1, 5, new Integer[]{2, 3}));
        employees.add(new Employee(2, 3, null));
        employees.add(new Employee(3, 3, null));
        int result = new EmployeeImportance().getImportance(employees, 1);
        System.out.println(result);
    }
}

// Employee info
class Employee {
    // It's the unique id of each node;
    // unique id of this employee
    public int id;
    // the importance value of this employee
    public int importance;
    // the id of direct subordinates
    public List<Integer> subordinates;

    public Employee() {

    }

    public Employee(int id, int importance, Integer[] subordinates) {
        this.id = id;
        this.importance = importance;
        if (subordinates != null) {
            this.subordinates = Arrays.asList(subordinates);
        }
    }
};

package bbb.groupsofSpecialEquivalentStrings;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/8/29 下午10:18
 */
public class GroupsofSpecialEquivalentStrings {
    public int numSpecialEquivGroups(String[] A) {
        Set<String> strSet = new HashSet<>();
        for (String tmp : A) {
            strSet.add(strHash(tmp));
        }
        return strSet.size();
    }

    private String strHash(String tmp) {
        if (tmp.length() == 1) return tmp;
        int[] cArr = new int[52];
        int odd = 0;
        for (char c : tmp.toCharArray()) {
            cArr[(odd++ % 2 == 0 ? 26 : 0) + c - 'a']++;
        }
        StringBuilder sb = new StringBuilder();
        for (int i : cArr) {
            sb.append(i);
        }
        // System.out.println(sb.toString());
        return sb.toString();
    }

    public static void main(String[] args) {
        String[] A = {"abc", "acb", "bac", "bca", "cab", "cba"};
        // String[] A = {"leetcode0001_0100","b","c","leetcode0001_0100","c","c"};
        GroupsofSpecialEquivalentStrings main = new GroupsofSpecialEquivalentStrings();
        int size = main.numSpecialEquivGroups(A);
        System.out.println(size);
    }
}

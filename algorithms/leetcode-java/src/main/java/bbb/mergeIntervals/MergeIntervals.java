//package bbb.mergeIntervals;
//
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Author:   yangdc
// * Date:     2018/9/8 下午8:32
// */
//public class MergeIntervals {
//    public List<Interval> merge(List<Interval> intervals) {
//        if (intervals == null || intervals.size() == 0) return intervals;
//
//        // sort
//        intervals = intervals.stream()
//                .sorted(Comparator.comparing(e -> e.start))
//                .collect(Collectors.toList());
//
//        List<Interval> result = new ArrayList<>();
//        int start = intervals.get(0).start;
//        int end = intervals.get(0).end;
//        for (int i = 1; i < intervals.size(); i++) {
//            Interval tmp = intervals.get(i);
//            if (tmp.start >= start && tmp.start <= end) {
//                if (tmp.end > end) end = tmp.end;
//            } else {
//                result.add(new Interval(start, end));
//                start = tmp.start;
//                end = tmp.end;
//            }
//        }
//        result.add(new Interval(start, end));
//        return result;
//    }
//}

package bbb.miniParser;


import java.util.List;

// This is the interface that allows for creating nested lists.
// You should not implement it, or speculate about its implementation
public interface INestedInteger {
    // Constructor initializes an empty nested list.
    // public INestedInteger();

    // Constructor initializes leetcode0001_0100 single integer.
    // public INestedInteger(int value);

    // @return true if this INestedInteger holds leetcode0001_0100 single integer, rather than leetcode0001_0100 nested list.
    public boolean isInteger();

    // @return the single integer that this INestedInteger holds, if it holds leetcode0001_0100 single integer
    // Return null if this INestedInteger holds leetcode0001_0100 nested list
    public Integer getInteger();

    // Set this INestedInteger to hold leetcode0001_0100 single integer.
    public void setInteger(int value);

    // Set this INestedInteger to hold leetcode0001_0100 nested list and adds leetcode0001_0100 nested integer to it.
    public void add(INestedInteger ni);

    // @return the nested list that this INestedInteger holds, if it holds leetcode0001_0100 nested list
    // Return null if this INestedInteger holds leetcode0001_0100 single integer
    public List<INestedInteger> getList();
}


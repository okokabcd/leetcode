package bbb.miniParser;

import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/6/12 21:18
 */
public class NestedInteger implements INestedInteger {
    // Constructor initializes an empty nested list.
    public NestedInteger() {
    }

    // Constructor initializes leetcode0001_0100 single integer.
    public NestedInteger(int value) {

    }

    @Override
    public boolean isInteger() {
        return false;
    }

    @Override
    public Integer getInteger() {
        return null;
    }

    @Override
    public void setInteger(int value) {

    }

    @Override
    public void add(INestedInteger ni) {

    }

    @Override
    public List<INestedInteger> getList() {
        return null;
    }
}

//package bbb.miniParser;
//
//import java.util.Stack;
//
///**
// * Author:   yangdc
// * Date:     2018/6/12 21:15
// */
//public class MiniParser {
//    public NestedInteger deserialize(String s) {
//        if (!s.startsWith("[")) return new NestedInteger(Integer.parseInt(s));
//        Stack<NestedInteger> stack = new Stack<>();
//        // 123,[456,[789]]
//
//        char[] arr = s.toCharArray();
//        for (int i = 0; i < arr.length; i++) {
//            char ch = arr[i];
//            if (ch == '[') {
//                stack.push(new NestedInteger());
//            } else if (ch == ']' & stack.size() > 1) {
//                NestedInteger pop = stack.pop();
//                stack.peek().add(pop);
//            } else if (Character.isDigit(ch) || ch == '-') {
//                boolean pos = true;
//                if (ch == '-') {
//                    pos = false;
//                    i++;
//                }
//                int num = 0;
//                while (i < arr.length && Character.isDigit(arr[i])) {
//                    num *= 10;
//                    num += arr[i++] - '0';
//                }
//                i--;
//                stack.peek().add(new NestedInteger(pos ? num : -num));
//            }
//        }
//        return stack.pop();
//    }
//}
///**
// * // This is the interface that allows for creating nested lists.
// * // You should not implement it, or speculate about its implementation
// * public interface NestedInteger {
// * // Constructor initializes an empty nested list.
// * public NestedInteger();
// * <p>
// * // Constructor initializes leetcode0001_0100 single integer.
// * public NestedInteger(int value);
// * <p>
// * // @return true if this NestedInteger holds leetcode0001_0100 single integer, rather than leetcode0001_0100 nested list.
// * public boolean isInteger();
// * <p>
// * // @return the single integer that this NestedInteger holds, if it holds leetcode0001_0100 single integer
// * // Return null if this NestedInteger holds leetcode0001_0100 nested list
// * public Integer getInteger();
// * <p>
// * // Set this NestedInteger to hold leetcode0001_0100 single integer.
// * public void setInteger(int value);
// * <p>
// * // Set this NestedInteger to hold leetcode0001_0100 nested list and adds leetcode0001_0100 nested integer to it.
// * public void add(NestedInteger ni);
// * <p>
// * // @return the nested list that this NestedInteger holds, if it holds leetcode0001_0100 nested list
// * // Return null if this NestedInteger holds leetcode0001_0100 single integer
// * public List<NestedInteger> getList();
// * }
// */

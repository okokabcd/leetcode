package bbb.randomPickwithBlacklist;

import java.util.*;

/**
 * Author:   admin
 * Date:     2018/7/13 15:13
 */
public class RandomPickwithBlacklist {
    public static void main(String[] args) {
        int N = 2;
        int[] blacklist = new int[0];
        Solution s = new Solution(N, blacklist);
        for (int i = 0; i < 10; i++) {
            System.out.println(s.pick());
            System.out.println("--------------");
        }
    }
}

class Solution {
    int M;
    Map<Integer, Integer> map;
    Random r;
    public Solution(int N, int[] blacklist) {
        M = N - blacklist.length;
        map = new HashMap<>();
        r = new Random();
        for (int tmp : blacklist) {
            map.put(tmp, -1);
        }

        for (int tmp : blacklist) {
            if (tmp < M) {
                while (map.containsKey(N-1)) {
                    N--;
                }
                map.put(tmp, --N);
            }
        }
    }

    public int pick() {
        // random in [0,N) not in blacklist
        int p = r.nextInt(M);
        if (map.containsKey(p)) return map.get(p);
        return p;
    }
}

class Solution3 {

    int N;
    Set<Integer> blackSet;
    // int[] blacklist;
    Random random;
    int[] sampling;
    int samplingIndex = 0;
    public Solution3(int N, int[] blacklist) {
        this.N = N;
        blackSet = new HashSet<>();
        for (int tmp : blacklist) {
            blackSet.add(tmp);
        }
        random = new Random();
        // blacklist = blacklist;

        Map<Integer, Integer> map = new HashMap<>();

        // init
        sampling((N - blacklist.length) > 1000 ? 1000 : (N - blacklist.length));
    }

    // Reservoir Sampling
    private void sampling(int k) {
        // int[] sampling = new int[k];
        sampling = new int[k];
        int j = 0;
        for (int i=0; i<N; i++) {
            if (blackSet.contains(i)) continue; // 排除黑名单的元素
            if (j < k) { // 前k个元素直接放入数组
                sampling[j++] = i;
            } else { // k+1个元素之后开始进行采样
                int r = random.nextInt(i + 1);
                if (r < k) sampling[r] = i;
            }
        }
    }

    public int pick() {
        // random in [0,N) not in blacklist
        if (samplingIndex == sampling.length) {
            sampling((N - blackSet.size()) > 16 ? 16 : (N - blackSet.size()));
            samplingIndex = 0;
        }
        return sampling[samplingIndex++];
    }
}

// 此方案超时
class Solution2 {

    int N;
    Set<Integer> blackSet;
    Random random;

    public Solution2(int N, int[] blacklist) {
        this.N = N;
        blackSet = new HashSet<>();
        for (int tmp : blacklist) {
            blackSet.add(tmp);
        }
        random = new Random();
        // System.out.println(N);
    }

    public int pick() {
        // if (blackSet.size() == 0) return 0;
        // random in [0,N) not in blacklist
        int ans = N;
        for (int i = 0; i < N; i++) {
            if (blackSet.contains(i)) continue;
            if (ans == N) ans = i;
            if (random.nextBoolean()) ans = i;
        }
        return ans;
    }
}
package bbb.linkedListComponents;

import java.util.HashSet;
import java.util.Set;

/**
 * Author:   admin
 * Date:     2018/7/9 17:15
 */
public class LinkedListComponents {
    public int numComponents(ListNode head, int[] G) {
        Set<Integer> existSet = new HashSet<>();
        for (int tmp : G) {
            existSet.add(tmp);
        }
        int ans = existSet.contains(head.val) ? 1 : 0;
        ListNode cur = head;
        while (cur.next != null) {
            if (!existSet.contains(cur.val) && existSet.contains(cur.next.val)) {
                ans++;
            }
            cur = cur.next;
        }
        return ans;
    }

    public static void main(String[] args) {
        ListNode head = ListNode.stringToListNode("0,1,2,3");
        int[] G = new int[]{0, 1, 3};
        System.out.println(new LinkedListComponents().numComponents(head, G));
    }
}

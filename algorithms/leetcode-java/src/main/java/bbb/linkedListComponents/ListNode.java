package bbb.linkedListComponents;

/**
 * Author:   admin
 * Date:     2018/7/9 17:16
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }

    public static ListNode stringToListNode(String line) {
        ListNode head = new ListNode(0);
        ListNode cur = head;
        for (String tmp : line.split(",")) {
            // ListNode tmpNode = cur;
            cur.next = new ListNode(Integer.parseInt(tmp));
            cur = cur.next;
        }

        return head.next;
    }

    public void println() {
        ListNode cur = this;
        while (cur != null) {
            System.out.print(cur.val + ",");
            cur = cur.next;
        }
        System.out.println();
    }
}


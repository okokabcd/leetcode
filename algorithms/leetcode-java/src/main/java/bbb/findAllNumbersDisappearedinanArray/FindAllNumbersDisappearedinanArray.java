package bbb.findAllNumbersDisappearedinanArray;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 下午5:40:46
 *
 */
public class FindAllNumbersDisappearedinanArray {
	public List<Integer> findDisappearedNumbers(int[] nums) {
		int[] arr = new int[nums.length + 1];
		for (int n : nums) {
			arr[n]++;
		}
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 1; i <= arr.length; i++) {
			if (arr[i] > 0) {
				list.add(i);
			}
		}
		return list;
	}
}

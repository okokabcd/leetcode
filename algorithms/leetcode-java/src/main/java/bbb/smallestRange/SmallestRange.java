package bbb.smallestRange;

import java.util.*;

/**
 * https://leetcode.com/problems/smallest-range/description/
 * Author:   admin
 * Date:     2018/6/10 17:02
 */
public class SmallestRange {
    public int[] smallestRange(List<List<Integer>> nums) {
        // 将输入的二维数据转换为一维数据，去重合并并排序
        Set<Integer> set = new HashSet<>();
        // 找目标对象，即这几个列表的共同整数
        Map<Integer, Element> numElementMap = new HashMap<>();
        for (int i = 0; i < nums.size(); i++) {
            List<Integer> subList = nums.get(i);
            for (Integer tmp : subList) {
                Element element = numElementMap.get(tmp);
                if (element == null) {
                    element = new Element(i, 0, 0);
                    numElementMap.put(tmp, element);
                }
                set.add(tmp); // 去重合并
            }
        }
        List<Integer> list = new ArrayList<>(set);
        Collections.sort(list); // 排序

        // 初始化map
        for (int i = 0; i < list.size(); i++) {
            int num = list.get(i);
            Element element = numElementMap.get(num);
            if (element != null) {
                element.start = list.get(i == 0 ? i : i - 1);
                element.end = list.get(i == list.size() - 1 ? i : i + 1);
            }
        }

        // [leetcode0001_0100,b] b-a最小  a最小
        int start = 0;
        int end = 0;
        int count = 0;
        for (Map.Entry<Integer, Element> entry : numElementMap.entrySet()) {
            Element element = entry.getValue();
            if (count == 0 || (end - start) >= (element.end - element.start)) {
                if (count != 0 && start > element.start) {
                    continue;
                }
                start = element.start;
                end = element.end;
            }
            count++;
        }

        return new int[]{start, end};
    }

    class Element {
        int index;
        int start; // start of range
        int end; // end of range
        public Element(int index, int start, int end) {
            this.index = index;
            this.start = start;
            this.end = end;
        }
    }

    public static void main(String[] args) {
        // 二维数组转list
        int[][] arr = new int[][]{{4,10,15,24,26},{0,9,12,20},{5,18,22,30}};
        List<List<Integer>> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            List<Integer> columnList = new ArrayList<>();
            for (int j = 0; j < arr[i].length; j++) {

                columnList.add(j, arr[i][j]);

            }
            list.add(i, columnList);
        }
        int[] retArr = new SmallestRange().smallestRange(list);
        System.out.println(retArr[0] + "," + retArr[1]);
    }
}

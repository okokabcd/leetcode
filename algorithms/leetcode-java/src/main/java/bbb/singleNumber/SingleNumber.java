package bbb.singleNumber;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/6/16 23:40
 */
public class SingleNumber {
    public int singleNumber(int[] nums) {
        Map<Integer, Integer> countMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer count = countMap.get(nums[i]);
            if (count == null) {
                count = 0;
            }
            countMap.put(nums[i], count + 1);
        }
        int num = -1;
        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() == 1) {
                num = entry.getKey();
                break;
            }
        }
        return num;
    }
}

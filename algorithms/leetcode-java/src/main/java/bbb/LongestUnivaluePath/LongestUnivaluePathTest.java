package bbb.LongestUnivaluePath;

import leetcode.common.TreeNode;
import org.junit.Assert;
import org.junit.Test;

public class LongestUnivaluePathTest {
	@Test
	public void test0() {
		TreeNode t = new TreeNode(5);
		t.left = new TreeNode(4);
		t.left.left = new TreeNode(1);
		t.left.right = new TreeNode(1);
		t.right = new TreeNode(5);
		t.right.right = new TreeNode(5);
		Assert.assertEquals(2, new LongestUnivaluePath().longestUnivaluePath(t));
	}

	@Test
	public void test1() {
		TreeNode t = new TreeNode(1);
		t.left = new TreeNode(4);
		t.left.left = new TreeNode(4);
		t.left.right = new TreeNode(4);
		t.right = new TreeNode(5);
		t.right.right = new TreeNode(5);
		Assert.assertEquals(2, new LongestUnivaluePath().longestUnivaluePath(t));
	}
}

package bbb.nthDigit;

import org.junit.Assert;
import org.junit.Test;

public class NthDigitTest {

	@Test
	public void test0() {
		Assert.assertEquals(0, new NthDigit().findNthDigit(11));
	}
	
	@Test
	public void test1() {
		Assert.assertEquals(3, new NthDigit().findNthDigit(3));
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(1, new NthDigit().findNthDigit(10));
	}
}

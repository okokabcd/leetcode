package bbb.nthDigit;

public class NthDigit {
	public int findNthDigit(int n) {
		if (n < 10) {
			return n;
		}
		long count = 9;
		int len = 1;
		int start = 1;
		while (n > len * count) {
			n -= len * count;
			len++;
			start *= 10;
			count *= 10;
		}
		start += (n - 1) / len;
		return String.valueOf(start).charAt((n - 1) % len) - '0';
	}
}

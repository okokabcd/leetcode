package bbb.judgeRouteCircle;

import org.junit.Assert;
import org.junit.Test;

public class JudgeRouteCircleTest {
	@Test
	public void test0() {
		Assert.assertTrue(new JudgeRouteCircle().judgeCircle("UD"));
	}
	
	@Test
	public void test1() {
		Assert.assertFalse(new JudgeRouteCircle().judgeCircle("LL"));
	}
}

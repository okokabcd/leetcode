package bbb.judgeRouteCircle;

/**
 * 
 * @author yangdc
 * @date 2017年9月27日 下午2:37:51
 *
 */
public class JudgeRouteCircle {
	public boolean judgeCircle(String moves) {
		if (moves == null || moves.length() == 0) {
			return true;
		}
		int r = 0, l = 0, u = 0, d = 0;
		for (char c : moves.toCharArray()) {
			switch (c) {
			case 'U':
				u++;
				break;
			case 'D':
				d++;
				break;
			case 'L':
				l++;
				break;
			case 'R':
				r++;
				break;
			}
		}
		return r == l && u == d;
	}
}

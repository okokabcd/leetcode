package bbb.keyboardRow;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 下午5:09:34
 *
 */
public class KeyboardRow {
	public String[] findWords(String[] words) {
		String[] table = { "qwertyuiop", "asdfghjkl", "zxcvbnm" };
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		int n = 1;
		for (String line : table) {
			for (char c : line.toCharArray()) {
				map.put(c, n);
				map.put(Character.toUpperCase(c), n);
			}
			n++;
		}
		String ret = "";
		for (String word : words) {
			n = map.get(word.charAt(0));
			boolean ok = true;
			for (char c : word.toCharArray()) {
				if (map.get(c) != n) {
					ok = false;
					break;
				}
			}
			if (ok) {
				ret += word + ",";
			}
		}
		return ret.length() == 0 ? new String[0] : ret.split(",");
	}
}

package bbb.keyboardRow;

import org.junit.Assert;
import org.junit.Test;

public class KeyboardRowTest {
	@Test
	public void test0() {
		String[] arr = {"Hello", "Alaska", "Dad", "Peace"};
		Assert.assertArrayEquals(new String[]{"Alaska", "Dad"}, new KeyboardRow().findWords(arr));
	}
}

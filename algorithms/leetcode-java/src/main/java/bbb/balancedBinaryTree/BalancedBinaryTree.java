package bbb.balancedBinaryTree;

import leetcode.common.TreeNode;

/**
 * @author yangdc
 * @date 2017年9月19日 上午10:50:13
 */
public class BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        boolean[] balanced = {true};
        height(root, balanced);
        return balanced[0];
    }

    private int height(TreeNode node, boolean[] balanced) {
        if (node == null) return 0;
        int leftHeight = height(node.left, balanced);
        int rightHeight = height(node.right, balanced);
        balanced[0] = balanced[0] && !(Math.abs(leftHeight - rightHeight) > 1);
        System.out.println(node.val + "\t" + leftHeight + "\t" + rightHeight + "\t" + balanced[0]);
        return balanced[0] ? Math.max(leftHeight, rightHeight) + 1 : -1;
    }

    public static void main(String[] args) {
        // TreeNode root = TreeNodeUtil.buildTree(new Integer[]{1, 2, 2, 3, 3, null, null, 4, 4});
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(3);
        root.left.left.left = new TreeNode(4);
        root.left.left.right = new TreeNode(4);

        System.out.println(new BalancedBinaryTree().isBalanced(root));
    }
}

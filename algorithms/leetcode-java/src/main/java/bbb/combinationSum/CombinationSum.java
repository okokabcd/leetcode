package bbb.combinationSum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author:   admin
 * Date:     2018/6/13 17:56
 */
public class CombinationSum {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(candidates); // 排序
        search(candidates, target, result, new ArrayList<>(), 0);
        return result;
    }

    private void search(int[] candidates, int target, List<List<Integer>> ans, List<Integer> cur, int start) {
        if (target == 0) {
            ans.add(new ArrayList<>(cur)); // 浅拷贝
            return;
        }

        for (int i=start; i<candidates.length; i++) {
            if (candidates[i] > target) break;
            cur.add(candidates[i]);
            search(candidates, target - candidates[i], ans, cur, i);
            cur.remove(cur.size() - 1);
        }
    }

    public static void main(String[] args) {

    }
}

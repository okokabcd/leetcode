package bbb.compareVersionNumbers;

/**
 * Author:   yangdc
 * Date:     2018/8/27 21:59
 */
public class CompareVersionNumbers {
    public int compareVersion(String version1, String version2) {
        String[] v1Arr = version1.split("\\.");
        String[] v2Arr = version2.split("\\.");
        int i = 0, max = Math.max(v1Arr.length, v2Arr.length);
        while (i < max) {
            int v1 = v1Arr.length <= i ? 0 : Integer.parseInt(v1Arr[i]);
            int v2 = v2Arr.length <= i ? 0 : Integer.parseInt(v2Arr[i]);
            if (v1 > v2) {
                return 1;
            } else if (v2 > v1) {
                return -1;
            }
            i++;
        }
        return 0;
    }

    public static void main(String[] args) {
        // String v1 = "0.1";
        String v1 = "1";
        String v2 = "1.1";
        System.out.println(new CompareVersionNumbers().compareVersion(v1, v2));
    }
}

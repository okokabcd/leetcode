package bbb.shortestDistancetoaCharacter;

/**
 * Author:   yangdc
 * Date:     2018/6/28 8:23
 */
public class ShortestDistancetoaCharacter {
    public int[] shortestToChar(String S, char C) {
        int[] store = new int[S.length()];
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == C) {
                store[i] = 0;
                left(S, C, store, i);
                right(S, C, store, i);
            }
        }
        return store;
    }

    void left(String S, char C, int[] store, int target) {
        for (int i = target - 1; i >= 0; i--) {
            if (S.charAt(i) == C) break;
            if (store[i] == 0 || store[i] > target - i) store[i] = target - i;
        }
    }

    void right(String S, char C, int[] store, int target) {
        for (int i = target + 1; i < S.length(); i++) {
            if (S.charAt(i) == C) break;
            if (store[i] == 0 || store[i] > i - target) store[i] = i - target;
        }
    }

    public static void main(String[] args) {
        int[] ret = new ShortestDistancetoaCharacter().shortestToChar("loveleetcode", 'e');
        for (int tmp : ret) {
            System.out.print(tmp + ",");
        }
    }
}

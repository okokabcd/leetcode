package bbb.reverseStringII;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 上午11:05:44
 *
 */
public class ReverseStringII {
	public String reverseStr(String s, int k) {
		int len = s.length();
		char[] carr = s.toCharArray();
		// [2nk,2nk+k-1]
		for (int i = 0; i < len; i += k) {
			System.out.println(i);
			int n = (i/k) % 2;
			if (n == 0) { // 需要反转
				int start = i;
				int end = i + k - 1;
				end = (len-1 > end) ? end : len-1;
				reverse(carr, start, end);
			}
		}
		return new String(carr);
	}

	public void reverse(char[] carr, int a, int b) {
		int len = (b - a) / 2;
		char c = 0;
		for (int i = 0; i <= len; i++) {
			c = carr[a + i];
			carr[a + i] = carr[b - i];
			carr[b - i] = c;
		}
	}

}

package bbb.reverseStringII;

import org.junit.Assert;
import org.junit.Test;

public class ReverseStringIITest {
	@Test
	public void test0() {
		String s = "abcdefg";
		int k = 2;
//		char[] carr = s.toCharArray();
//		new ReverseStringII().reverse(carr, 3, 5);
//		System.out.println(new String(carr));
		Assert.assertEquals("bacdfeg", new ReverseStringII().reverseStr(s, k));
	}
}

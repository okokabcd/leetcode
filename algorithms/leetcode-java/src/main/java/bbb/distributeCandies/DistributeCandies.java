package bbb.distributeCandies;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author admin
 *
 */
public class DistributeCandies {
	public int distributeCandies(int[] candies) {
		int mid = candies.length / 2;
		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
		for (int tmp : candies) {
			if (map.get(tmp) == null) {
				map.put(tmp, true);
			}
		}
		int size = map.size();
		return size > mid ? mid : size;
	}
}

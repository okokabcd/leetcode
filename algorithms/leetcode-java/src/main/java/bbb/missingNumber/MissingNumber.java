package bbb.missingNumber;

import java.util.Arrays;

/**
 * 
 * @author yangdc
 * @date 2017年9月21日 下午2:16:39
 *
 */
public class MissingNumber {
	// Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space complexity?
	public int missingNumber(int[] nums) {
		if (nums.length == 1) {
			if (nums[0] == 1) {
				return 0;
			}
			return 1;
		}
		Arrays.sort(nums);
		for (int i=0; i<nums.length; i++) {
			if (i != nums[i]) {
				return i;
			}
		}
		return nums.length;
	}
}

package bbb.missingNumber;

import org.junit.Assert;
import org.junit.Test;


public class MissingNumberTest {
	@Test
	public void test0() {
		Assert.assertEquals(2, new MissingNumber().missingNumber(new int[]{0, 1, 3}));
	}
	@Test
	public void test1() {
		Assert.assertEquals(2, new MissingNumber().missingNumber(new int[]{1, 0}));
	}
}

package bbb.numberComplement;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 下午4:20:32
 *
 */
public class NumberComplement {
    public int findComplement(int num) {
    	// System.out.println(Integer.toBinaryString(num));
    	// System.out.println(Integer.toBinaryString(Integer.MAX_VALUE));
    	// int len = Integer.toBinaryString(Integer.MAX_VALUE).length();
    	int len2 = Integer.toBinaryString(num).length();
    	String binStr = Integer.toBinaryString(num ^ 0b11111111111111111111111111111111).substring(32 - len2, 32);
    	return Integer.parseInt(binStr, 2);
    }
}

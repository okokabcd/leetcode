package bbb.numberComplement;

import org.junit.Assert;
import org.junit.Test;

public class NumberComplementTest {
	@Test
	public void test0() {
		// new NumberComplement().findComplement(5);
		Assert.assertEquals(2, new NumberComplement().findComplement(5));
	}
	
	@Test
	public void test1() {
		Assert.assertEquals(0, new NumberComplement().findComplement(1));
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(0, new NumberComplement().findComplement(2147483647));
	}
}

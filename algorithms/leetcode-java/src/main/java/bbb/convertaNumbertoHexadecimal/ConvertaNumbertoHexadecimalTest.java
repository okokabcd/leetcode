package bbb.convertaNumbertoHexadecimal;

import org.junit.Assert;
import org.junit.Test;

public class ConvertaNumbertoHexadecimalTest {
	@Test
	public void test0() {
		Assert.assertEquals("1a", new ConvertaNumbertoHexadecimal().toHex(26));
	}
	
	@Test
	public void test1() {
		Assert.assertEquals("ffffffff", new ConvertaNumbertoHexadecimal().toHex(-1));
	}
}

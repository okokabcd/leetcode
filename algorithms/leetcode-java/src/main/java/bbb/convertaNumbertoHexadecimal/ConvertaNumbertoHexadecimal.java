package bbb.convertaNumbertoHexadecimal;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 下午4:36:01
 *
 */
public class ConvertaNumbertoHexadecimal {
    public String toHex(int num) {
        // return Integer.toBinaryString(num);
    	return Integer.toHexString(num);
    }
}

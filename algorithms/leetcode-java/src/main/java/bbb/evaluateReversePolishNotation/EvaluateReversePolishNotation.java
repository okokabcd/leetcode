package bbb.evaluateReversePolishNotation;

import java.util.Stack;

/**
 * Author:   admin
 * Date:     2018/6/8 18:00
 */
public class EvaluateReversePolishNotation {
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String tmp : tokens) {
            if (tmp.length() > 1) {
                stack.push(Integer.parseInt(tmp));
                continue;
            }
            char c = tmp.charAt(0); // String转char
            int a, b;
            switch (c) {
                case '+':
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a + b);
                    break;
                case '-':
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a - b);
                    break;
                case '*':
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a * b);
                    break;
                case '/':
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a / b);
                    break;
                default:
                    stack.push(c - '0'); // char 转 int
            }
        }
        return stack.pop();
    }

    public static void main(String[] args) {
        System.out.println(new EvaluateReversePolishNotation().evalRPN(new String[]{"2", "1", "+", "3", "*"}));
        System.out.println(new EvaluateReversePolishNotation().evalRPN(new String[]{"4", "13", "5", "/", "+"}));
        System.out.println(new EvaluateReversePolishNotation().evalRPN(new String[]{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}));
    }
}

package bbb.subdomainVisitCount;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/7/27 下午9:51
 */
public class SubdomainVisitCount {
    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : cpdomains) {
            String[] arr = str.split(" ");
            int baseCount = Integer.parseInt(arr[0]);
            String tmpSub = arr[1];
            int fromIndex = 0;
            // 7 16
            while (fromIndex != -1) {
                // System.out.println(fromIndex);
                String subDomain = tmpSub.substring(fromIndex + (fromIndex != 0 ? 1 : 0));
                Integer oldCount = map.get(subDomain) == null ? 0 : map.get(subDomain);
                map.put(subDomain, oldCount + baseCount);
                fromIndex = tmpSub.indexOf(".", fromIndex + 1);
            }
        }

        List<String> retList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            retList.add(entry.getValue() + " " + entry.getKey());
        }
        return retList;
    }

    public static void main(String[] args) {
        String str = "9001 discuss.leetcode0001_0100.com";
        String[] arr = str.split(" ");
        int baseCount = Integer.parseInt(arr[0]);
        String tmpSub = arr[1];
        int fromIndex = 0;
        int lastIndex = tmpSub.lastIndexOf(".");
        // 7 16
        while (fromIndex != -1) {
            // System.out.println(tmpSub.indexOf(".", fromIndex + 1));
            System.out.println(fromIndex);
            System.out.println(tmpSub.substring(fromIndex + (fromIndex != 0 ? 1 : 0)));
            fromIndex = tmpSub.indexOf(".", fromIndex + 1);
        }
    }
}

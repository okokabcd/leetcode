package bbb.findPivotIndex;

public class FindPivotIndex {
	public int pivotIndex(int[] nums) {
		if (nums == null || nums.length == 0) {
			return -1;
		}
		if (nums.length == 1) {
			return 0;
		}
		int rightSum = 0;
		for (int tmp : nums) {
			rightSum += tmp;
		}

		int leftSum = 0;
		int pivot = -1;
		for (int i = 0; i < nums.length; i++) {
			leftSum += nums[i];
			if (leftSum == rightSum) {
				pivot = i;
				break;
			}
			rightSum -= nums[i];
		}
		return pivot;
	}
}

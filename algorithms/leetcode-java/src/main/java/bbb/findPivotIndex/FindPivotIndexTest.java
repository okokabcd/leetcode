package bbb.findPivotIndex;

import org.junit.Test;

public class FindPivotIndexTest {
	@Test
	public void test01() {
		int[] nums = new int[] { 1, 7, 3, 6, 5, 6 };
		System.out.println(new FindPivotIndex().pivotIndex(nums));
	}

	@Test
	public void test02() {
		int[] nums = new int[] { -1, -1, -1, 0, 1, 1 };
		System.out.println(new FindPivotIndex().pivotIndex(nums));
	}

	@Test
	public void test03() {
		int[] nums = new int[] { -1, -1, 0, 0, -1, -1 };
		System.out.println(new FindPivotIndex().pivotIndex(nums));
	}
}

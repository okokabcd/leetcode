//package bbb.addTwoNumbers;
//
//
///**
// * Author:   admin
// * Date:     2018/7/11 17:02
// */
//public class AddTwoNumbers {
//    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
//        ListNode ans = new ListNode(0);
//        ListNode curL1 = l1;
//        ListNode curL2 = l2;
//        ListNode curAns = ans;
//        int last = 0;
//        ListNode curAnsPre = ans;
//        while (curL1 != null || curL2 != null) {
//            int valL1 = 0;
//            int valL2 = 0;
//            if (curL1 != null) {
//                valL1 = curL1.val;
//                curL1 = curL1.next;
//            }
//            if (curL2 != null) {
//                valL2 = curL2.val;
//                curL2 = curL2.next;
//            }
//            curAns.val = valL1 + valL2 + last;
//            if (curAns.val >= 10) {
//                curAns.val -= 10;
//                last = 1;
//            } else {
//                last = 0;
//            }
//            curAns.next = new ListNode(1);
//            curAnsPre = curAns;
//            curAns = curAns.next;
//        }
//        curAnsPre.next = last == 0 ? null : curAnsPre.next;
//        return ans;
//    }
//}

package bbb.sortArrayByParity;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/9/19 22:00
 */
public class SortArrayByParity {
    public int[] sortArrayByParity(int[] A) {
        List<Integer> evenList = new ArrayList<>();
        List<Integer> oddList = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) evenList.add(A[i]);
//            bbb oddList.add(A[i]);
        }
        evenList.addAll(oddList);
        int[] retArr = new int[A.length];
        for (int i = 0; i < evenList.size(); i++) {
            retArr[i] = evenList.get(i);
        }
        return retArr;
    }

    public static void main(String[] args) {
        int[] arr = new SortArrayByParity().sortArrayByParity(new int[]{3, 1, 2, 4});
        for (int tmp : arr) {
            System.out.print(tmp + ", ");
        }
    }
}

package bbb.intersectionofTwoArrays;

import org.junit.Assert;
import org.junit.Test;

public class IntersectionofTwoArraysTest {
	@Test
	public void test0() {
		int[] nums1 = new int[]{1, 2, 3, 1};
		int[] nums2 = new int[]{2, 2};
		Assert.assertArrayEquals(new int[]{2}, new IntersectionofTwoArrays().intersection(nums1, nums2));
	}
	
	@Test
	public void test1() {
		int[] nums1 = new int[]{1};
		int[] nums2 = new int[]{1};
		Assert.assertArrayEquals(new int[]{1}, new IntersectionofTwoArrays().intersection(nums1, nums2));
	}
	
	@Test
	public void test2() {
		int[] nums1 = new int[]{4,7,9,7,6,7};
		int[] nums2 = new int[]{5,0,0,6,1,6,2,2,4};
		Assert.assertArrayEquals(new int[]{6, 4}, new IntersectionofTwoArrays().intersection(nums1, nums2));
	}
}

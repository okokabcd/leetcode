package bbb.randomPickIndex;

import java.util.Random;

/**
 * Author:   admin
 * Date:     2018/6/14 12:51
 */
public class RandomPickIndex {
    public static void main(String[] args) {
        System.out.println(3/2);

//        Random random = new Random();
//        for (int i = 1; i < 10; i++) {
//            System.out.println(random.nextInt(i));
//        }
    }
}

class Solution {
    private int[] nums;
    private Random random;

    public Solution(int[] nums) {
        this.nums = nums;
        random = new Random();
    }

    public int pick(int target) {
        int index = -1;
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target && random.nextInt(++count) == 0) {
                index = i;
            }
        }
        return index;
    }
}

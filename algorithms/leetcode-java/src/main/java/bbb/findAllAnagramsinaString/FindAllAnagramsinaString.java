package bbb.findAllAnagramsinaString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Author:   admin
 * Date:     2018/7/17 15:22
 */
public class FindAllAnagramsinaString {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        if (s.length() <= p.length()) return ans;

        // 将p的全排列保存到all中
        Set<String> all = permutation(p);


        // 遍历s
        for (int i=0; i<=s.length() - p.length(); i++) {
            String substr = s.substring(i, i+p.length());
            if (all.contains(substr)) {
                ans.add(i);
            }
        }

        return ans;
    }

    // 返回字符串的全排列，递归实现
    Set<String> permutation(String str) {
        char[] arr = str.toCharArray();
        int start = 0;
        int len = arr.length;
        boolean[] fixed = new boolean[len];
        Set<String> ans = new HashSet<>();
        permutation(arr, start, len, fixed, "", ans);

        return ans;
    }

    void permutation(char[] arr, int start, int len, boolean[] fixed, String cur, Set<String> ans) {
        if (start == len) {
            ans.add(cur);
            return;
        }

        for (int i=0; i<len; i++) {
            if (fixed[i]) continue;
            fixed[i] = true;
            cur += arr[i];
            permutation(arr, start+1, len, fixed, cur, ans);
            cur = cur.substring(0, cur.length()-1);
            fixed[i] = false;
        }
    }
}

package bbb.findAllAnagramsinaString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:   admin
 * Date:     2018/7/17 16:14
 */
public class FindAllAnagramsinaString2 {
    public static void main(String[] args) {
//        "abacbabc"
//        "abc"
//        String s = "abacbabc";
//        String p = "abc";
//        String s = "baa";
//        String p = "aa";
        String s = "ababababab";
        String p = "aab";
        new FindAllAnagramsinaString2().findAnagrams(s, p);
    }

    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        if (s.length() <= p.length()) return ans;

        // 构造map,并初始化target
        Map<Character, Bo> map = new HashMap<>();
        for (char tmp : p.toCharArray()) {
            Bo bo = map.get(tmp);
            if (bo == null) {
                bo = new Bo();
                map.put(tmp, bo);
            }
            bo.target++;
        }

        // 前p.length()项
        for (int i = 0; i < p.length(); i++) {
            char cur = s.charAt(i);
            // map.put(cur, 1 + (map.get(cur) == null ? 0 : map.get(cur)));
            Bo bo = map.get(cur);
            if (bo != null) {
                bo.cur++;
            }
            // System.out.println(cur + "\t" + map);
        }
        if (allOne(map)) ans.add(0);

        for (int i = p.length(); i < s.length(); i++) {
            char cur = s.charAt(i);
            if (map.get(cur) != null) map.get(cur).cur++;
            char last = s.charAt(i - p.length());
            if (map.get(last) != null) map.get(last).cur--;
            if (allOne(map)) ans.add(i - p.length() + 1);
            // System.out.println(cur + "\t" + map);
        }

        return ans;
    }

    public boolean allOne(Map<Character, Bo> map) {
        for (Map.Entry<Character, Bo> entry : map.entrySet()) {
            if (entry.getValue().cur != entry.getValue().target) return false;
        }
        return true;
    }

    class Bo {
        int cur; // 当前数
        int target; // 目标数

        public Bo() {
            this(0, 0);
        }

        public Bo(int cur, int target) {
            this.cur = cur;
            this.target = target;
        }
    }
}

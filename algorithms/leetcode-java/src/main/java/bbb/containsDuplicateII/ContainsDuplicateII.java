package bbb.containsDuplicateII;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:   admin
 * Date:     2018/7/19 18:09
 */
public class ContainsDuplicateII {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int tmp = nums[i];
            if (map.get(tmp) != null) {
                // System.out.println(i + "---" + map.get(tmp));
                if (i - map.get(tmp) <= k) return true;
            }
            map.put(tmp, i);
        }
        return false;
    }

    public static void main(String[] args) {
//        [1,2,3,1,2,3]
//        2
        int[] nums = new int[]{1, 2, 3, 1, 2, 3};
        int k = 2;
        System.out.println(new ContainsDuplicateII().containsNearbyDuplicate(nums, k));
    }
}

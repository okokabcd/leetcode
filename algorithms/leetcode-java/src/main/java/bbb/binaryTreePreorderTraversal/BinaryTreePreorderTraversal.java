package bbb.binaryTreePreorderTraversal;

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/6/6 8:02
 */
public class BinaryTreePreorderTraversal {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        printTree(list, root);
        return list;
    }

    private void printTree(List<Integer> list, TreeNode root) {
        if (root == null) return;
        list.add(root.val);
        printTree(list, root.left);
        printTree(list, root.right);
    }
}

package bbb.guessNumberHigherorLower;

/**
 * 
 * @author yangdc
 * @date 2017年9月22日 下午5:31:01
 *
 */
/* The guess API is defined in the parent class GuessGame.
@param num, your guess
@return -1 if my number is lower, 1 if my number is higher, otherwise return 0
   int guess(int num); */
public class GuessNumberHigherorLower extends GuessGame {
	public GuessNumberHigherorLower(int num) {
		super(num);
	}

	public int guessNumber(int n) {
		int left = 1, right = n;
		while (left <= right) {
			int mid = left + (right - left) / 2;
			int guess = guess(mid);
			if (guess == 1) {
				left = mid + 1;
			}
			if (guess == -1) {
				right = mid - 1;
			}
			if (guess == 0) {
				return mid;
			}
		}
		return -1;
	}
}

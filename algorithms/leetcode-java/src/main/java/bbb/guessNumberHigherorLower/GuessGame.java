package bbb.guessNumberHigherorLower;

public class GuessGame {
	int pick;

	public GuessGame(int pick) {
		this.pick = pick;
	}

	public int guess(int guess) {
		int ret = 0;
		ret = (this.pick > guess ? -1 : 1);
		return this.pick == guess ? 0 : ret;
	}
}

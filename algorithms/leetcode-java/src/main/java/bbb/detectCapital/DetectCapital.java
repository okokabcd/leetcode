package bbb.detectCapital;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 上午10:32:03
 *
 */
public class DetectCapital {
	public boolean detectCapitalUse(String word) {
		boolean r1 = true; // 全大写
		boolean r2 = true; // 全小写
		boolean r3 = true; // 首字母大写
		char[] carr = word.toCharArray();
		for (int i = 0; i < carr.length; i++) {
			char c = carr[i];
			if (c >= 'A' && c <= 'Z') {
				r2 = false;
				if (i > 0) {
					r3 = false;
				}
			} else {
				r1 = false;
				if (i == 0) {
					r3 = false;
				}
			}
		}
		
		return r1 || r2 || r3;
	}
}

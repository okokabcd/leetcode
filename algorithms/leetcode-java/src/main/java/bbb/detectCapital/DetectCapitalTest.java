package bbb.detectCapital;

import org.junit.Assert;
import org.junit.Test;

public class DetectCapitalTest {
	@Test
	public void test0() {
		Assert.assertTrue(new DetectCapital().detectCapitalUse("USA"));
	}
	
	@Test
	public void test1() {
		Assert.assertFalse(new DetectCapital().detectCapitalUse("FlaG"));
	}
}

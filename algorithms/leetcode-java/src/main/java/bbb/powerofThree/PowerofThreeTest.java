package bbb.powerofThree;

import org.junit.Assert;
import org.junit.Test;

public class PowerofThreeTest {
	@Test
	public void test0() {
		Assert.assertFalse(new PowerofThree().isPowerOfThree(1024));
	}
	
	@Test
	public void test1() {
		Assert.assertFalse(new PowerofThree().isPowerOfThree(1023));
	}
	
	@Test
	public void test2() {
		Assert.assertFalse(new PowerofThree().isPowerOfThree(0));
	}
	
	@Test
	public void test3() {
		Assert.assertTrue(new PowerofThree().isPowerOfThree(1));
	}
	
	@Test
	public void test4() {
		Assert.assertFalse(new PowerofThree().isPowerOfThree(-16));
	}
	
	@Test
	public void test5() {
		Assert.assertFalse(new PowerofThree().isPowerOfThree(6));
	}
}


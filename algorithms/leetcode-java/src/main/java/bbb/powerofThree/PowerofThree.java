package bbb.powerofThree;

/**
 * 
 * @author yangdc
 * @date 2017年9月20日 下午3:26:56
 *
 */
public class PowerofThree {
	// Could you do it without using any loop / recursion?
	public boolean isPowerOfThree(int n) {
		boolean[] arr = new boolean[] { false, true, false, true };
		if (n < 3) {
			return arr[n < 0 ? 0 : n];
		}
		while (true) {
			int a = n / 3;
			int b = n % 3;
			if (a < 3 && b == 0) {
				return arr[a];
			}
			if (b != 0) {
				return false;
			}
			n = a;
		}
	}

//    if (n < 1) {
//        return false;
//    }
//
//    while (n % 3 == 0) {
//        n /= 3;
//    }
//
//    return n == 1;
}

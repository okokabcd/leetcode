package bbb.mergekSortedLists;

import bbb.linkedListComponents.ListNode;

/**
 * Author:   admin
 * Date:     2018/8/2 15:08
 */
public class MergekSortedLists {
    public static void main(String[] args) {
        ListNode[] lists = new ListNode[3];
        lists[0] = ListNode.stringToListNode("1,4,5");
        lists[1] = ListNode.stringToListNode("1,3,4");
        lists[2] = ListNode.stringToListNode("2,6");
        ListNode ans = new MergekSortedLists().mergeKLists(lists);

        ans.println();
    }

    public ListNode mergeKLists(ListNode[] lists) {
        ListNode preHead = new ListNode(0);
        ListNode minNode = getMinNode(lists);
        ListNode tmpNode = preHead;
        while (minNode != null) {
            tmpNode.next = minNode;
            tmpNode = minNode;
            minNode = getMinNode(lists);
        }
        return preHead.next;
    }

    ListNode getMinNode(ListNode[] lists) {
        int minIdx = -1;
        int minVal = 0;
        for (int i=0; i<lists.length; i++) {
            ListNode cur = lists[i];
            if (cur != null) {
                if (minIdx == -1 || cur.val < minVal) {
                    minIdx = i;
                    minVal = cur.val;
                }
            }
        }
        if (minIdx != -1) {
            ListNode tmpNode = lists[minIdx];
            lists[minIdx] = tmpNode.next;
            return tmpNode;
        }
        return null;
    }
}

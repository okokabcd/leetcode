package bbb.powerofFour;

/**
 * 
 * @author yangdc
 * @date 2017年9月20日 下午4:06:48
 *
 */
public class PowerofFour {
	// Could you do it without using any loop / recursion?
	public boolean isPowerOfFour(int n) {
		if (n < 1) {
			return false;
		}
		while (n % 4 == 0) {
			n /= 4;
		}
		return n == 1;
	}
}

package bbb.powerofFour;

import org.junit.Assert;
import org.junit.Test;

public class PowerofFourTest {
	@Test
	public void test0() {
		Assert.assertTrue(new PowerofFour().isPowerOfFour(16));
	}
	
	@Test
	public void test1() {
		Assert.assertFalse(new PowerofFour().isPowerOfFour(15));
	}
	
	@Test
	public void test2() {
		Assert.assertFalse(new PowerofFour().isPowerOfFour(0));
	}
	
	@Test
	public void test3() {
		Assert.assertTrue(new PowerofFour().isPowerOfFour(1));
	}
	
	@Test
	public void test4() {
		Assert.assertFalse(new PowerofFour().isPowerOfFour(-16));
	}
	
	@Test
	public void test5() {
		Assert.assertFalse(new PowerofFour().isPowerOfFour(6));
	}
}


package bbb.sumofTwoIntegers;

/**
 * 
 * @author yangdc
 * @date 2017年9月18日 下午2:55:47
 *
 */
public class SumofTwoIntegers {
	public int getSum(int a, int b) {
		if (b == 0) {
			return a;
		}
		int sum, carry;
		sum = a ^ b;
		carry = (a & b) << 1;
		return getSum(sum, carry);
	}
}

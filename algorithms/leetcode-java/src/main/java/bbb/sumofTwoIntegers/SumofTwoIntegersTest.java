package bbb.sumofTwoIntegers;

import org.junit.Assert;
import org.junit.Test;

public class SumofTwoIntegersTest {
	@Test
	public void test0() {
		Assert.assertEquals(3L, new SumofTwoIntegers().getSum(1, 2));
	}
	
	@Test
	public void test1() {
		Assert.assertEquals(55L, new SumofTwoIntegers().getSum(33, 22));
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(5L, new SumofTwoIntegers().getSum(2, 3));
	}
}

package bbb.uniqueMorseCodeWords;


import java.util.HashSet;
import java.util.Set;

/**
 * Author:   yangdc
 * Date:     2018/8/22 21:23
 */
public class UniqueMorseCodeWords {
    public int uniqueMorseRepresentations(String[] words) {
        String[] arr = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        Set<String> codeSet = new HashSet<>();
        for (String word : words) {
            String code = "";
            for (char c : word.toCharArray()) {
                code += arr[c - 'a'];
            }
            if (!codeSet.contains(code)) {
                codeSet.add(code);
            }
        }
        return codeSet.size();
    }

    public static void main(String[] args) {

    }
}

package bbb.toLowerCase;

/**
 * Author:   yangdc
 * Date:     2018/8/21 20:18
 */
public class ToLowerCase {
    public String toLowerCase(String str) {
        char[] arr = str.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 'A' && arr[i] <= 'Z') {
                arr[i] += 32;
            }
        }
        return String.valueOf(arr);
    }

    public static void main(String[] args) {
        // String str = "LOVELY";
        String str = "al&phaBET";
        ToLowerCase main = new ToLowerCase();
        System.out.println(main.toLowerCase(str));
    }
}

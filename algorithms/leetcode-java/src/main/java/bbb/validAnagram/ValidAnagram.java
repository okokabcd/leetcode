package bbb.validAnagram;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年10月5日 上午11:52:34
 *
 */
public class ValidAnagram {
    public boolean isAnagram(String s, String t) {
    	Map<Character, Integer> map1 = new HashMap<Character, Integer>();
    	Map<Character, Integer> map2 = new HashMap<Character, Integer>();
    	for (char c : s.toCharArray()) {
    		if (map1.get(c) == null) {
    			map1.put(c, 1);
    		} else {
    			map1.put(c, map1.get(c) + 1);
    		}
    	}
    	for (char c : t.toCharArray()) {
    		if (map2.get(c) == null) {
    			map2.put(c, 1);
    		} else {
    			map2.put(c, map2.get(c) + 1);
    		}
    	}
    	if (map1.size() == map2.size()) {
    		boolean isAnagram = true;
    		for (Map.Entry<Character, Integer> entry : map1.entrySet()) {
    			if (map2.get(entry.getKey()) == null) {
    				isAnagram = false;
    				break;
    			}
    			int map2Count = map2.get(entry.getKey());
    			if (map2Count != entry.getValue()) {
    				isAnagram = false;
    				break;
    			}
    		}
    		return isAnagram;
    	}
    	return false;
    }
}

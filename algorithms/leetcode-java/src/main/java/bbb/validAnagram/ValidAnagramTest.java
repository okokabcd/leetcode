package bbb.validAnagram;

import org.junit.Assert;
import org.junit.Test;

public class ValidAnagramTest {
	@Test
	public void test0() {
		String s = "anagram";
		String t = "nagaram";
		Assert.assertTrue(new ValidAnagram().isAnagram(s, t));
	}
	
	@Test
	public void test2() {
		String s = "rat";
		String t = "car";
		Assert.assertFalse(new ValidAnagram().isAnagram(s, t));
	}
}

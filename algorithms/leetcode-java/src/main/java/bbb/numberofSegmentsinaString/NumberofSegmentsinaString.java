package bbb.numberofSegmentsinaString;

/**
 * 
 * @author yangdc
 * @date 2017年9月27日 下午1:42:52
 *
 */
public class NumberofSegmentsinaString {
    public int countSegments(String s) {
    	if (s == null || s.trim().length() == 0) {
    		return 0;
    	}
    	s = s.trim();
    	return s.split("\\s+").length;
    }
}

package bbb.numberofSegmentsinaString;

import org.junit.Assert;
import org.junit.Test;

public class NumberofSegmentsinaStringTest {
	@Test
	public void test0() {
		String s = "Hello, my name is John";
		Assert.assertEquals(5, new NumberofSegmentsinaString().countSegments(s));
	}
	
	@Test
	public void test1() {
		String s = ", , , ,        leetcode0001_0100, eaefa";
		Assert.assertEquals(6, new NumberofSegmentsinaString().countSegments(s));
	}
	
	@Test
	public void test2() {
		String s = "    foo    bar";
		Assert.assertEquals(2, new NumberofSegmentsinaString().countSegments(s));
	}
}

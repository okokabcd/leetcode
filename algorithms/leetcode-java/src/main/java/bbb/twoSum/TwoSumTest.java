package bbb.twoSum;

import org.junit.Test;

import static org.junit.Assert.*;

public class TwoSumTest {
	@Test
	public void test0() {
		int[] nums = new int[] { 2, 7, 11, 15 };
		int target = 9;
		TwoSum t = new TwoSum();
		int[] result = t.twoSum(nums, target);
		assertArrayEquals(new int[] { 0, 1 }, result);
	}
}

package bbb.twoSum;

import java.util.*;

/**
 * @author ysue
 * @time 2017年9月17日 上午5:04:45
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        boolean find = false;
        int targeti = -1, targetj = -1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i == j) {
                    continue;
                }
                if (nums[i] + nums[j] == target) {
                    targeti = i;
                    targetj = j;
                    find = true;
                    break;
                }
            }
            if (find) {
                break;
            }
        }
        return new int[]{targeti, targetj};
    }

    public int[] twoSum3(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                result[1] = i;
                result[0] = map.get(target - nums[i]);
                return result;
            }
            map.put(nums[i], i);
        }
        return result;
    }


}
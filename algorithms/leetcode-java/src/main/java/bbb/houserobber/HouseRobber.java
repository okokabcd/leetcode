package bbb.houserobber;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author ysue
 * @time 2017年10月27日 下午9:53:59
 */
public class HouseRobber {
	public int rob(int[] nums) {
        cache.clear();
		if (nums.length == 0) {
			return 0;
		}
		return rob(0, nums);
	}
	
	static Map<Integer, Integer> cache = new HashMap<Integer, Integer>();
	public int rob(int idx, int[] nums) {
		if (idx >= nums.length) { // 边界条件
			return 0;
		}
		if (cache.containsKey(idx)) {
			return cache.get(idx);
		}
		int a = nums[idx] + rob(idx + 2, nums);
		int b = 0 + rob(idx + 1, nums);
		int c = Math.max(a, b);
		cache.put(idx, c);
		return c;
	}
}

package bbb.grayCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   admin
 * Date:     2018/6/26 15:50
 */
public class GrayCode {
    public List<Integer> grayCode(int n) {
        List<Integer> list = new ArrayList<>();
        grayCode(n, 0, list);
        return list;
    }

    void grayCode(int n, int cur, List<Integer> list) {
        if (cur == 0) {
            list.add(0);
        } else if (cur == 1) {
            list.add(1);
        } else {
            int tmpSize = list.size();
            for (int i = tmpSize - 1; i >= 0; i--) {
                // int tmp = 1 << (cur - 1);
                // System.out.println(tmp + "\t" + list.get(i) + "\t" + (list.get(i) | tmp));
                list.add(list.get(i) | (1 << (cur - 1)));
            }
        }
        if (cur < n) grayCode(n, cur + 1, list);
    }

    public static void main(String[] args) {
        List<Integer> list = new GrayCode().grayCode(2);
        for (Integer tmp : list) {
            System.out.println(tmp);
        }
//        int[] arr = new int[]{0, 1, 2};
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println(1 << 1);
//        }
    }
}

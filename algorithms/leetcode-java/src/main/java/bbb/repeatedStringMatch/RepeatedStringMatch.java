package bbb.repeatedStringMatch;

/**
 * 
 * @author ysue
 * @time 2017年10月1日 上午10:07:19
 */
public class RepeatedStringMatch {
    public int repeatedStringMatch(String A, String B) {
        int ret = 1;
        boolean flag = false;
        String aAppend = A;
        int aLen = A.length();
        int bMaxLen = B.length() + 2 * aLen;
        int aAppendLen = aLen;
        while (!flag) {
        	if (aAppendLen > bMaxLen) {
        		break;
        	}
        	if (aAppend.contains(B)) {
        		flag = true;
        		break;
        	}
        	aAppend += A;
        	aAppendLen += aLen;
        	ret++;
        }
        // System.out.println(aAppend);
    	return flag ? ret : -1;
    }
}

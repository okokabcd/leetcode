package bbb.repeatedStringMatch;

import org.junit.Assert;
import org.junit.Test;

public class RepeatedStringMatchTest {
	@Test
	public void test0() {
		String A = "abcd";
		String B = "cdabcdab";
		Assert.assertEquals(3, new RepeatedStringMatch().repeatedStringMatch(A, B));
	}

	@Test
	public void test1() {
		String A = "abcabcabcabc";
		String B = "abac";
		Assert.assertEquals(-1, new RepeatedStringMatch().repeatedStringMatch(A, B));
	}
}

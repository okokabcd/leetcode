package bbb.findBottomLeftTreeValue;

import leetcode.common.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Author:   yangdc
 * Date:     2018/9/2 20:02
 */
public class FindBottomLeftTreeValue {
    public int findBottomLeftValue(TreeNode root) {
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        nodeQueue.offer(root); // 向队列追加元素，如果队列满返回false
        int left = -1;
        while (!nodeQueue.isEmpty()) {
            int curLayerSize = nodeQueue.size();
            for (int i = 0; i < curLayerSize; i++) {
                TreeNode cur = nodeQueue.poll(); // 移除并返回队列头部元素，队列为空返回 null
                if (i == 0) left = cur.val; // 当前层的第一个节点最左结点就是最左侧节点
                if (cur.left != null) nodeQueue.offer(cur.left);
                if (cur.right != null) nodeQueue.offer(cur.right);
            }
        }
        return left;
    }
}

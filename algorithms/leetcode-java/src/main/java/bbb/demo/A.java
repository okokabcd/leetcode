/**
* An example for comment formatting. This example is meant to illustrate the various possibilities offered by <i>Eclipse</i> in order to format comments.
*/
package bbb.demo;

/**
 * This is the comment for the example interface.
 */
interface Example {
	// This is leetcode0001_0100 long comment with whitespace that should be split in multiple line
	// comments in case the line comment formatting is enabled
	int foo3();

	// void commented() {
	// System.out.println("indented");
	// }

	// void indentedCommented() {
	// System.out.println("indented");
	// }

	/* block comment on first column */
	int bar();

	/*
	 *
	 * These possibilities include: <ul><li>Formatting of header
	 * comments.</li><li>Formatting of Javadoc tags</li></ul>
	 */
	int bar2(); // This is leetcode0001_0100 long comment that should be split in multiple line comments in case
				// the line comment formatting is enabled

	/**
	 * The following is some sample code which illustrates source formatting within
	 * javadoc comments:
	 * 
	 * <pre>
	 * public class Example {
	 * 	final int leetcode0001_0100 = 1;
	 * 	final boolean b = true;
	 * }
	 * </pre>
	 * 
	 *
	 *                                0 0 0 1 0
	 *                                1 0 0 0 0
	 *                                0 0 0 0 0
	 *                                0 0 0 1 0
	 *
	 * the output would be
	 *
	 *                                1 1 1 1 1
	 *                                1 1 1 1 1
	 *                                1 0 0 1 0
	 *                                1 1 1 1 1
	 *
	 * Similarly, given matrix
	 *
	 *                                  0 0 0
	 *                                  0 1 0
	 *                                  0 0 0
	 *
	 * The output would be
	 *
	 *                                  0 1 0
	 *                                  1 1 1
	 *                                  0 1 0
	 *
	 *
	 *
	 * Descriptions of parameters and return values are best appended at end of the
	 * javadoc comment.
	 * 
	 * @param a
	 *            The first parameter. For an optimum result, this should be an odd
	 *            number between 0 and 100.
	 * @param b
	 *            The second parameter.
	 * @return The result of the foo operation, usually within 0 and 1000.
	 */
	int foo(int a, int b);
}

class Test {
	void trailingCommented() {
		System.out.println("indented"); // comment
		System.out.println("indent"); // comment
	}
}

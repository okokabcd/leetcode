package bbb.fizzBuzz;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author yangdc
 * @date 2017年9月26日 上午9:13:07
 *
 */
public class FizzBuzz {
	public List<String> fizzBuzz(int n) {
		List<String> list = new ArrayList<String>();
		String[] arr = new String[] { "Fizz", "Buzz", "FizzBuzz" };
		for (int i = 1; i <= n; i++) {
			int a = -1;
			if (i % 3 == 0) {
				a++;
			}
			if (i % 5 == 0) {
				a += 2;
			}
			if (a > -1) {
				list.add(arr[a]);
			} else {
				list.add(String.valueOf(i));
			}
		}
		return list;
	}
}

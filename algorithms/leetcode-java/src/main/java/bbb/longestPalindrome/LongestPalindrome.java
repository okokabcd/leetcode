package bbb.longestPalindrome;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 下午4:44:42
 *
 */
public class LongestPalindrome {
	public int longestPalindrome(String s) {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (char c : s.toCharArray()) {
			int key = (int) c;
			int count = 1;
			if (map.get(key) != null) {
				count = map.get(key) + 1;
			}
			map.put(key, count);
		}
		int oneCount = 0;
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() % 2 != 0) {
				oneCount++;
			}
		}
		if (oneCount == 1) {
			oneCount = 0;
		}
		if (oneCount > 1) {
			oneCount -= 1;
		}
		return s.length() - oneCount;
	}
}

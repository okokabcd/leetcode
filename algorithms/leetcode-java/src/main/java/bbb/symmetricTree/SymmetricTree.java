package bbb.symmetricTree;

import leetcode.common.TreeNode;

/**
 * 
 * @author yangdc
 * @date 2017年9月18日 上午9:46:13
 *
 */
public class SymmetricTree {
    public boolean isSymmetric(TreeNode root) {
    	if (root == null) {
    		return true;
    	}
        return isMirror(root.left, root.right);
    }
    
    private boolean isMirror(TreeNode t1, TreeNode t2) {
    	if (t1 == null && t2 == null) {
    		return true;
    	}
    	if (t1 != null && t2 != null && t1.val != t2.val) {
    		return false;
    	}
    	if (t1 != null && t2 == null) {
    		return false;
    	}
    	if (t1 == null && t2 != null) {
    		return false;
    	}
    	return isMirror(t1.left, t2.right) && isMirror(t1.right, t2.left);
    }
}

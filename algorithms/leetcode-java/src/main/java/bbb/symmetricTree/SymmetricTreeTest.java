package bbb.symmetricTree;

import leetcode.common.TreeNode;
import org.junit.Assert;
import org.junit.Test;


public class SymmetricTreeTest {
	@Test
	public void test0() {
		TreeNode root = new TreeNode(1);
		TreeNode left = new TreeNode(2);
		TreeNode right = new TreeNode(2);
		root.left = left;
		root.right = right;
		left.left = new TreeNode(3);
		left.right = new TreeNode(4);
		right.left = new TreeNode(4);
		right.right = new TreeNode(3);
		Assert.assertTrue(new SymmetricTree().isSymmetric(root));
	}
	
//	@Test
//	public void test1() {
//		TreeNode root = new TreeNode(1);
//		TreeNode left = new TreeNode(2);
//		TreeNode right = new TreeNode(2);
//		root.left = left;
//		root.right = right;
//		left.left = null;
//		left.right = new TreeNode(4);
//		right.left = null;
//		right.right = new TreeNode(3);
//		Assert.assertFalse(new SymmetricTree().isSymmetric(root));
//	}
}

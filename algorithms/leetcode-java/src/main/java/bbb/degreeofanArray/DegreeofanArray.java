package bbb.degreeofanArray;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/6/9 上午10:41
 */
public class DegreeofanArray {
    public int findShortestSubArray(int[] nums) {
        int start = 0;
        int end = 0;
        int degree = 0;
        Map<Integer, Element> numElementMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            Element element = numElementMap.get(num);
            if (element == null) {
                element = new Element(i, 0);
                numElementMap.put(num, element);
            }
            element.degree++;
            int elementEnd  = i;

            // degree最大 end-start最小
            boolean change = i == 0 ? true : false; // 第一个元素要初始化
            if (element.degree >= degree) {
                change = true;
                if (element.degree == degree && (end - start < elementEnd - element.start)) {
                    change = false;
                }
            }
            if (change) {
                start = element.start;
                end = elementEnd;
                degree = element.degree;
            }

        }
        return end - start + 1;
    }

    class Element {
        int start;
        int degree;

        public Element(int start, int degree) {
            this.start = start;
            this.degree = degree;
        }
    }

    public static void main(String[] args) {
        System.out.println(new DegreeofanArray().findShortestSubArray(new int[]{1, 2, 2, 3, 1}));
        System.out.println(new DegreeofanArray().findShortestSubArray(new int[]{1,2,2,3,1,4,2}));
    }
}

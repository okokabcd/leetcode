package bbb.binaryTreePostorderTraversal;

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/6/6 8:18
 */
public class BinaryTreePostorderTraversal {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        printTree(list, root);
        return list;
    }

    private void printTree(List<Integer> list, TreeNode root) {
        if (root == null) return;
        printTree(list, root.left);
        printTree(list, root.right);
        list.add(root.val);
    }
}

package bbb.firstUniqueCharacterinaString;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 上午11:26:58
 *
 */
public class FirstUniqueCharacterinaString {
	public int firstUniqChar(String s) {
		Map<Integer, Integer> map = new HashMap<>();
		for (char c : s.toCharArray()) {
			int key = (int) c;
			int count = 1;
			if (map.get(key) != null) {
				count = map.get(key) + 1;
			}
			map.put(key, count);
		}
		int i = 0;
		for (char c : s.toCharArray()) {
			if (map.get((int) c) == 1) {
				return i; 
			}
			i++;
		}
		return -1;
	}
}

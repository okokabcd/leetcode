package bbb.firstUniqueCharacterinaString;

import org.junit.Assert;
import org.junit.Test;

public class FirstUniqueCharacterinaStringTest {
    @Test
	public void test0() {
    	String s = "leetcode";
    	Assert.assertEquals(0, new FirstUniqueCharacterinaString().firstUniqChar(s));
    }
    
    @Test
    public void test1() {
    	String s = "loveleetcode";
    	Assert.assertEquals(2, new FirstUniqueCharacterinaString().firstUniqChar(s));
    }
}

package bbb.reshapetheMatrix;

/**
 * Author:   admin
 * Date:     2018/7/2 13:03
 */
public class ReshapetheMatrix {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        int originalR = nums.length;
        int originalC = nums[0].length;
        if (originalR * originalC < r * c) {
            return nums;
        }
        int[][] retArr = new int[r][c];
        int count = -1;
        for (int i = 0; i < originalR; i++) {
            for (int j = 0; j < originalC; j++) {
                count++;
                int rIndex = count / c;
                int cIndex = count % c;
                // System.out.println(rIndex + "," + cIndex + "\t" + i + "," + j);
                retArr[rIndex][cIndex] = nums[i][j];
                // System.out.println(nums[i][j]);
            }
        }
        return retArr;
    }

    public static void main(String[] args) {
        // [[1,2],[3,4]]
//        int[][] nums = new int[][]{{1, 2}, {3, 4}};
//        int r = 1;
//        int c = 4;
        int[][] nums = new int[][]{{1, 2, 3, 4}};
        int r = 2;
        int c = 2;
        int[][] ret = new ReshapetheMatrix().matrixReshape(nums, r, c);
        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret[i].length; j++) {
                System.out.print(ret[i][j] + ",");
            }
            System.out.println();
        }
    }
}

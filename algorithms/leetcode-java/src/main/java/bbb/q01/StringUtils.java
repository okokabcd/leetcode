package bbb.q01;

/**
 * 
 * @author yangdc
 * @date 2017年9月29日 上午10:09:02
 *
 */
public class StringUtils {
	// 实现String工具类
	// https://www.shiyanlou.com/contests/lou10/console
	
    public static boolean allIsNotNull(String... s){
        if (s == null) {
        	return false;
        }
        boolean notNullFlag = true;
        for (String tmp : s) {
        	if (tmp == null) {
        		notNullFlag = false;
        		break;
        	}
        }
        return notNullFlag;
    }

    public static boolean allIsNotEmpty(String... s){
    	if (s == null) {
        	return false;
        }
    	boolean notEmpty = true;
    	for (String tmp : s) {
    		if (tmp == null || tmp.length() == 0) {
    			notEmpty = false;
    			break;
    		}
    	}
        return notEmpty;
    }
}


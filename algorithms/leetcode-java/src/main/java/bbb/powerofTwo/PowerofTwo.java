package bbb.powerofTwo;

/**
 * 
 * @author yangdc
 * @date 2017年9月20日 下午3:09:52
 *
 */
public class PowerofTwo {
    public boolean isPowerOfTwo(int n) {
    	if (n < 2) {
    		return new boolean[]{false, true}[n < 0 ? 0 : n];
    	}
        while (true) {
        	int a = n / 2;
        	int b = n % 2;
        	if (a < 2 && b == 0) {
        		return true;
        	}
        	if (b != 0) {
        		return false;
        	}
        	n = a;
        }
    }
}

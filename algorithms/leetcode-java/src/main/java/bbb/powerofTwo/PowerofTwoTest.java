package bbb.powerofTwo;

import org.junit.Assert;
import org.junit.Test;

public class PowerofTwoTest {
	@Test
	public void test0() {
		Assert.assertTrue(new PowerofTwo().isPowerOfTwo(1024));
	}
	
	@Test
	public void test1() {
		Assert.assertFalse(new PowerofTwo().isPowerOfTwo(1023));
	}
	
	@Test
	public void test2() {
		Assert.assertFalse(new PowerofTwo().isPowerOfTwo(0));
	}
	
	@Test
	public void test3() {
		Assert.assertTrue(new PowerofTwo().isPowerOfTwo(1));
	}
	
	@Test
	public void test4() {
		Assert.assertFalse(new PowerofTwo().isPowerOfTwo(-16));
	}
}


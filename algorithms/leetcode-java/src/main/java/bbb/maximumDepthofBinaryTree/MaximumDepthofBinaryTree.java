package bbb.maximumDepthofBinaryTree;

import leetcode.common.TreeNode;

/**
 * 
 * @author yangdc
 * @date 2017年9月18日 上午10:00:44
 *
 */
public class MaximumDepthofBinaryTree {
	public int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		if (root.left == null && root.right == null) {
			return 1;
		}
		if (root.left != null && root.right == null) {
			return maxDepth(root.left) + 1;
		}
		if (root.left == null && root.right != null) {
			return maxDepth(root.right) + 1;
		}
		if (root.left != null && root.right != null) {
			int leftDepth = maxDepth(root.left);
			int rightDepth = maxDepth(root.right);
			return (leftDepth > rightDepth ? leftDepth : rightDepth) + 1;
		}
		return 0;
	}
}

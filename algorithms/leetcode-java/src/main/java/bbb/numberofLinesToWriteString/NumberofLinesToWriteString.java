package bbb.numberofLinesToWriteString;

/**
 * Author:   admin
 * Date:     2018/6/19 13:22
 */
public class NumberofLinesToWriteString {
    public int[] numberOfLines(int[] widths, String S) {
        int left = 0;
        int lines = 0;
        for (char c : S.toCharArray()) {
            left += widths[c - 'a'];
            if (left >= 100) {
                lines ++;
                left = left > 100 ? widths[c - 'a'] : 0;
            }
        }
        lines += left > 0 ? 1 : 0;
        return new int[]{lines, left};
    }

    public static void main(String[] args) {
//        int[] arr = new NumberofLinesToWriteString().numberOfLines(new int[]{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}, "abcdefghijklmnopqrstuvwxyz");
//        System.out.println(arr[0]);
//        System.out.println(arr[1]);

//        int[] arr = new NumberofLinesToWriteString().numberOfLines(new int[]{7,5,4,7,10,7,9,4,8,9,6,5,4,2,3,10,9,9,3,7,5,2,9,4,8,9}, "zlrovckbgjqofmdzqetflraziyvkvcxzahzuuveypqxmjbwrjvmpdxjuhqyktuwjvmbeswxuznumazgxvitdrzxmqzhaaudztgie");
//        System.out.println(arr[0]);
//        System.out.println(arr[1]);

        int[] arr = new NumberofLinesToWriteString().numberOfLines(new int[]{4,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10}, "bbbcccdddaaa");
        System.out.println(arr[0]);
        System.out.println(arr[1]);
    }
}

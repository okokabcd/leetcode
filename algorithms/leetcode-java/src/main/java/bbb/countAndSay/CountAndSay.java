package bbb.countAndSay;

/**
 * 
 * @author ysue
 * @time 2017年9月17日 上午5:37:24
 */
public class CountAndSay {
	public String countAndSay(int n) {
		String input = "11";
		for (int i = 1; i < n - 1; i++) {
			String tmpResult = "";
			char[] arr = input.toCharArray();
			char cur = arr[0];
			int index = 0;
			for (char tmp : arr) {
				if (tmp == cur) {
					index++;
				} else {
					tmpResult += (index + String.valueOf(cur));
					index = 1;
					cur = tmp;
				}
			}
			input = tmpResult + (index + String.valueOf(cur));
		}
		return input;
	}
}

package bbb.convertSortedArraytoBinarySearchTree;

import leetcode.common.TreeNode;
import org.junit.Test;

public class ConvertSortedArraytoBinarySearchTreeTest {
	@Test
	public void test0() {
		int[] nums = new int[]{1, 2, 3, 4, 5, 6};
		TreeNode t = new ConvertSortedArraytoBinarySearchTree().sortedArrayToBST(nums);
		printTree(t);
	}
	
	private void printTree(TreeNode t) {
		if (t == null) {
			return;
		}
		System.out.println(t.val + ",");
		printTree(t.left);
		printTree(t.right);
	}
}

package bbb.convertSortedArraytoBinarySearchTree;

import leetcode.common.TreeNode;

/**
 * 
 * @author yangdc
 * @date 2017年9月19日 上午10:06:56
 *
 */
public class ConvertSortedArraytoBinarySearchTree {
	// 将一个排序的数据转化为平衡二叉树
	public TreeNode sortedArrayToBST(int[] nums) {
		if (nums == null || nums.length == 0) {
			return null;
		}
		// {0, 1, 2, 3, 4, 5}
		// {1, 2, 3, 4, 5, 6}
		int val = nums[nums.length / 2]; // nums[3]
		TreeNode t = new TreeNode(val);
		
		int leftLen = nums.length / 2; // 3
		if (leftLen > 0) {
			int[] leftNums = new int[leftLen];
			System.arraycopy(nums, 0, leftNums, 0, leftLen);
			t.left = sortedArrayToBST(leftNums);
		}

		int rightLen = nums.length - leftLen - 1; // 6 - 3 - 1 = 2 
		if (rightLen > 0) {
			int[] rightNums = new int[rightLen];
			System.arraycopy(nums, leftLen + 1, rightNums, 0, rightLen);
			t.right = sortedArrayToBST(rightNums);
		}
		return t;
	}
}

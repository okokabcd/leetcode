package bbb.groupAnagrams;

import java.util.*;

/**
 * Author:   admin
 * Date:     2018/6/18 17:33
 */
public class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            String key = getSorted(str);

            List<String> list = map.get(key);
            if (list == null) {
                list = new ArrayList<>();
                map.put(key, list);
            }
            list.add(str);

            // if (!map.containsKey(key)) map.put(key, new ArrayList());
            // map.get(key).add(str);
        }
        return new ArrayList<>(map.values()); // map转list
    }

    // 返回根据字符排序后的字符串 如:cba返回abc
    private String getSorted(String str) {
        char[] arr = str.toCharArray();
        Arrays.sort(arr);
        // return new String(arr);
        return String.valueOf(arr); // 这样写效率更高
    }
}

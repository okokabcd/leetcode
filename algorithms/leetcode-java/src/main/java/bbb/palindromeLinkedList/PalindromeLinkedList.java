package bbb.palindromeLinkedList;

import bbb.linkedListComponents.ListNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/7/10 21:24
 */
public class PalindromeLinkedList {
    public boolean isPalindrome(ListNode head) {
        List<Integer> list = new ArrayList<>();
        ListNode cur = head;
        while (cur != null) {
            list.add(cur.val);
            cur = cur.next;
        }
        if (list.size() == 2 && list.get(0).equals(list.get(1))) return true;
        for (int i = 0; i < list.size() / 2; i++) {
            if (list.get(i).equals(list.get(list.size() - i - 1))) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(-129);
        head.next = new ListNode(-129);
        System.out.println(new PalindromeLinkedList().isPalindrome(head));
    }
}

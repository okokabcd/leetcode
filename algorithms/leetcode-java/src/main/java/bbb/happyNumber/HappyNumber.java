package bbb.happyNumber;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author admin
 *
 */
public class HappyNumber {
    public boolean isHappy(int n) {
    	Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
    	
    	int tmp = getResult(n);
    	while (tmp != 1) {
    		if (map.get(tmp) == null) {
    			map.put(tmp, true);
    		} else {
    			return false;
    		}
    		tmp = getResult(tmp);
    	}
    	return true;
    }
    
    public int getResult(int n) {
        int tmp = n;
        int a = tmp / 10;
        int b = tmp % 10;
        int result = 0;
        while (a != 0 || b != 0) {
        	tmp = a;
        	result += Math.pow(b, 2);
        	a = tmp / 10;
        	b = tmp % 10;
        }
        return result;
    }
}

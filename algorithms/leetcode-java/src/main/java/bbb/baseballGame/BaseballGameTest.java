package bbb.baseballGame;

import org.junit.Assert;
import org.junit.Test;

public class BaseballGameTest {
	@Test
	public void test0() {
		String[] ops = new String[] { "5", "2", "C", "D", "+" };
		Assert.assertEquals(30, new BaseballGame().calPoints(ops));
	}

	@Test
	public void test1() {
		String[] ops = new String[] { "5", "-2", "4", "C", "D", "9", "+", "+" };
		Assert.assertEquals(27, new BaseballGame().calPoints(ops));
	}
	
	@Test
	public void test2() {
		String[] ops = new String[] { "-60","D","-36","30","13","C","C","-33","53","79" };
		Assert.assertEquals(-117, new BaseballGame().calPoints(ops));
	}
}

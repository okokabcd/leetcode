package bbb.baseballGame;

/**
 * 
 * @author yangdc
 * @date 2017年10月5日 上午10:30:02
 *
 */
public class BaseballGame {
	public int calPoints(String[] ops) {
		int[] score = new int[ops.length];
		for (int i = 0; i < ops.length; i++) {
			String op = ops[i];
			char c = op.toCharArray()[0];
			if ('C' == c) {
				setLastValid(score, i);
				score[i] = 0;
			} else if ('D' == c) {
				int[] lastValid = getLastValid(score, i);
				score[i] = lastValid[0] * 2;
			} else if ('+' == c) {
				int[] lastValid = getLastValid(score, i);
				score[i] = lastValid[0] + lastValid[1];
			} else {
				int tmpScore = Integer.parseInt(op);
				score[i] = tmpScore;
			}
		}
		int total = 0;
		for (int tmp : score) {
			total += tmp;
		}
		return total;
	}

	private void setLastValid(int[] score, int curI) {
		if (curI > 0 && curI < score.length) {
			for (int i=curI-1; i>=0; i--) {
				if (score[i] != 0) {
					score[i] = 0;
					break;
				}
			}
		}
	}
	
	private int[] getLastValid(int[] score, int curI) {
		int[] retArr = new int[2];
		if (curI > 0 && curI < score.length) {
			int retIndex = 0;
			for (int i = curI - 1; i >= 0; i--) {
				if (score[i] != 0) {
					retArr[retIndex] = score[i];
					if (retIndex == 1) {
						return retArr;
					}
					retIndex++;
				}
			}
		}
		return retArr;
	}
}

package bbb.sqrt;

/**
 * 
 * @author ysue
 * @time 2017年9月17日 下午8:34:48
 */
public class Sqrt {
	public int mySqrt(int x) {
		return (int) Math.sqrt(x);
	}
}

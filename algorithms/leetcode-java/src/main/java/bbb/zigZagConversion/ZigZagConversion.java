//package bbb.zigZagConversion;
//
//import java.util.*;
//
///**
// * Author:   yangdc
// * Date:     2018/8/12 上午12:12
// */
//public class ZigZagConversion {
//
//    public static void main(String[] args) {
//        String s = "PAYPALISHIRING";
//        int n = 4;
//        String result = new ZigZagConversion().convert(s, n);
//        System.out.println(result);
//    }
//
//    public String convert(String s, int n) {
//        if (s.length() < 3) return s;
//        if (n < 2) return s;
//        List<Node> nodeList = new ArrayList<>();
//        int lastCol = -1;
//        for (int i = 0; i < s.length(); i++) {
//            int row = i % (n + n - 2);
//            if (row > n - 1) {
//                row = (n + n - 2) - row;
//            }
//            int col = i % (n + n - 2);
//
//            if (col > n - 1) {
//                col = lastCol + 1;
//            } else if (col == 0) {
//                col = lastCol + 1;
//            } else {
//                col = lastCol;
//            }
//            lastCol = col;
//            // System.out.println(String.valueOf(s.charAt(i)) + ", " + row + ", " + col + ", " + i);
//            nodeList.add(new Node(s.charAt(i), row, col));
//        }
//        nodeList.sort((x, y) -> 10 * (x.row - y.row) + (x.col - y.col));
//        StringBuilder sb = new StringBuilder();
//        for (Node tmp : nodeList) {
//            sb.append(String.valueOf(tmp.c));
//        }
//        return sb.toString();
//    }
//
//    class Node {
//        char c;
//        int row;
//        int col;
//        public Node(char c, int row, int col) {
//            this.c = c;
//            this.row = row;
//            this.col = col;
//        }
//    }
//}

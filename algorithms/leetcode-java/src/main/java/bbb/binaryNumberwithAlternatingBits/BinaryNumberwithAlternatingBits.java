package bbb.binaryNumberwithAlternatingBits;

/**
 * Author:   yangdc
 * Date:     2018/6/28 下午10:04
 */
public class BinaryNumberwithAlternatingBits {
    public boolean hasAlternatingBits(int n) {
        char last = '2';
        for (char c : Integer.toBinaryString(n).toCharArray()) {
            if (c == last) return false;
            last = c;
        }
        return true;
    }
}

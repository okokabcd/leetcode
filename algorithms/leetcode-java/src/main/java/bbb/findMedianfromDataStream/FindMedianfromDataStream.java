package bbb.findMedianfromDataStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author:   admin
 * Date:     2018/6/14 13:54
 */
public class FindMedianfromDataStream {
    public static void main(String[] args) {
        MedianFinder mf = new MedianFinder();
        mf.addNum(1);
        mf.printData();
        mf.addNum(2);
        mf.printData();
//        // mf.addNum(3);
//        System.out.println(mf.findMedian());
    }

}

class MedianFinder {
    private List<Integer> data;

    /**
     * initialize your data structure here.
     */
    public MedianFinder() {
        data = new ArrayList<>();
    }

    public void addNum(int num) {
        if (data.size() == 0) {
            data.add(num);
            return;
        }
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i) > num) {
                data.add(i, num);
                break;
            }
            if (i == data.size() - 1) {
                data.add(num);
                break;
            }
        }
    }

    public double findMedian() {
        Collections.sort(data);
        if (data.size() % 2 == 0) { // 偶数
            return (data.get(data.size() / 2) + data.get(data.size() / 2 - 1)) / 2.0;
        } else { // 奇数
            return data.get(data.size() / 2);
        }
    }

    public void printData() {
        System.out.println("--------------");
        for (int num : data) {
            System.out.println(num);
        }
    }
}

package bbb.hammingDistance;

/**
 * 
 * @author yangdc
 * @date 2017年9月27日 下午2:29:19
 *
 */
public class HammingDistance {
    public int hammingDistance(int x, int y) {
    	int d = x^y;
    	String s = Integer.toBinaryString(d);
    	int count = 0;
    	for (char c : s.toCharArray()) {
    		if (c - '0' == 1) {
    			count++;
    		}
    	}
        return count;
    }
}

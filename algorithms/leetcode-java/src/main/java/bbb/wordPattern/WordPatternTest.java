package bbb.wordPattern;

import org.junit.Assert;
import org.junit.Test;

public class WordPatternTest {
//	pattern = "abba", str = "dog cat cat dog" should return true.
//	pattern = "abba", str = "dog cat cat fish" should return false.
//	pattern = "aaaa", str = "dog cat cat dog" should return false.
//	pattern = "abba", str = "dog dog dog dog" should return false.

	@Test
	public void test0() {
		Assert.assertTrue(new WordPattern().wordPattern("abba", "dog cat cat dog"));
	}

	@Test
	public void test1() {
		Assert.assertFalse(new WordPattern().wordPattern("abba", "dog cat cat fish"));
	}

	@Test
	public void test2() {
		Assert.assertFalse(new WordPattern().wordPattern("aaaa", "dog cat cat dog"));
	}

	@Test
	public void test3() {
		Assert.assertFalse(new WordPattern().wordPattern("abba", "dog dog dog dog"));
	}

	@Test
	public void test4() {
		Assert.assertFalse(new WordPattern().wordPattern("abba", "dog cat cat fish"));
	}
}

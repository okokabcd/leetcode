package bbb.wordPattern;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月21日 下午1:46:53
 *
 */
public class WordPattern {
	// pattern = "abba", str = "dog cat cat dog" should return true.
	public boolean wordPattern(String pattern, String str) {
		char[] carr = pattern.toCharArray();
		String[] sarr = str.split(" ");
		if (carr.length != sarr.length) {
			return false;
		}
		Map<String, Integer> map = new HashMap<String, Integer>();
		Map<Integer, String> map2 = new HashMap<Integer, String>();
		for (int i = 0; i < carr.length; i++) {
			map.put(sarr[i], (int)carr[i]);
			map2.put((int)carr[i], sarr[i]);
		}
		if (map.size() != map2.size()) {
			return false;
		}
		for (int i = 0; i < carr.length; i++) {
			int value = map.get(sarr[i]);
			if (value != carr[i]) {
				return false;
			}
		}
		return true;
	}
}

package bbb.findtheDifference;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 上午11:27:04
 *
 */
public class FindtheDifference {
	public char findTheDifference(String s, String t) {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (char c : s.toCharArray()) {
			int key = (int) c;
			int count = 1;
			if (map.get(key) != null) {
				count = map.get(key) + 1;
			}
			map.put(key, count);
		}
		for (char c : t.toCharArray()) {
			int key = (int) c;
			if (map.get(key) == null) {
				return c;
			} else {
				map.put(key, map.get(key) - 1);
			}
			if (map.get(key) < 0) {
				return c;
			}
		}
		return 1;
	}
}

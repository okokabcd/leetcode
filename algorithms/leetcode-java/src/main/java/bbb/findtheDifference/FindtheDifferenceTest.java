package bbb.findtheDifference;

import org.junit.Assert;
import org.junit.Test;


public class FindtheDifferenceTest {
	@Test
	public void test0() {
		String s = "abcd";
		String t = "abcde";
		Assert.assertEquals('e', new FindtheDifference().findTheDifference(s, t));
	}
	
	@Test
	public void test1() {
		String s = "leetcode";
		String t = "aa";
		Assert.assertEquals("leetcode", new FindtheDifference().findTheDifference(s, t));
	}
}

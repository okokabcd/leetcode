package bbb.reverseWordsinaStringIII;

import org.junit.Assert;
import org.junit.Test;

public class ReverseWordsinaStringIIITest {
	@Test
	public void test0() {
		String s = "Let's take LeetCode contest";
		Assert.assertEquals("s'teL ekat edoCteeL tsetnoc", new ReverseWordsinaStringIII().reverseWords(s));
	}
}

package bbb.reverseWordsinaStringIII;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 上午11:39:24
 *
 */
public class ReverseWordsinaStringIII {
    public String reverseWords(String s) {
    	if (s == null || s.length() == 0) {
    		return s;
    	}
        String str = "";
        for (String tmp : s.split(" ")) {
        	char[] carr = tmp.toCharArray();
        	reverse(carr, 0, carr.length - 1);
        	str += new String(carr) + " ";
        }
        str = str.substring(0, str.length() - 1);
    	return str;
    }
    
	public void reverse(char[] carr, int a, int b) {
		int len = (b - a) / 2;
		char c = 0;
		for (int i = 0; i <= len; i++) {
			c = carr[a + i];
			carr[a + i] = carr[b - i];
			carr[b - i] = c;
		}
	}
}

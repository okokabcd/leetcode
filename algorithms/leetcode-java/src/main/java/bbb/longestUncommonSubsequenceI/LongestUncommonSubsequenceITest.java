package bbb.longestUncommonSubsequenceI;

import org.junit.Assert;
import org.junit.Test;

public class LongestUncommonSubsequenceITest {
	@Test
	public void test0() {
		Assert.assertEquals(3, new LongestUncommonSubsequenceI().findLUSlength("aba", "cdc"));
	}
}

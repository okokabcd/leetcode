package bbb.longestUncommonSubsequenceI;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 上午11:55:48
 *
 */
public class LongestUncommonSubsequenceI {
    public int findLUSlength(String a, String b) {
        if (a.equals(b)) {
        	return -1;
        }
    	return 1;
    }
}

package bbb.nextGreaterElementI;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:   admin
 * Date:     2018/7/4 17:53
 */
public class NextGreaterElementI {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int tmp : nums1) {
            map.put(tmp, -1);
        }
        for (int i = 0; i < nums2.length; i++) {
            int tmp = nums2[i];
            Integer nextGreaterElement = map.get(tmp);
            if (nextGreaterElement != null) {
                for (int j=i+1; j<nums2.length; j++) {
                    if (tmp < nums2[j]) {
                        nextGreaterElement = nums2[j];
                        break;
                    }
                }
                map.put(tmp, nextGreaterElement);
                // System.out.println(tmp + ", " + nextGreaterElement);
            }
        }
        // System.out.println();
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            result[i] = map.get(nums1[i]);
            // System.out.print(result[i] + ",");
        }
        return result;
    }

    public static void main(String[] args) {
        // nums1 = [4,1,2], nums2 = [1,3,4,2]
//        int[] nums1 = new int[]{4,1,2};
//        int[] nums2 = new int[]{1,3,4,2};

//        int[] nums1 = new int[]{2, 1, 3};
//        int[] nums2 = new int[]{2, 3, 1};
        // [1,3,5,2,4] [6,5,4,3,2,1,7]
        int[] nums1 = new int[]{1, 3, 5, 2, 4};
        int[] nums2 = new int[]{6, 5, 4, 3, 2, 1, 7};
        new NextGreaterElementI().nextGreaterElement(nums1, nums2);
    }
}

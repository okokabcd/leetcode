package bbb.reverseVowelsofaString;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:   yangdc
 * Date:     2018/6/28 7:50
 */
public class ReverseVowelsofaString {
    public String reverseVowels(String s) {
        List<Integer> store = new ArrayList<>();
        char[] arr = s.toCharArray();
        for (int i=0; i< arr.length; i++) {
            switch (arr[i]) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    store.add(i);
                    break;
            }
        }
        for (int i=0; i<store.size()/2; i++) {
            int start = store.get(i);
            int end = store.get(store.size() - 1 - i);
            char tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
        }
        return String.valueOf(arr);
    }
}

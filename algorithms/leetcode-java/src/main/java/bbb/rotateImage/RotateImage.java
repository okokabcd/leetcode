package bbb.rotateImage;

/**
 * Author:   yangdc
 * Date:     2018/6/7 19:06
 */
public class RotateImage {
    public void rotate(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n / 2; i++) {
            change(matrix, i);
        }
    }

    private void change(int[][] matrix, int i) {
        int n = matrix.length - 1;
        for (int j = i; j < n - i; j++) {
            int tmp = matrix[i][j];

            matrix[i][j] = matrix[n - j][i];
            matrix[n - j][i] = matrix[n - i][n - j];
            matrix[n - i][n - j] = matrix[j][n - i];
            matrix[j][n - i] = tmp;
        }
    }
}

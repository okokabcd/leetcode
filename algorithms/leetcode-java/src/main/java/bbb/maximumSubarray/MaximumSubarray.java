package bbb.maximumSubarray;

/**
 * 
 * @author yangdc
 * @date 2017年9月17日 下午1:08:53
 *
 */
public class MaximumSubarray {
	public int maxSubArray(int[] nums) {
		int max = nums[0], sum = 0;
		for (int i = 0; i < nums.length; i++) {
			if (sum < 0) {
				sum = nums[i];
			} else {
				sum += nums[i];
			}
			if (sum > max) {
				max = sum;
			}
		}
		return max;
	}
}

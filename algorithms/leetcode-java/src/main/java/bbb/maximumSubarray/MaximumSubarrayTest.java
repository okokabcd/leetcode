package bbb.maximumSubarray;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MaximumSubarrayTest {
	@Test
	public void test0() {
		int[] nums = new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		assertEquals(6, new MaximumSubarray().maxSubArray(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = new int[] { -2, -11, -3, -14, -1, -12, -1, -5, -4 };
		assertEquals(-1, new MaximumSubarray().maxSubArray(nums));
	}

	@Test
	public void testn() {

	}
}

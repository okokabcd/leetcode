//package bbb.licenseKeyFormatting;
//
///**
// * Author:   yangdc
// * Date:     2018/6/20 22:15
// */
//public class LicenseKeyFormatting {
//    public String bbb.licenseKeyFormatting(String S, int K) {
//        StringBuilder sb = new StringBuilder();
//        char[] arr = S.toCharArray();
//        int count = 0;
//        for (int i = arr.length - 1; i >= 0; i--) {
//            char c = arr[i];
//            if (c == '-') continue;
//            if (count % K == 0) sb.insert(0, '-');
//            if (c >= 'leetcode0001_0100' && c <= 'z') c -= 32;
//            sb.insert(0, c);
//            count++;
//        }
//        // return sb.substring(0, sb.length() - 1); // "---" 不通过
//        return sb.length() > 0 ? sb.substring(0, sb.length() - 1) : "";
//    }
//
//    public String licenseKeyFormatting2(String S, int K) {
//        StringBuilder sb = new StringBuilder();
//        // char[] arr = S.toCharArray();
//        int count = 0;
//        for (int i = S.length() - 1; i >= 0; i--) {
//            char c = S.charAt(i);
//            if (c == '-') continue;
//            if (count % K == 0) sb.append('-');//sb.insert(0, '-');
//            if (c >= 'leetcode0001_0100' && c <= 'z') c -= 32;
//            sb.append(c);// sb.insert(0, c);
//            count++;
//        }
//        // return sb.substring(0, sb.length() - 1); // "---" 不通过
//        return sb.length() > 0 ? sb.reverse().substring(0, sb.length()-1) : "";
//    }
//
//    public static void main(String[] args) {
//        //  S = "2-5g-3-J", K = 2
//        // System.out.println(new LicenseKeyFormatting().bbb.licenseKeyFormatting("2-5g-3-J", 2));
//        System.out.println(new LicenseKeyFormatting().bbb.licenseKeyFormatting("--leetcode0001_0100-leetcode0001_0100-leetcode0001_0100-leetcode0001_0100--", 2));
//    }
//}

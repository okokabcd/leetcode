package bbb.arrayPartitionI;

import org.junit.Assert;
import org.junit.Test;


public class ArrayPartitionITest {
	@Test
	public void test0() {
		int[] nums = new int[]{1,4,3,2};
		Assert.assertEquals(4, new ArrayPartitionI().arrayPairSum(nums));
	}
}

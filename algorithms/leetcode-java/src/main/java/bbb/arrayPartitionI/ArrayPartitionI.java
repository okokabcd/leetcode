package bbb.arrayPartitionI;

import java.util.Arrays;

/**
 * 
 * @author yangdc
 * @date 2017年10月13日 下午4:10:55
 *
 */
public class ArrayPartitionI {
	public int arrayPairSum(int[] nums) {
		Arrays.sort(nums);
		int sum = 0;
		for (int i = 0; i < nums.length; i += 2) {
			sum += nums[i];
		}
		return sum;
	}
}

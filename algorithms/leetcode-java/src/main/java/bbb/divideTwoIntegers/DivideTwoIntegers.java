package bbb.divideTwoIntegers;

/**
 * Author:   admin
 * Date:     2018/7/6 15:35
 */
public class DivideTwoIntegers {
    public int divide(int dividend, int divisor) {
        // 防止溢出
        if (dividend == Integer.MIN_VALUE && divisor == -1) return Integer.MAX_VALUE;

        // 获取最终结果的符号
        int sign = ((dividend < 0) ^ (divisor < 0)) ? -1 : 1;
        long dvd = Math.abs((long) dividend);
        long dvs = Math.abs((long) divisor);
        int ans = 0;
        while (dvd >= dvs) {
            long tmp = dvs, multiple = 1;
            while (dvd >= (tmp << 1)) {
                tmp <<= 1;
                multiple <<= 1;
            }
            dvd -= tmp;
            ans += multiple;
        }
        return sign == 1 ? ans : -ans;
    }

    public static void main(String[] args) {
        System.out.println(new DivideTwoIntegers().divide(1073741823, 1));
    }

    public int divide2(int dividend, int divisor) {
        // overflows
        if (dividend == Integer.MIN_VALUE && divisor == -1) return Integer.MAX_VALUE;
        // 给定两个数字，求出它们的商，要求不能使用乘法、除法以及求余操作。
        // return dividend / divisor;

        int ans = 0;
        boolean negative = !((dividend > 0 && divisor > 0) || (dividend < 0 && divisor < 0));
        dividend = Math.abs(dividend);
        divisor = Math.abs(divisor);
        dividend -= divisor;
        while (dividend >= 0) {
            ans++;
            dividend -= divisor;
        }
        return negative ? -ans : ans;
    }
}

package bbb.searchInsertPosition;

/**
 * 
 * @author yangdc
 * @date 2017年9月17日 上午9:46:41
 *
 */
public class SearchInsertPosition {
	public int searchInsert(int[] nums, int target) {
		int index = nums.length;
		for (int i = 0; i < nums.length; i++) {
			if (target <= nums[i]) {
				index = i;
				break;
			}
		}
		return index;
	}
}

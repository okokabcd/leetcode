//package bbb.searchInsertPosition;
//
//import org.junit.Test;
//
//public class SearchInsertPositionTest {
//	@Test
//	public void test0() {
//		int[] nums = new int[] { 1, 3, 5, 6 };
//		int target = 5;
//		assertEquals(2, new SearchInsertPosition().searchInsert(nums, target));
//	}
//
//	@Test
//	public void test1() {
//		int[] nums = new int[] { 1, 3, 5, 6 };
//		int target = 2;
//		assertEquals(1, new SearchInsertPosition().searchInsert(nums, target));
//	}
//
//	@Test
//	public void test2() {
//		int[] nums = new int[] { 1, 3, 5, 6 };
//		int target = 7;
//		assertEquals(4, new SearchInsertPosition().searchInsert(nums, target));
//	}
//
//	@Test
//	public void test3() {
//		int[] nums = new int[] { 1, 3, 5, 6 };
//		int target = 0;
//		assertEquals(0, new SearchInsertPosition().searchInsert(nums, target));
//	}
//
//}

package bbb.firstBadVersion;

/**
 * https://leetcode.com/problems/first-bad-version/description/
 * @author yangdc
 * @date 2017年10月7日 上午11:05:09
 *
 */
public class FirstBadVersion {

	int badVersion = 0;

	public FirstBadVersion(int badVersion) {
		this.badVersion = badVersion;
	}

	public int firstBadVersion(int n) {
		int left = 1;
		int right = n;
		int m = left + (right - left) / 2;
		while (right - left > 1) {
			if (isBadVersion(m)) {
				right = m;
			} else {
				left = m;
			}
			m = left + (right - left) / 2;
		}
		System.out.println("left" + left);
		System.out.println("right" + right);
		System.out.println("m" + m);
		return m == left ? right : left;
	}

	public int firstBadVersion2(int n) {
		int badVersion = n;
		for (int i = 1; i < n; i++) {
			if (isBadVersion(i)) {
				badVersion = i;
				break;
			}
		}
		return badVersion;
	}

	public boolean isBadVersion(int version) {
		return (version >= badVersion);
	}

//	这样写有效率问题
//	public int bbb.firstBadVersion(int n) {
//		int i = n;
//		while (i > 0) {
//			i--;
//			if(!isBadVersion(i)) {
//				return i+1;
//			}
//		}
//		return 0;
//	}
}

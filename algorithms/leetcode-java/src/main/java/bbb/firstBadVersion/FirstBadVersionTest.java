package bbb.firstBadVersion;

import org.junit.Assert;
import org.junit.Test;


public class FirstBadVersionTest {
	@Test
	public void test0() {
		// 2126753390 versions
		// 1702766719 is the first bad version.
		Assert.assertEquals(1702766719, new FirstBadVersion(1702766719).firstBadVersion(2126753390));
	}
	
	@Test
	public void test1() {
		// 共2个版本 版本1是第一个badVersion
		Assert.assertEquals(1, new FirstBadVersion(1).firstBadVersion(2));
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(2, new FirstBadVersion(2).firstBadVersion(2));
	}
	
	@Test
	public void test3() {
		Assert.assertEquals(2147483647, new FirstBadVersion(2147483647).firstBadVersion(2147483647));
	}
	
	@Test
	public void test4() {
		Assert.assertEquals(10000, new FirstBadVersion(10000).firstBadVersion(10000));
	}
}

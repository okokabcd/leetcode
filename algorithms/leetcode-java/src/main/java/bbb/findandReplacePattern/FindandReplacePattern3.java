package bbb.findandReplacePattern;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/8/21 23:11
 */
public class FindandReplacePattern3 {
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        int[] patternPos = getStrPos(pattern);
        List<String> retList = new ArrayList<>();
        for (String word : words) {
            if (Arrays.equals(getStrPos(word), patternPos)) retList.add(word);
        }
        return retList;
    }

    private int[] getStrPos(String str) {
        Map<Character, Integer> posMap = new HashMap<>();
        char[] arr = str.toCharArray();
        int[] posArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (posMap.get(arr[i]) == null) {
                posMap.put(arr[i], i);
            }
            posArr[i] = posMap.get(arr[i]);
        }
        return posArr;
    }
}

package bbb.findandReplacePattern;

import java.util.*;

/**
 * Author:   yangdc
 * Date:     2018/8/21 22:22
 */
public class FindandReplacePattern2 {
    // https://leetcode.com/problems/find-and-replace-pattern/discuss/161288/C++JavaPython-Easy-Understood
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        int[] p = F(pattern);
        List<String> res = new ArrayList<String>();
        for (String w : words)
            if (Arrays.equals(F(w), p)) res.add(w);
        return res;
    }

    public int[] F(String w) {
        Map<Character, Integer> m = new HashMap<>();
        int n = w.length();
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            m.putIfAbsent(w.charAt(i), m.size());
            res[i] = m.get(w.charAt(i));
        }
        return res;
    }

    public static void main(String[] args) {
        String[] words = {"abc", "deq", "mee", "aqq", "dkd", "ccc"};
        String pattern = "abb";
        List<String> ansList = new FindandReplacePattern2().findAndReplacePattern(words, pattern);
        ansList.stream().forEach(System.out::println);
    }
}

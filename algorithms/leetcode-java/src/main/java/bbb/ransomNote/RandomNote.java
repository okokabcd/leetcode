package bbb.ransomNote;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 上午11:27:18
 *
 */
public class RandomNote {
	public boolean canConstruct(String ransomNote, String magazine) {
		Map<Integer, Integer> map = new HashMap<>();
		for (char c : magazine.toCharArray()) {
			int key = (int) c;
			int count = 0;
			if (map.get(key) != null) {
				count = map.get(key);
			}
			map.put((int) c, count + 1);
		}
		for (char c : ransomNote.toCharArray()) {
			int key = (int) c;
			if (map.get(key) == null) {
				return false;
			}
			int count = map.get(key);
			if (count - 1 < 0) {
				return false;
			}
			map.put(key, count - 1);
		}
		return true;
	}
}

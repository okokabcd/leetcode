package bbb.ransomNote;

import org.junit.Assert;
import org.junit.Test;

public class RandomNoteTest {
	@Test
	public void test0() {
		Assert.assertFalse(new RandomNote().canConstruct("leetcode", "b"));
	}

	@Test
	public void test1() {
		Assert.assertFalse(new RandomNote().canConstruct("aa", "ab"));
	}

	@Test
	public void test2() {
		Assert.assertTrue(new RandomNote().canConstruct("aa", "aab"));
	}
	
	@Test
	public void test3() {
		// "bg"
		// "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj"
		Assert.assertTrue(new RandomNote().canConstruct("bg", "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj"));
	}
	
	@Test
	public void test4() {
		String a = "fffbfg";
		String b = "effjfggbffjdgbjjhhdegh";
		Assert.assertTrue(new RandomNote().canConstruct(a, b));
	}
}

package bbb.selfDividingNumbers;

import java.util.List;

import org.junit.Test;

public class SelfDividingNumbersTest {
	@Test
	public void test01() {
		// Input: 
		// left = 1, right = 22
        // Output: 
		// [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
		// [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
		// Assert.assert
		// List<Integer> retList = new SelfDividingNumbers().bbb.selfDividingNumbers(10, 10);
		List<Integer> retList = new SelfDividingNumbers().selfDividingNumbers(1, 22);
		// List<Integer> retList = new SelfDividingNumbers().bbb.selfDividingNumbers(99, 111);
		System.out.println(retList);
	}
}

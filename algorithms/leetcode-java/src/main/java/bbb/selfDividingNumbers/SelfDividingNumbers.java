package bbb.selfDividingNumbers;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author admin
 *
 */
public class SelfDividingNumbers {
	public List<Integer> selfDividingNumbers(int left, int right) {
		List<Integer> retList = new ArrayList<Integer>();
		for (int i = left; i <= right; i++) {
			// 10000
			int tmp = i;
			boolean pass = true;
			while (tmp / 10 != 0 || tmp % 10 != 0) {
				int b = tmp % 10;
				tmp = tmp / 10;
				if (b == 0 || i % b != 0) {
					pass = false;
					break;
				}
			}
			if (pass)
				retList.add(i);
		}
		return retList;
	}
}

package bbb.reverseString;

import org.junit.Assert;
import org.junit.Test;

public class ReverseStringTest {
	@Test
	public void test0() {
		Assert.assertEquals("olleh", new ReverseString().reverseString("hello"));
	}
}

package bbb.reverseString;

/**
 * 
 * @author yangdc
 * @date 2017年9月20日 下午2:53:44
 *
 */
public class ReverseString {
	public String reverseString(String s) {
		if (s == null || s.length() < 2) {
			return s;
		}
		int len = s.length();
		char[] sarr = s.toCharArray();
		for (int i = 0; i < len / 2; i++) {
			char tmp = sarr[i];
			sarr[i] = sarr[len - i - 1];
			sarr[len - i - 1] = tmp;
		}
		return String.valueOf(sarr);
	}
}

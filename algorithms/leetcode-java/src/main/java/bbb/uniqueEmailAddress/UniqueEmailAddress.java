package bbb.uniqueEmailAddress;

import java.util.HashSet;
import java.util.Set;

/**
 * Author:   yangdc
 * Date:     2018/10/30 9:06
 */
public class UniqueEmailAddress {
    public int numUniqueEmails(String[] emails) {
        if (emails == null || emails.length == 0) {
            return 0;
        }

        Set<String> localNames = new HashSet<>();
        for (String email : emails) {
            String[] arr =  email.split("@");
            String localName = arr[0];
            // 忽略加号(+)后边的字符
            int indexOfPlus = localName.indexOf("+");
            if (indexOfPlus != -1) {
                localName = localName.substring(0, indexOfPlus);
            }
            // 替换逗号
            localName = localName.replaceAll("\\.", "");
            localNames.add(localName + "@" + arr[1]);
        }
        return localNames.size();
    }

    public static void main(String[] args) {
        String[] emails = new String[]{"test.email+alex@leetcode0001_0100.com","test.e.mail+bob.cathy@leetcode0001_0100.com","testemail+david@lee.tcode.com"};
        System.out.println(new UniqueEmailAddress().numUniqueEmails(emails));
    }
}

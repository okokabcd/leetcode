package bbb.nondecreasingArray;

/**
 * Author:   yangdc
 * Date:     2018/8/17 15:37
 */
public class NondecreasingArray {
    public boolean checkPossibility(int[] nums) {
        if (nums == null || nums.length < 3) return true;

        int count = 2;
        // 判断前2个
        if (nums[1] < nums[0]) {
            nums[0] = nums[1] - 1;
            count--;
        }
        for (int i = 2; i < nums.length; i++) {
            if (nums[i] < nums[i - 1]) {
                count--;
                if (nums[i - 2] <= nums[i] - 1) {
                    nums[i - 1] = nums[i] - 1;
                } else if (i == nums.length - 1 || nums[i + 1] >= nums[i - 1] + 1) {
                    nums[i] = nums[i - 1] + 1;
                } else {
                    return false;
                }
            }
            // System.out.println(nums[i] + "\t" + nums[i - 1] + "\t" + count);
        }
        // System.out.println(count);

        return count > 0;
    }

    public static void main(String[] args) {
        // int[] nums = {3, 4, 2, 3};
        // int[] nums = {4, 2, 1};
        int[] nums = {1, 2, 5, 3, 3};
        boolean ansBoolean = new NondecreasingArray().checkPossibility(nums);
        System.out.println(ansBoolean);
    }
}

package bbb.sumofLeftLeaves;

import leetcode.common.TreeNode;

/**
 * 
 * @author yangdc
 * @date 2017年9月25日 下午4:20:46
 *
 */
public class SumofLeftLeaves {
	public int sumOfLeftLeaves(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return sum(root);
	}
	
	private int sum(TreeNode root) {
		int sum = 0;
		if (root.left != null) {
			if (root.left.left == null && root.left.right == null) {
				sum += root.left.val;
			}
			sum += sum(root.left);
		}
		if (root.right != null) {
			sum += sum(root.right);
		}
		return sum;
	}
}

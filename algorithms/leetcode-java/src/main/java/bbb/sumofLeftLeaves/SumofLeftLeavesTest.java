package bbb.sumofLeftLeaves;

import leetcode.common.TreeNode;
import org.junit.Assert;
import org.junit.Test;

public class SumofLeftLeavesTest {
	@Test
	public void test0() {
		TreeNode t = new TreeNode(3);
		t.left = new TreeNode(9);
		t.right = new TreeNode(20);
		t.right.left = new TreeNode(15);
		t.right.right = new TreeNode(7);
		Assert.assertEquals(24, new SumofLeftLeaves().sumOfLeftLeaves(t));
	}
	
	@Test
	public void test1() {
		TreeNode t = new TreeNode(1);
		t.left = new TreeNode(2);
		t.right = new TreeNode(3);
		t.left.left = new TreeNode(4);
		t.left.right = new TreeNode(5);
		Assert.assertEquals(4, new SumofLeftLeaves().sumOfLeftLeaves(t));
	}
	
}

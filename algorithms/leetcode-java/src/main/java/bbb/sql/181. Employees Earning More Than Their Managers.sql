-- 181. Employees Earning More Than Their Managers
-- https://leetcode.com/problems/employees-earning-more-than-their-managers/description/
select t1.Name Employee
from Employee t1
join Employee t2 on t1.ManagerId = t2.Id
where t1.ManagerId != ''
and t1.Salary > t2.Salary
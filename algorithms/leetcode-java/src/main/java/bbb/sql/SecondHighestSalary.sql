-- 176. Second Highest Salary
-- https://leetcode.com/problems/second-highest-salary/description/
select max(t1.Salary) SecondHighestSalary
from Employee t1
where t1.Salary != (select max(Salary) from Employee)
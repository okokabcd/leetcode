-- https://leetcode.com/problems/customers-who-never-order/description/
select c.Name Customers
from Customers c
where c.Id not in (select CustomerId from Orders)
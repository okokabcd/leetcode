package bbb.intersectionofTwoArraysII;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年10月5日 上午11:46:38
 *
 */
public class IntersectionofTwoArraysII {
	public int[] intersection(int[] nums1, int[] nums2) {
		if (nums1.length == 0 || nums2.length == 0) {
			return new int[0];
		}
		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
		Map<Integer, Integer> map2 = new HashMap<Integer, Integer>();
		for (int i : nums1) {
			if (map1.get(i) == null) {
				map1.put(i, 1);
			} else {
				map1.put(i, map1.get(i) + 1);
			}
		}
		for (int i : nums2) {
			if (map2.get(i) == null) {
				map2.put(i, 1);
			} else {
				map2.put(i, map2.get(i) + 1);
			}
		}
		List<Integer> l = new ArrayList<Integer>();
		for (Map.Entry<Integer, Integer> entry : map1.entrySet()) {
			if (map2.get(entry.getKey()) != null) {
				int map1Count = entry.getValue();
				int map2Count = map2.get(entry.getKey());
				int count = (map1Count < map2Count) ? map1Count : map2Count;
				for (int i=0; i<count; i++) {
					l.add(entry.getKey());					
				}
			}
		}
		int[] arr = new int[l.size()];
		for (int i = l.size() - 1; i >= 0; i--) {
			arr[i] = l.get(i);
		}
		return arr;
	}
}

package bbb.binaryTreeLevelOrderTraversalII;

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author yangdc
 * @date 2017年9月18日 上午10:07:36
 *
 */
public class BinaryTreeLevelOrderTraversalII {
	public List<List<Integer>> levelOrderBottom(TreeNode root) {
		Map<Integer, List<Integer>> map = new LinkedHashMap<Integer, List<Integer>>();
		order(map, root, 0);
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		for (int i=map.size(); i>=1; i--) {
			list.add(map.get(i));
		}
		if (root != null) {
			List<Integer> levelList = new ArrayList<Integer>();
			levelList.add(root.val);
			list.add(levelList);
		}
		
		return list;
	}

	private void order(Map<Integer, List<Integer>> map, TreeNode t, int level) {
		if (t == null) {
			return;
		}
		order(map, t.left, level + 1);
		order(map, t.right, level + 1);
		if (t.left == null && t.right == null) {
			return;
		}
		int key = level + 1;
		List<Integer> levelList = map.get(key);
		if (levelList == null) {
			levelList = new ArrayList<Integer>();
			map.put(key, levelList);
		}
		if (t.left != null) {
			levelList.add(t.left.val);
		}
		if (t.right != null) {
			levelList.add(t.right.val);
		}		
	}
}

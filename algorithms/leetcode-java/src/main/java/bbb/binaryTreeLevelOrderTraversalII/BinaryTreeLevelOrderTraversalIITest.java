package bbb.binaryTreeLevelOrderTraversalII;

import java.util.List;

import leetcode.common.TreeNode;
import org.junit.Test;

public class BinaryTreeLevelOrderTraversalIITest {
	@Test
	public void test0() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		print(new BinaryTreeLevelOrderTraversalII().levelOrderBottom(root));
	}
	
	@Test
	public void test1() {
		TreeNode root = new TreeNode(3);
		print(new BinaryTreeLevelOrderTraversalII().levelOrderBottom(root));
	}
	
	@Test
	public void test2() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.right = new TreeNode(3);
		root.right.right = new TreeNode(5);
		print(new BinaryTreeLevelOrderTraversalII().levelOrderBottom(root));
	}
	
	@Test
	public void test3() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		print(new BinaryTreeLevelOrderTraversalII().levelOrderBottom(root));
	}
	
	private void print(List<List<Integer>> dest) {
		System.out.print("[");
		for (List<Integer> tmpList : dest) {
			System.out.print("[");
			for (Integer tmpInt : tmpList) {
				System.out.print(tmpInt + ",");
			}
			System.out.print("],");
		}
		System.out.println("]");
	}
}

package bbb.burstBalloons;

/**
 * Author:   yangdc
 * Date:     2018/6/7 23:28
 */
public class BurstBalloons {
    public int maxCoins(int[] nums) {
        // 假设输入是[3,1,5,8]

        // 构造一个新的气球数组[1,3,1,5,8,1]
        int[] fullNums = new int[nums.length + 2];
        fullNums[0] = 1;
        fullNums[fullNums.length - 1] = 1;
        for (int i = 1; i < fullNums.length - 1; i++) fullNums[i] = nums[i - 1];

        // 存储从a气球到b气球的最高得分，初始化为-1，由于b>=a所以存储一半就可以了
        int[][] result = new int[fullNums.length][fullNums.length];
        for (int i = 1; i < result.length; i++) {
            for (int j = i; j < result[0].length; j++) {
                result[i][j] = -1;
            }
        }

        // 相对于构造的数组 从1到4号气球才是我们要打的气球
        return getMax(fullNums, result, 1, fullNums.length - 2);
    }

    private int getMax(int[] fullNums, int[][] result, int begin, int end) {
        if (begin > end) return 0;

        // 如果不是初始值，说明已经计算过该值了，直接返回结果
        if (result[begin][end] != -1) return result[begin][end];

        // 最后结果有4种，最后打3或1或5或8这四种可能，比较取最大值
        for (int pos = begin; pos <= end; pos++) {
            int left = getMax(fullNums, result, begin, pos - 1);
            int mid = fullNums[begin - 1] * fullNums[pos] * fullNums[end + 1];
            int right = getMax(fullNums, result, pos + 1, end);
            int coin = left + mid + right;
            if (coin > result[begin][end]) result[begin][end] = coin;
        }
        return result[begin][end];
    }
}

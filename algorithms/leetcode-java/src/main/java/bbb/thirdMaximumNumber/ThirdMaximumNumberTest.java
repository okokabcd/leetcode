package bbb.thirdMaximumNumber;

import org.junit.Assert;
import org.junit.Test;

public class ThirdMaximumNumberTest {
	
	@Test
	public void test0() {
		int[] nums = new int[]{3, 2, 1};
		Assert.assertEquals(1, new ThirdMaximumNumber().thirdMax(nums));
	}
	
	@Test
	public void test1() {
		int[] nums = new int[]{2, 1};
		Assert.assertEquals(2, new ThirdMaximumNumber().thirdMax(nums));
	}
	
	@Test
	public void test2() {
		int[] nums = new int[]{2, 2, 3, 1};
		Assert.assertEquals(1, new ThirdMaximumNumber().thirdMax(nums));
	}
}

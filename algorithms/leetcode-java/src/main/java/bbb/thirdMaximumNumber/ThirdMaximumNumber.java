package bbb.thirdMaximumNumber;

import java.util.Arrays;

/**
 * 
 * @author yangdc
 * @date 2017年9月26日 上午9:28:55
 *
 */
public class ThirdMaximumNumber {
	public int thirdMax(int[] nums) {
		Arrays.sort(nums);
		int thirdMax = nums[nums.length - 1];
		int count = 1;
		for (int i = nums.length - 2; i > -1; i--) {
			int n = nums[i];
			if (thirdMax > n && count < 3) {
				count++;
				thirdMax = n;
			}
		}
		return count == 3 ? thirdMax : nums[nums.length - 1];
	}
}

package marscode.code06;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2024/9/20
 */
public class Main {
    public static int solution(int n, List<Integer> catsLevels) {
        if (n <= 0) {
            return 0;
        }

        // 初始化鱼干数组为1，因为每只猫至少需要1斤鱼干
        int[] fishbones = new int[n];
        for (int i = 0; i < n; i++) {
            fishbones[i] = 1;
        }

        // 从前向后处理
        for (int i = 1; i < n; i++) {
            if (catsLevels.get(i) > catsLevels.get(i - 1)) {
                fishbones[i] = Math.max(fishbones[i - 1] + 1, fishbones[i]);
            }
        }

        // 从后向前处理
        for (int i = n - 2; i >= 0; i--) {
            if (catsLevels.get(i) > catsLevels.get(i + 1)) {
                fishbones[i] = Math.max(fishbones[i + 1] + 1, fishbones[i]);
            }
        }

        // 计算总鱼干数量
        int totalFishbones = 0;
        for (int fishbone : fishbones) {
            totalFishbones += fishbone;
        }

        return totalFishbones;
    }

    public static void main(String[] args) {
        List<Integer> catsLevels1 = new ArrayList<>();
        catsLevels1.add(1);
        catsLevels1.add(2);
        catsLevels1.add(2);

        List<Integer> catsLevels2 = new ArrayList<>();
        catsLevels2.add(6);
        catsLevels2.add(5);
        catsLevels2.add(4);
        catsLevels2.add(3);
        catsLevels2.add(2);
        catsLevels2.add(16);

        List<Integer> catsLevels3 = new ArrayList<>();
        catsLevels3.add(1);
        catsLevels3.add(2);
        catsLevels3.add(2);
        catsLevels3.add(3);
        catsLevels3.add(3);
        catsLevels3.add(20);
        catsLevels3.add(1);
        catsLevels3.add(2);
        catsLevels3.add(3);
        catsLevels3.add(3);
        catsLevels3.add(2);
        catsLevels3.add(1);
        catsLevels3.add(5);
        catsLevels3.add(6);
        catsLevels3.add(6);
        catsLevels3.add(5);
        catsLevels3.add(5);
        catsLevels3.add(7);
        catsLevels3.add(7);
        catsLevels3.add(4);

        System.out.println(solution(3, catsLevels1) == 4);
        System.out.println(solution(6, catsLevels2) == 17);
        System.out.println(solution(20, catsLevels3) == 35);
    }
}

package marscode.code08;

/**
 * @author yangdc
 * @date 2024/9/23
 */
public class Main {
    public static int solution(int num) {
        String s = String.valueOf(num); // 将数字转换为字符串，方便逐位处理
        int n = s.length();
        if (n == 0) return 0;

        // 动态规划数组
        int[] dp = new int[n + 1];
        dp[0] = 1; // 空字符串只有一种翻译方法
        dp[1] = 1; // 单个字符也只有一种翻译方法

        // 从第二个字符开始进行动态规划
        for (int i = 2; i <= n; i++) {
            // 单独翻译当前字符
            dp[i] = dp[i - 1];

            // 判断前两位是否可以一起翻译
            int twoDigit = Integer.parseInt(s.substring(i - 2, i));
            if (twoDigit >= 10 && twoDigit <= 25) {
                dp[i] += dp[i - 2];
            }
        }

        return dp[n];
    }

    public static void main(String[] args) {
        // 测试用例
        System.out.println(solution(12258) == 5);
        System.out.println(solution(1400112) == 6);
        System.out.println(solution(2110101) == 10);
    }
}


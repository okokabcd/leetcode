package marscode.code09;

import java.util.Stack;

/**
 * @author yangdc
 * @date 2024/9/24
 */
public class Main {
    public static int solution(String expression) {
        Stack<Integer> numbers = new Stack<>();
        Stack<Character> operations = new Stack<>();

        int currentNumber = 0;
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);

            // 处理数字
            if (Character.isDigit(ch)) {
                currentNumber = ch - '0'; // 将字符转换为整数
            }

            // 处理操作符和括号
            if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                // 先处理栈中的运算
                while (!operations.isEmpty() && precedence(operations.peek()) >= precedence(ch)) {
                    currentNumber = applyOperation(numbers.pop(), currentNumber, operations.pop());
                }
                numbers.push(currentNumber); // 将当前数字压入数字栈
                operations.push(ch); // 将当前操作符压入操作符栈
                currentNumber = 0; // 重置当前数字
            } else if (ch == '(') {
                operations.push(ch); // 左括号直接压入操作符栈
            } else if (ch == ')') {
                // 处理括号内的表达式
                while (operations.peek() != '(') {
                    currentNumber = applyOperation(numbers.pop(), currentNumber, operations.pop());
                }
                operations.pop(); // 弹出左括号
                numbers.push(currentNumber); // 将计算结果压入数字栈
                currentNumber = 0; // 重置当前数字
            }
        }

        // 处理剩余的操作
        numbers.push(currentNumber); // 最后的数字入栈
        while (!operations.isEmpty()) {
            currentNumber = applyOperation(numbers.pop(), numbers.pop(), operations.pop());
        }

        return currentNumber; // 返回最终结果
    }

    private static int applyOperation(int a, int b, char op) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b; // 整数除法，自动向下取整
            default:
                return 0;
        }
    }

    private static int precedence(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return 0;
        }
    }

    public static void main(String[] args) {
        // 测试用例
        System.out.println(solution("1+1") == 2);
        System.out.println(solution("3+4*5/(3+2)") == 7);
        System.out.println(solution("4+2*5-2/1") == 12);
        System.out.println(solution("(1+(4+5+2)-3)+(6+8)") == 23);
    }
}


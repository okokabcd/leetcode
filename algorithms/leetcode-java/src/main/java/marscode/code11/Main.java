package marscode.code11;

import java.util.*;

/**
 * @author yangdc
 * @date 2024/9/30
 */
public class Main {
    public static int solution(int xPosition, int yPosition) {
        if (xPosition == yPosition) {
            return 0; // 如果起始位置与目标位置相同，步数为 0
        }
        // 使用队列进行 BFS
        Queue<int[]> queue = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        int step = 1;
        int currentPosition = xPosition;
        queue.offer(new int[]{xPosition, step}); // {当前位置, 当前步数}
        visited.add(currentPosition);

        while (!queue.isEmpty()) {
            int[] current = queue.poll();
            currentPosition = current[0];
            step = current[1];

            // 计算可以到达的三个位置
            for (int nextStep : new int[]{-1, 0, 1}) {
                int nextPosition = currentPosition + nextStep;
                if (nextPosition == yPosition && step > 0) {
                    return step + 1;
                }
                if (nextPosition >= 0 && nextPosition <= yPosition && !visited.contains(nextPosition)) {
                    visited.add(nextPosition);
                    queue.offer(new int[]{nextPosition, step + 1});
                }
            }
        }
        return -1; // 如果无法到达 yPosition（理论上不会发生）
    }

    public static void main(String[] args) {
        // 测试用例
        System.out.println(solution(12, 6) == 4);
        System.out.println(solution(34, 45) == 6);
        System.out.println(solution(50, 30) == 8);
    }
}


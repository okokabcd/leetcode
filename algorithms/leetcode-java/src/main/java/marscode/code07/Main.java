package marscode.code07;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2024/9/21
 */
public class Main {
    public static int solution(int length, List<Integer> linkedList, int k) {
        // 统计链表中值为k的节点数量
        int count_k = 0;
        for (int value : linkedList) {
            if (value == k) {
                count_k++;
            }
        }

        // 如果没有值为k的节点，或所有节点都是k，返回0
        if (count_k == 0 || count_k == length) {
            return 0;
        }

        // 找到包含最多k的窗口
        int max_k_in_window = 0;
        int current_k_in_window = 0;

        // 先计算第一个长度为count_k的窗口中k的数量
        for (int i = 0; i < count_k; i++) {
            if (linkedList.get(i) == k) {
                current_k_in_window++;
            }
        }
        max_k_in_window = current_k_in_window;

        // 滑动窗口，计算每个长度为count_k的窗口中k的数量
        for (int i = count_k; i < length; i++) {
            if (linkedList.get(i) == k) {
                current_k_in_window++;
            }
            if (linkedList.get(i - count_k) == k) {
                current_k_in_window--;
            }
            max_k_in_window = Math.max(max_k_in_window, current_k_in_window);
        }

        // 需要的最少交换次数等于将非k的元素换成k，差值就是交换次数
        return count_k - max_k_in_window;
    }

    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(1);
        list1.add(2);
        list1.add(1);

        List<Integer> list2 = new ArrayList<>();
        list2.add(0);
        list2.add(0);
        list2.add(1);
        list2.add(0);
        list2.add(0);

        List<Integer> list3 = new ArrayList<>();
        list3.add(6);
        list3.add(1);
        list3.add(6);
        list3.add(3);
        list3.add(6);
        list3.add(10);
        list3.add(40);
        list3.add(6);
        list3.add(6);
        list3.add(12);
        list3.add(6);

        System.out.println(solution(5, list1, 2) == 1);
        System.out.println(solution(5, list2, 1) == 0);
        System.out.println(solution(11, list3, 6) == 3);
    }
}


package marscode.code04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author yangdc
 * @date 2024/9/18
 */
public class Main {

    private static final int MOD = 998244353;

    // 理解：这个方法旨在计算将树划分为指定数量的特殊连通分块的方案数
    // 数据结构选择：使用数组 gifts 存储每个节点的礼物信息，使用列表 tree 存储树的连接关系
    // 算法步骤：
    //  1. 从输入中提取礼物信息和构建树的结构
    //  2. 通过 DFS 计算每个礼物种类对应的节点数量
    //  3. 根据节点数量计算划分方案数
    public static int solution(int nodes, int decorations, List<List<Integer>> input) {
        int[] gifts = input.remove(0).stream().mapToInt(i -> i).toArray();
        List<List<Integer>> tree = new ArrayList<>(nodes + 1);
        for (int i = 0; i <= nodes; i++) {
            tree.add(new ArrayList<>());
        }

        for (List<Integer> edge : input) {
            int u = edge.get(0);
            int v = edge.get(1);
            tree.get(u).add(v);
            tree.get(v).add(u);
        }

        int[] sizes = new int[decorations + 1];
        dfs(1, -1, gifts, tree, sizes);

        // Calculate the number of ways to split the tree
        long result = 1;
        int totalGiftedNodes = 0;
        for (int i = 1; i <= decorations; i++) {
            if (sizes[i] > 0) {
                result = (result * modPow(sizes[i], sizes[i])) % MOD;
                totalGiftedNodes += sizes[i];
            }
        }

        if (totalGiftedNodes!= nodes - (decorations - 1)) {
            return 0;
        }

        // Multiply by the number of ways to distribute unassigned nodes
        result = (result * modPow(decorations, nodes - totalGiftedNodes)) % MOD;

        return (int) result;
    }

    // 理解：这个方法用于深度优先搜索计算每个礼物种类对应的节点数量
    // 数据结构选择：使用数组 sizes 记录每种礼物的节点数量
    // 算法步骤：
    //  1. 如果当前节点有礼物，增加对应礼物种类的计数
    //  2. 递归地遍历子节点
    private static void dfs(int node, int parent, int[] gifts, List<List<Integer>> tree, int[] sizes) {
        if (gifts[node] > 0) {
            sizes[gifts[node]]++;
        }
        for (int child : tree.get(node)) {
            if (child!= parent) {
                dfs(child, node, gifts, tree, sizes);
            }
        }
    }

    // 理解：这个方法用于计算 base 的 exp 次幂对 MOD 取模的结果
    // 数据结构选择：使用循环和位运算来高效计算幂
    // 算法步骤：
    //  1. 通过循环和位运算逐步计算幂
    //  2. 在每次计算过程中对结果取模
    private static long modPow(long base, int exp) {
        long result = 1;
        while (exp > 0) {
            if ((exp & 1) == 1) {
                result = (result * base) % MOD;
            }
            base = (base * base) % MOD;
            exp >>= 1;
        }
        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> testTree1 = new ArrayList<>();
        testTree1.add(Arrays.asList(1, 0, 0, 0, 2, 3, 0)); // Assuming the first element is always 0 and not considered
        testTree1.add(Arrays.asList(1, 7));
        testTree1.add(Arrays.asList(3, 7));
        testTree1.add(Arrays.asList(2, 1));
        testTree1.add(Arrays.asList(3, 5));
        testTree1.add(Arrays.asList(5, 6));
        testTree1.add(Arrays.asList(6, 4));

        List<List<Integer>> testTree2 = new ArrayList<>();
        testTree2.add(Arrays.asList(1, 0, 1, 0, 2));
        testTree2.add(Arrays.asList(1, 2));
        testTree2.add(Arrays.asList(1, 5));
        testTree2.add(Arrays.asList(2, 4));
        testTree2.add(Arrays.asList(3, 5));

        System.out.println(solution(7, 3, testTree1) == 3);
        System.out.println(solution(5, 2, testTree2) == 0);
    }
}

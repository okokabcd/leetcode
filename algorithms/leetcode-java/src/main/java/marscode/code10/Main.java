package marscode.code10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author yangdc
 * @date 2024/9/25
 */
public class Main {
    public static String solution(int distance, int n, List<List<Integer>> gasStations) {
        // Add starting point and destination as gas stations
        gasStations.add(List.of(0, 0)); // Starting point (0 km from destination)
        gasStations.add(List.of(distance, 0)); // Destination (at distance km from start)

        // Sort gas stations by distance from destination
        Collections.sort(gasStations, Comparator.comparingInt(a -> a.get(0)));

        int currentFuel = 200; // Initial fuel in liters
        int fuelTankCapacity = 400; // Capacity of the tank
        int totalCost = 0; // Total cost incurred
        int lastDistance = 0; // Last distance traveled
        int index = 0; // Index for gas stations

        while (currentFuel < distance) {
            // Find the next gas station that we can reach
            int nextStation = -1;
            int minCost = Integer.MAX_VALUE;

            for (int i = index; i < gasStations.size(); i++) {
                int stationDistance = gasStations.get(i).get(0);
                int pricePerLitre = gasStations.get(i).get(1);
                int distanceToStation = stationDistance - lastDistance;

                // If the station is reachable
                if (distanceToStation <= currentFuel) {
                    // Calculate total cost if we decide to refuel here
                    int fuelNeeded = distance - stationDistance;
                    int costToDestination = fuelNeeded > 0 ? fuelNeeded * pricePerLitre : 0;
                    if (costToDestination < minCost) {
                        minCost = costToDestination;
                        nextStation = i;
                    }
                } else {
                    break; // No more reachable stations
                }
            }

            // If we can't find a reachable station
            if (nextStation == -1) {
                return "Impossible";
            }

            // Calculate the distance to the chosen station
            int distanceToNext = gasStations.get(nextStation).get(0) - lastDistance;
            currentFuel -= distanceToNext; // Reduce fuel based on distance traveled
            lastDistance += distanceToNext; // Update last distance
            totalCost += gasStations.get(nextStation).get(1) * (distanceToNext); // Add cost for this fuel
            index = nextStation; // Update index to the next station
            currentFuel = Math.min(currentFuel + (fuelTankCapacity - currentFuel), fuelTankCapacity); // Refuel if needed
        }

        return String.valueOf(totalCost);
    }

    public static void main(String[] args) {
        List<List<Integer>> gasStations1 = new ArrayList<>();
        gasStations1.add(List.of(100, 1));
        gasStations1.add(List.of(200, 30));
        gasStations1.add(List.of(400, 40));
        gasStations1.add(List.of(300, 20));

        List<List<Integer>> gasStations2 = new ArrayList<>();
        gasStations2.add(List.of(100, 999));
        gasStations2.add(List.of(150, 888));
        gasStations2.add(List.of(200, 777));
        gasStations2.add(List.of(300, 999));
        gasStations2.add(List.of(400, 1009));
        gasStations2.add(List.of(450, 1019));
        gasStations2.add(List.of(500, 1399));

        List<List<Integer>> gasStations3 = new ArrayList<>();
        gasStations3.add(List.of(101));
        gasStations3.add(List.of(100, 100));
        gasStations3.add(List.of(102, 1));

        List<List<Integer>> gasStations4 = new ArrayList<>();
        gasStations4.add(List.of(34, 1));
        gasStations4.add(List.of(105, 9));
        gasStations4.add(List.of(9, 10));
        gasStations4.add(List.of(134, 66));
        gasStations4.add(List.of(215, 90));
        gasStations4.add(List.of(999, 1999));
        gasStations4.add(List.of(49, 0));
        gasStations4.add(List.of(10, 1999));
        gasStations4.add(List.of(200, 2));
        gasStations4.add(List.of(300, 500));
        gasStations4.add(List.of(12, 34));
        gasStations4.add(List.of(1, 23));
        gasStations4.add(List.of(46, 20));
        gasStations4.add(List.of(80, 12));
        gasStations4.add(List.of(1, 1999));
        gasStations4.add(List.of(90, 33));
        gasStations4.add(List.of(101, 23));
        gasStations4.add(List.of(34, 88));
        gasStations4.add(List.of(103, 0));
        gasStations4.add(List.of(1, 1));

        System.out.println(solution(500, 4, gasStations1)); // Expected output: 4300
        System.out.println(solution(500, 7, gasStations2)); // Expected output: 410700
        System.out.println(solution(500, 3, gasStations3)); // Expected output: Impossible
        System.out.println(solution(100, 20, gasStations4)); // Expected output: 0
        System.out.println(solution(100, 0, new ArrayList<>())); // Expected output: Impossible
    }
}


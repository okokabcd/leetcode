package marscode.code05;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2024/9/19
 */
public class MainTest {

    @Test
    public void test02() {
        String str = "bljhy+++";
        int plusCount = 1;

        String base32Str = "9876543210mnbvcxzasdfghjklpoiuyt";

    }

    @Test
    public void test01() {
        String str = "foo";
        byte[] bytes = str.getBytes();
        StringBuilder binaryStr = new StringBuilder();
        for (byte b : bytes) {
            System.out.println(b + ":" + byteToBinaryString(b));
            binaryStr.append(byteToBinaryString(b));
        }
        // append zero
        int bitCount = binaryStr.length();
        int tmp = 5 - bitCount % 5;
        int zeroCount = tmp == 5 ? 0 : tmp;
        binaryStr.append("000000".substring(0, zeroCount));

        String base32Str = "9876543210mnbvcxzasdfghjklpoiuyt";
        StringBuilder encodedStr = new StringBuilder();
        for (int i = 0; i < binaryStr.length() / 5; i++) {
            int start = i * 5;
            int aTmp = Integer.parseInt(binaryStr.substring(start, start + 5), 2);
            encodedStr.append(base32Str.charAt(aTmp));
        }

        // append plus
        int left = bitCount % 40;
        int[] map = {0, 6, 4, 3, 1};
        int plusCount = map[left / 8];
        String plusStr = "++++++++";
        encodedStr.append(plusStr.substring(0, plusCount));
        System.out.println(encodedStr);
    }

    // byte转二进制字符串
    public static String byteToBinaryString(byte b) {
        StringBuilder binaryString = new StringBuilder();
        for (int i = 7; i >= 0; i--) {
            binaryString.append((b & (1 << i)) == 0 ? '0' : '1');
        }
        return binaryString.toString();
    }

}

package aaa;

import java.util.Stack;

/**
 * Author:   yangdc
 * Date:     2018/9/14 下午11:18
 */
public class StackTest {
    public static void main(String[] args) {
        // 实例化
        Stack<Integer> stack = new Stack();

        // 进栈
        stack.push(1);
        stack.push(2);
        stack.push(3);

        // 判断是否为空
        System.out.println(stack.empty());

        // 取栈顶值,不出栈
        System.out.println(stack.peek());

        // 出栈
        System.out.println(stack.pop());

    }
}

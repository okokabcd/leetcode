package aaa;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author yangdc
 * @date 2022/8/15
 */
public class DequeTest {
    Deque<String> deque;

    @Before
    public void setup() {
        // 默认构造器
        deque = new LinkedList<>();
        // 先添加的先到尾部
        deque.addFirst("I");
        deque.addFirst("Like");
        deque.addFirst("Java");
        // deque like this:
        // last           first
        // I    Like      Java
    }

    // push addFirst     addLast
    // pop removeFirst   removeLast
    // peek peekFirst    peekLast

    @Test
    public void testGetFirst() {
        Assert.assertEquals("Java", deque.getFirst());
    }

    @After
    public void clear() {
        deque = null;
    }


}

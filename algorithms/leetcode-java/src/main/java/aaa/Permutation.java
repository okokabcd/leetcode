package aaa;

import java.util.ArrayList;
import java.util.List;

/**
 * 全排列
 * Author:   yangdc
 * Date:     2018/6/13 19:14
 */
public class Permutation {


    /**
     * @param nums  需要全排列的数组
     * @param start 开始索引
     * @param len   (length)取几个数进行全排列
     * @param used  记录是否用过
     * @param cur   (current)当前的一个全排列
     * @param ans   (answer)所有全排列
     */
    public void p(int nums[], int start, int len, boolean[] used, List<Integer> cur, List<List<Integer>> ans) {
        if (start == len) {
            ans.add(new ArrayList<>(cur));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (used[i]) continue;
            used[i] = true;
            cur.add(nums[i]);
            p(nums, start + 1, len, used, cur, ans);
            cur.remove(cur.size() - 1);
            used[i] = false;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        List<List<Integer>> ans = new ArrayList<>();
        new Permutation().p(nums, 0, 3, new boolean[nums.length], new ArrayList<>(), ans);
        for (List<Integer> tmpList : ans) {
            for (Integer tmp : tmpList) {
                System.out.print(tmp + ",");
            }
            System.out.println();
        }
        // output
        //1,2,3,
        //1,3,2,
        //2,1,3,
        //2,3,1,
        //3,1,2,
        //3,2,1,
    }
}

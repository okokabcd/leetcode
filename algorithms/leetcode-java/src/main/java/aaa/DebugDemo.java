package aaa;

/**
 * 调试 bbb.demo
 *
 * @author yangdc
 * @date 2021/08/23
 */
public class DebugDemo {
    public static void main(String[] args) {
        new DebugDemo().test01();
    }

    public void test01() {
        // 条件断点
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }
}

package aaa;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/6/6
 */
public class Solution {

    @Test
    public void test01() {
        String str = "abc";
        List<String> ans = new ArrayList<>();
        backTracking(str.toCharArray(), 0, ans);
        for (String tmp : ans) {
            System.out.println(tmp);
        }
    }

    void backTracking(char[] arr, int level, List<String> ans) {
        if (level == arr.length - 1) {
            ans.add(String.valueOf(arr));
            return;
        }
        for (int i = level; i < arr.length; i++) {
            swap(arr, i, level);
            backTracking(arr, level + 1, ans);
            swap(arr, i, level);
        }
    }

    void swap(char[] arr, int i, int j) {
        char tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

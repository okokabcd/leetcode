package aaa;

import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/15
 */
public class StringTest {
    @Test
    public void testSubstring() {
        String str = "123456";
        System.out.println(str.substring(0, 3));
    }

    @Test
    public void test02() {
        StringBuilder sb = new StringBuilder();
        sb.append("LAST_MODIFY_TM%3E=TO_DATE(%27")
                .append("lastModifyDate")
                .append("%27,%27yyyy/mm/dd%27)%20AND%20ROWNUM%3C=")
                .append("end")
                .append("%20and%20ROWNUM%3E=")
                .append("start");
        System.out.println(sb);
        // LAST_MODIFY_TM%3E=TO_DATE(%27lastModifyDate%27,%27yyyy/mm/dd%27)%20AND%20ROWNUM%3C=end%20and%20ROWNUM%3E=start
        // LAST_MODIFY_TM>=TO_DATE('lastModifyDate','yyyy/mm/dd') AND ROWNUM<=end and ROWNUM>=start
    }
}

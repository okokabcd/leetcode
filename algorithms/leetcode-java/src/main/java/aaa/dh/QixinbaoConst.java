package aaa.dh;

/**
 * @author yangdc
 * @date 2022/8/17
 */
public class QixinbaoConst {
    public final static String AES_KEY = "4462985467060569";
    public final static String AES_IV = "4735004672674356";

    /**
     * 客户号
     */
    public final static String TENANT = "dfhk";

    /**
     * 获取 TOKEN URL
     * ?tenant=dfhk&sig=FODMHfemCBKDe5VzNzzf2c6KOoB9jvxn+kQnnNv+u0Y=
     */
    public final static String GET_TOKEN_URL = "http://b-plugin.qixin.com/authTokenApi/fetch_token";

    /**
     * 获取 公司编号 URL
     * ?keyword=小米科技有限责任公司&secret_key=7b7cc5a8-17a7-4f8f-8968-58bc2168d72c&appkey=b2ae43b7-03b2-4dd4-91f0-8206d4903289
     */
    public final static String GET_ID_URL = "http://api.qixin.com/APIService/enterprise/getIdByName";


    public final static String SECRET_KEY = "7b7cc5a8-17a7-4f8f-8968-58bc2168d72c";
    public final static String APPKEY = "b2ae43b7-03b2-4dd4-91f0-8206d4903289";
}

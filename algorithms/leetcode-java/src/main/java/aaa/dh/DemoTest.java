package aaa.dh;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangdc
 * @date 2022/8/22
 */
public class DemoTest {
    public static void main(String[] args) {
        List<FindSomeoneMailVO> list = new ArrayList<>();
        for (int i = 0; i< 3; i++) {
            FindSomeoneMailVO findSomeoneMailVO = new FindSomeoneMailVO();
            findSomeoneMailVO.setBizProduct("Customer");
            findSomeoneMailVO.setPrivateKey("483613e5-1240-4184-a1ef-c018c50ee79c");
            findSomeoneMailVO.setRequestId(String.valueOf(1001 + i));
            findSomeoneMailVO.setFromUserId(String.valueOf(1001 + i));
            findSomeoneMailVO.setToMail("jc-yangdc@chinaunicom.cn");
            findSomeoneMailVO.setTitle("交付计划行");
            findSomeoneMailVO.setContent("交付计划行发送邮箱!");
            //整理数据
            list.add(findSomeoneMailVO);
        }
        // System.out.println(JSONUtil.toJsonStr(list));
    }

    @Test
    public void test03() {
        FindSomeoneMailVO vo = new FindSomeoneMailVO();
        vo.setBizProduct("Customer");
        vo.setPrivateKey("483613e5-1240-4184-a1ef-c018c50ee79c");
        vo.setRequestId(String.valueOf(1001 + 1));
        vo.setFromUserId(String.valueOf(1001 + 1));
        vo.setToMail("jc-yangdc@chinaunicom.cn");
        vo.setTitle("交付计划行");
        vo.setContent("交付计划行发送邮箱!");
        // JSON.toJSONString(vo.getBizProduct());
        // System.out.println(JSONUtil.toJsonStr(vo.getBizProduct()));
        System.out.println(getFindSomeoneMailParam(vo));
        System.out.println(JSON.toJSONString(vo.getBizProduct()));
    }

    private String getFindSomeoneMailParam(FindSomeoneMailVO vo){
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.msgbuss.ceair.com/\">\n" +
                " <soapenv:Header/>\n" +
                " <soapenv:Body>\n" +
                " <ser:send>\n" +
                " <!--Optional:-->\n" +
                " <messageRequestVO>\n" +
                " <!--Optional:-->\n" +
                " <bizProduct>"+JSON.toJSONString(vo.getBizProduct())+"</bizProduct>\n" +
                " <!--Optional:-->\n" +
                " <content>"+JSON.toJSONString(vo.getContent())+"</content>\n" +
                " <!--Optional:-->\n" +
                " <extention></extention>\n" +
                " <!--Optional:-->\n" +
                " <fromUserId>"+JSON.toJSONString(vo.getFromUserId())+"</fromUserId>\n" +
                " <!--Optional:-->\n" +
                " <privateKey>"+JSON.toJSONString(vo.getPrivateKey())+"</privateKey>\n" +
                " <!--Optional:-->\n" +
                " <requestId>"+JSON.toJSONString(vo.getRequestId())+"</requestId>\n" +
                " <!--Optional:-->\n" +
                " <title>"+ JSON.toJSONString(vo.getTitle())+"</title>\n" +
                "<!--Optional:--> <toMail>"+JSON.toJSONString(vo.getToMail())+"</toMail>\n" +
                " <!--Optional:-->\n" +
                " <toMobile></toMobile>\n" +
                " <!--Optional:-->\n" +
                " <toUserId></toUserId>\n" +
                " <!--Optional:-->\n" +
                " <sendTime></sendTime>\n" +
                " </messageRequestVO>\n" +
                " </ser:send>\n" +
                " </soapenv:Body>\n" +
                "</soapenv:Envelope>";
    }

    @Test
    public void test04() {
        // 2207050000327870028
        // 2207050000328953502
        String str = "98765432109876543210";
        System.out.println(StringUtils.rightPad(str, 16, '0'));
        System.out.println(StringUtils.right(str, 16));
    }
}

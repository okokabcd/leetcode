package aaa.dh;

import cn.hutool.crypto.symmetric.AES;

/**
 * @author yangdc
 * @date 2022/8/25
 */
public class AesUtil {
    public final static String KEY = "d4ugrs4ur7ij28r1";
    public final static String IV = "95z65osmwnr7a59v";

    private static AES INSTNACE = new AES("CBC", "PKCS5Padding",
            // 密钥，可以自定义
            KEY.getBytes(),
            // iv加盐，按照实际需求添加
            IV.getBytes());

    /**
     * aes加密
     */
    public static String aesEncrypt(String data) {
        return INSTNACE.encryptBase64(data);
    }

    /**
     * aes解密
     */
    public static String aesDecrypt(String data) {
        return INSTNACE.decryptStr(data);
    }

    public static void main(String[] args) {
        System.out.println(AesUtil.aesEncrypt("abcdefg"));
    }
}

package aaa.dh;

import org.apache.commons.lang3.StringUtils;

/**
 * @author yangdc
 * @date 2022/8/24
 */
public class AesTest {
    public String getRandom(int length) {
        char[] arr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        String result = String.valueOf(arr[(int) Math.floor(Math.random() * 36)]);
        for (int i = 1; i < length; i++) {
            result += arr[(int) Math.floor(Math.random() * 36)];
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(new AesTest().getRandom(16));
        System.out.println(new AesTest().getRandom(16));
    }

    private String substring16(String str) {
        if (str.length() < 16) {
            return StringUtils.rightPad(str, 16, '0');
        }
        return StringUtils.right(str, 16);
    }
}

package aaa.dh;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author yangdc
 * @date 2022/8/22
 */
@Getter
@Setter
public class FindSomeoneMailVO implements Serializable {

    private String bizProduct;

    private String privateKey;

    private String requestId;

    private String fromUserId;

    private String toMail;

    private String title;

    private String content;

    private String extention;

    private LocalDateTime sendTime;

}

package aaa.dh;

import cn.hutool.crypto.symmetric.AES;
import cn.hutool.http.HttpUtil;
import org.junit.Test;

/**
 * @author yangdc
 * @date 2022/8/16
 */
public class QixinbaoTest {

    public AES getAes(String key, String iv) {
        AES aes = new AES("CBC", "PKCS5Padding",
                // 密钥，可以自定义
                key.getBytes(),
                // iv加盐，按照实际需求添加
                iv.getBytes());
        return aes;
    }

    public String encode(String key, String iv, String content) {
        // key:4462985467060569,iv:4735004672674356    客户号：dfhk

        // 加密为16进制表示
        // String encryptHex = aes.encryptHex(content);
        // 解密
        // String decryptStr = aes.decryptStr(encryptHex);
        // return Base64.encode(encryptHex, "UTF-8");
        return getAes(key, iv).encryptBase64(content);
    }

    @Test
    public void generateSig() {
        // key:4462985467060569,iv:4735004672674356    客户号：dfhk
        String content = getContent();
        System.out.println(content);
        System.out.println(encode(QixinbaoConst.AES_KEY, QixinbaoConst.AES_IV, content));
        System.out.println(getAes(QixinbaoConst.AES_KEY, QixinbaoConst.AES_IV).encryptBase64(content));
        // nujloXQ6USN4wnrVA1fgjeheKDp2enA+ydktu+TdAYo=
    }

    @Test
    public void getToken() {
        // https://b-plugin.qixin.com/authTokenApi/fetch_token?tenant=dfhk&sig=FODMHfemCBKDe5VzNzzf2c6KOoB9jvxn+kQnnNv+u0Y=
        String sig = getAes(QixinbaoConst.AES_KEY, QixinbaoConst.AES_IV).encryptBase64(getContent());
        System.out.println(sig);
        String url = String.format("%s?tenant=%s&sig=%s", QixinbaoConst.GET_TOKEN_URL, QixinbaoConst.TENANT, sig);
        System.out.println(url);
        String url2 = String.format("%s?tenant=%s&sig=%s", "http://localhost:8090", QixinbaoConst.TENANT, sig);
        System.out.println(url2);
//        String result= HttpUtil.get(url);
//        System.out.println(result);

        // 企业详情--/company/info?eid=<公司编号>
        // https://b-plugin.qixin.com/third-login?tenant=dfhk&token=d314c8f1f6b8412ebd1af1364694281d&returnUrl=/company/info?eid=534472fd-7d53-4958-8132-d6a6242423d8
        // 高级查询-/search/advanced
        // https://b-plugin.qixin.com/third-login?tenant=dfhk&token=bcfeea64b79e4fd7b5f5d75c0142bb80&returnUrl=/search/advanced?keyword=小米科技有限责任公司
        // 批量查询-/search/batch

        // qixinbao_token
        // tenant
        // token
        // expire_time

        // token: d314c8f1f6b8412ebd1af1364694281d
        // {"status":200,"message":"解密成功","token":"5492536d8ba04698a5b04081dd02b436","expire_time":1660728568}
    }

    @Test
    public void getId() {
        // http://api.qixin.com/APIService/enterprise/getIdByName?keyword=小米科技有限责任公司&secret_key=7b7cc5a8-17a7-4f8f-8968-58bc2168d72c&appkey=b2ae43b7-03b2-4dd4-91f0-8206d4903289
        String keyword = "小米科技有限责任公司";
        String url = String.format("%s?keyword=%s&secret_key=%s&appkey=%s", QixinbaoConst.GET_ID_URL, keyword, QixinbaoConst.SECRET_KEY, QixinbaoConst.APPKEY);
        System.out.println(url);

        String url2 = String.format("%s?keyword=%s&secret_key=%s&appkey=%s", "http://localhost:8090", keyword, QixinbaoConst.SECRET_KEY, QixinbaoConst.APPKEY);
        System.out.println(url2);
        // String result = HttpUtil.get(url);
        // System.out.println(result);

        // qixinbao_enterprise
        // keyword
        // eid

        // 小米科技有限责任公司 534472fd-7d53-4958-8132-d6a6242423d8
        // {"status":"200","message":"操作成功","sign":"a1e7ee240f75116208fb405a621314e3","data":{"id":"534472fd-7d53-4958-8132-d6a6242423d8"}}
    }

    @Test
    public void checkDemo() {
        String key = "1234567890abcdef";
        String iv = "1234567890abcdef";
        String dataStr = "czbank+zhangsan001+1596766819";
        String encodeStr = "nujloXQ6USN4wnrVA1fgjeheKDp2enA+ydktu+TdAYo=";
        System.out.println(getAes(key, iv).encryptBase64(dataStr));
        System.out.println(getAes(key, iv).decryptStr(encodeStr));

    }

    private String getContent() {
        // 准备字符串 [客户号tenant]+[客户方的用户名字(不能包含加号”+”)]+[当前时间戳(精确到秒)],如”cscb+zhangsan001+1596766819”;
        String tenant = "dfhk";
        // cebuy 东航采购 中国东方航空股份有限公司
        String customerName = "东航采购";
        Long timestamp = System.currentTimeMillis() / 1000;
        String content = String.format("%s+%s+%s", tenant, customerName, timestamp);
        return content;
    }

    @Test
    public void test02() {
//        String str = new String(Base64.decode("nujloXQ6USN4wnrVA1fgjeheKDp2enA+ydktu+TdAYo=".getBytes("UTF-8")));
//        System.out.println(str);
    }

    @Test
    public void test05() {
        String key = "7050000296133290";
        String iv = "7050000297173856";
        String content = "131313131";
        String encodeStr = "NWJUKXsrSStDW72eYaSUEA==";
        System.out.println(getAes(key, iv).decryptStr(encodeStr));
    }

    @Test
    public void test06() {
        // 验证：非加密字符串解密
        String key = "7050000296133290";
        String iv = "7050000297173856";
        String encodeStr = "1234567890123456";
        System.out.println(getAes(key, iv).decryptStr(encodeStr));
    }
}

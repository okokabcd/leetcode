package aaa.thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author yangdc
 * @date 2022/7/25
 */
public class ThreadDemo04 {
    static final AtomicInteger threadNo = new AtomicInteger(1);

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 1; i < 101; i += 2) {
                while (threadNo.get() != 1) {
                }
                System.out.printf("t1: %d\n", i);
                threadNo.set(2);
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 2; i < 101; i += 2) {
                while (threadNo.get() != 2) {
                }
                System.out.printf("t2: %d\n", i);
                threadNo.set(1);
            }
        });

        t1.start();
        t2.start();
    }
}

package aaa.thread;

/**
 * https://juejin.cn/post/6963831937899167775
 * 在本局游戏中，将有3位玩家出场，他们分别是哪吒、苏烈和安其拉。根据玩家不同的角色定位，在王者峡谷中，他们会有不同的游戏路线：
 *
 * 作为战士的哪吒将走上路的对抗路线；
 * 法师安其拉则去镇守中路；
 * 战坦苏烈则决定去下路。
 *
 * @author yangdc
 * @date 2022/7/8
 */
public class ThreadDemo01 {
    public static void main(String[] args) {
        Thread neZhaPlayer = new Thread(() -> System.out.println("我是哪吒，我进攻上路"));
        Thread anQilaPlayer = new Thread(() -> System.out.println("我是安其拉，我进攻中路"));
        Thread suLiePlayer = new Thread(() -> System.out.println("我是苏列，我进攻下路"));

        neZhaPlayer.start();
        anQilaPlayer.start();
        suLiePlayer.start();
    }
}

package aaa.thread;

/**
 * @author yangdc
 * @date 2022/7/8
 */
public class ThreadDemo03 {
    private static Object lock = new Object();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            synchronized (lock) {
                for (int i = 1; i < 101; i += 2) {
                    try {
                        System.out.printf("t1: %d\n", i);
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                lock.notify();
            }
        });

        Thread t2 = new Thread(() -> {
            synchronized (lock) {
                for (int i = 2; i < 101; i += 2) {
                    try {
                        System.out.printf("t2: %d\n", i);
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                lock.notify();
            }
        });

        t1.start();
        t2.start();
    }
}

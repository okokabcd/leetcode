package aaa.thread;

/**
 * 用两种不同的方式，创建出两个线程，交差打印1~100之间的奇数和偶数，并断点调试。
 *
 * @author yangdc
 * @date 2022/7/8
 */
public class ThreadDemo02 {
    /**
     * volatile 告诉signal为原子操作
     */
    private static volatile int signal = 1;

    public static void main(String[] args) {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                while (signal < 101) {
                    if (signal % 2 == 1) {
                        System.out.printf("t1: %d\n", signal);
                        // signal++非原子操作，所以加锁
                        synchronized (this) {
                            signal++;
                        }
                    }
                }
            }
        };

        Thread t2 = new Thread() {
            @Override
            public void run() {
                while (signal < 101) {
                    if (signal % 2 == 0) {
                        System.out.printf("t2: %d\n", signal);
                        synchronized (this) {
                            signal++;
                        }
                    }
                }
            }
        };

        t1.start();
        t2.start();
    }
}

package aaa;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合
 * Author:   yangdc
 * Date:     2018/6/13 19:14
 */
public class Combination {
    public void c(int[] nums, int start, int len, int cursor, List<Integer> cur, List<List<Integer>> ans) {
        if (start == len) {
            ans.add(new ArrayList<>(cur));
            return;
        }

        for (int i = cursor; i < nums.length; i++) {
            cur.add(nums[i]);
            c(nums, start + 1, len, cursor + 1, cur, ans);
            cur.remove(cur.size() - 1);
        }
    }

    public static void main(String[] args) {
        int[] nums = {-1, 0, 1, 2, -1, -4};
        List<List<Integer>> ans = new ArrayList<>();
        new Combination().c(nums, 0, 3, 0, new ArrayList<>(), ans);
        for (List<Integer> tmpList : ans) {
            for (Integer tmp : tmpList) {
                System.out.print(tmp + ",");
            }
            System.out.println();
        }
    }
}

package aaa;

import org.junit.Test;

import javax.swing.*;
import java.util.Arrays;

/**
 * @author yangdc
 * @date 2022/7/8
 */
public class DemoTest {
    @Test
    public void test03() {
        String stringPassword = "password";
        char[] charPassword = new char[]{'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
        System.out.println(String.format("Printing String password -> %s", stringPassword));
        System.out.println(String.format("Printing char[] password -> %s", charPassword));
    }

    @Test
    public void test02() {
        char[] charPassword = new char[]{'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};

        System.out.println(String.format("Original char password value: %s", charPassword));
        System.out.println(String.format("Original char password hashCode: %s",
                Integer.toHexString(charPassword.hashCode())));

        Arrays.fill(charPassword, '*');

        System.out.println(String.format("Changed char password value: %s", charPassword));
        System.out.println(String.format("Changed char password hashCode: %s",
                Integer.toHexString(charPassword.hashCode())));

    }

    @Test
    public void test01() {
        String stringPassword = "password";
        System.out.println(String.format("Original String password value: %s", stringPassword));
        System.out.println(String.format("Original String password hashCode: %s",
                Integer.toHexString(stringPassword.hashCode())));

        String newString = "*********";
        stringPassword.replace(stringPassword, newString);

        System.out.println(String.format("String password value after trying to replace it: %s", stringPassword));
        System.out.println(String.format("hashCode after trying to replace the original String: %s",
                Integer.toHexString(stringPassword.hashCode())));

    }
}

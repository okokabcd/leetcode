package aaa.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class RegUtil {
    private static Pattern numPat = Pattern.compile("^(\\d+.\\d+)");
    /**
     * 从问题类别中提取序号
     * eg: 问题类别 3.5岗位管理 将3.5提取出来
     *
     * @param problemType 问题类别
     * @return
     */
    public static String extractNum(String problemType) {
        if (problemType == null) {
            return "";
        }
        Matcher mat = numPat.matcher(problemType);
        if (mat.find()) {
            return mat.group(1);
        }
        return "";
    }
}

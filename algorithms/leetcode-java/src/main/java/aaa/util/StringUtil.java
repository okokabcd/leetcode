package aaa.util;

/**
 * 	实现String工具类
 * 	https://www.shiyanlou.com/contests/lou10/console
 */
public class StringUtil {
    public static boolean allIsNotNull(String... s){
        if (s == null || s.length == 0) {
            return false;
        }
        for (String tmp : s) {
            if (tmp == null) {
                return false;
            }
        }
        return true;
    }

    public static boolean allIsNotEmpty(String... s){
        if (s == null || s.length == 0) {
            return false;
        }
        for (String tmp : s) {
            if (tmp == null || "".equals(tmp)) {
                return false;
            }
        }
        return true;
    }
}

package aaa.util;

import cn.hutool.core.io.FileUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author yangdc
 * @date 2022/8/5
 */
public class GenerateBeanUtil {
    public static void main(String[] args) {
        String dir = "/tmp/bean/";
        List<String> propList = readLines(dir + "col1.txt");
        List<String> commentList = readLines(dir + "col2.txt");
        List<String> fieldList = readLines(dir + "col3.txt");

        for (int i = 0; i < propList.size(); i++) {
            String comment = String.format("@ApiModelProperty(notes = \"%s\")", commentList.get(i));
            String field = String.format("@TableField(value = \"%s\")", fieldList.get(i).toLowerCase(Locale.ROOT));
            String prop = propList.get(i);
            String line = String.format("%s\n%s\n%s\n", comment, field, prop);
            System.out.println(line);
        }
    }

    private static List<String> readLines(String filePath) {
        return FileUtil.readLines(filePath, "UTF-8");
    }
}

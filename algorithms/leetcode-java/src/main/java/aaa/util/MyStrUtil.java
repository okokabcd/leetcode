package aaa.util;

/**
 * @author yangdc
 * @date 2022/5/25
 */
public class MyStrUtil {
    //    最近有业务需求，编号自增。
//    1、查询数据最大编号，该字段为字符串
//    SELECT max(no_value+0) FROM `jm_xx`2、编号+1，递增后按格式补齐

    /**
     * 返回oldNum下一个数，格式为4位字符串，超过的话返回0000，异常的话返回0000
     *
     * @param oldNum
     * @return
     */
    public static String nextNum(String oldNum) {
        String nextNum = "0001";
        if (null != oldNum && !"".equals(oldNum)) {
            int i = 0;
            try {
                i = Integer.parseInt(oldNum) + 1;
            } catch (NumberFormatException e) {
            }
            if (i > 9999) {
                i = 0;
            }
            //%nd 输出的整型宽度至少为n位，右对齐，%8d即宽度至少为8位，位数大于8则输出实际位数，0表示用0补齐
            nextNum = String.format("%04d", i);
        }
        return nextNum;
    }

    /**
     * @param str eg: 项目年度-问题类别编号-4位序列号
     * @return
     */
    public static String extractSeq(String str) {
        if (str == null) {
            return "";
        }
        String[] arr = str.split("-");
        if (arr.length == 3) {
            return arr[2];
        }
        return "";
    }
}

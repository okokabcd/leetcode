package aaa.util;

import java.math.BigDecimal;

/**
 * @author yangdc
 * @date 2022/5/24
 */
public class BigDecimalUtil {
    /**
     * 金额字符串格式化，小数点保留2位，超过2位的直接去掉
     *
     * @param str
     * @return
     */
    public static String fmtMoney(String str) {
        if (str == null) {
            return "";
        }
        try {
            return new BigDecimal(str).setScale(2, BigDecimal.ROUND_FLOOR).toString();
        } catch (Exception e) {
            // 异常返回空串
            // e.printStackTrace();
        }
        return "";
    }
}

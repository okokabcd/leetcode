package aaa.util;

import cn.hutool.core.io.FileUtil;

import java.util.List;
import java.util.Locale;

/**
 * @author yangdc
 * @date 2022/8/5
 */
public class GenerateBeanUtil2 {
    public static void main(String[] args) {
        String filePath = "/tmp/tmp/a.txt";
        List<String> lineList = readLines(filePath);
        for (String line : lineList) {
            System.out.println(line);
            String[] arr = line.split("\\s+");
            String field = BeanUtil.toCamelCase(arr[1]);
            String methodName = String.format("set%s%s", (char) (field.charAt(0) - 32), field.substring(1));
            String express = String.format("orderDetail.%s(\"\");", methodName);
            System.out.println(express);
        }
    }

    private static List<String> readLines(String filePath) {
        return FileUtil.readLines(filePath, "UTF-8");
    }
}

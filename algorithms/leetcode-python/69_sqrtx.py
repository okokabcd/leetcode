#!/usr/bin/env python

def sqrt(x):
    l = 0
    r = x + 1
    while l < r:
        m = l + (r - l) // 2
        if m * m > x:
            r = m
        else:
            l = m + 1
    return l - 1

print(sqrt(4))
print(sqrt(8))

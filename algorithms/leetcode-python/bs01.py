#!/usr/bin/env python

# return the index of an element in a sorted array.Elements are unique. if not found return -1.
A = [1, 2, 5, 7, 8, 12]
def binary_search(A, val, l, r):
    while l < r:
        m = l + (r - l) // 2
        if A[m] == val: return m
        if A[m] > val:
            r = m
        else:
            l = m + 1
    return -1

print(binary_search(A, 8, 0, len(A)))
print(binary_search(A, 6, 0, len(A)))
